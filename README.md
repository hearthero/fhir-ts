![FHIR TS](https://i.imgur.com/TTrbevu.png)
# FHIR-TS: Strongly typed FHIR interfaces
FHIR-TS offers interface implementations of the [HL7 FHIR](http://hl7.org/fhir/) standard for healthcare interoperability. The main goal of this project is to support developers that need to implement FHIR into projects that rely on JavaScript or TypeScript. A common use case are medical PWAs that need to export FHIR conform data. In fact development started specifically to support developers of Digital Health Applications (DIGA in german) since bulk export of interoperable digital health data is a core requirement for ministerial classification.

FHIR-TS currently targets `v4.0.1` of HL7 FHIR.

The software directly references descriptions of the HL7 FHIR documentation in markdown comments to give developers a better idea of the objects that they are dealing with and encourages a coding style that makes sure FHIR resources are being  correctly used.

![A object description within a popular IDE](https://i.imgur.com/QDD7DE4.png)

## FHIR-TS specific changes
FHIR-TS sometimes references [Value Sets](https://www.hl7.org/fhir/valueset.html) with other names. This is due to JavaScript limitations for naming conventions.
The following letters for Value Set names will be replaced with `_` underscores:

- `/`
- `\`
- `-`
- `|`
- `(`
- `)`
- `<`
- `>`
- `=`
- `^`
- `°`
- `!`
- `?`
- `,`
- `.`
- `;`
- `:`
- `+`
- `#`
- `*`
- `~`
- `[SPACE]`

Usually these replacements don't matter since the names of CodeValueSets are not important for type checks. Only the defined types matter. However, each CodeValueSet is associated with a comment that references the original documentation and the status of the implementation. This is helpful for looking up the official names or further information.

![A CodeValueSet comment in FHIR-TS](https://i.imgur.com/YLNmBrv.png)

In some cases the sheer mass of values for a value set is so big that they are getting replaced with a `string` type. Such value sets always have a comment to let people know about this fact. Value sets with more than 1000 possible values would affect IDE performance negatively and it is encouraged to make use of the above approach in the future.

This software is a free resource and thus anybody is allowed to expand FHIR-TS. Please always check the official FHIR documentation beforehand since in cases like spelling mistakes or questionable resource references the origin will often be FHIR itself. You can create an issue for HL7 FHIR [here](https://jira.hl7.org/projects/FHIR/issues/FHIR-31328?filter=allopenissues).

FHIR-TS aims to reach 100% Conformance therefore the interfaces strictly follow the official documentation. This software does not aim to offer client or server functions but it can be used as a package for better implementations of HL7 FHIR or to offer additional implementation support.
