import * as fs from 'fs'

const baseString = `interface baseX {
  baseX: Y;
}\n\n`

const PrimitiveTypes = ['base64Binary', 'boolean', 'canonical', 'code', 'date', 'dateTime', 'decimal', 'id', 'instant', 'integer', 'markdown', 'oid', 'positiveInt', 'string', 'time', 'unsignedInt', 'uri', 'url', 'uuid']
const DataTypes = ['Address', 'Age', 'Annotation', 'Attachment', 'CodeableConcept', 'Coding', 'ContactPoint', 'Count', 'Distance', 'Duration', 'HumanName', 'Identifier', 'Money', 'Period', 'Quantity', 'Range', 'Ratio', 'Reference', 'SampledData', 'Signature', 'Timing']
const MetaDataTypes = ['ContactDetail', 'Contributor', 'DataRequirement', 'Expression', 'ParameterDefinition', 'RelatedArtifact', 'TriggerDefinition', 'UsageContext']
const SpecialTypes = ['Dosage', 'Meta']

const OpenTypes = [...PrimitiveTypes, ...DataTypes, ...MetaDataTypes, ...SpecialTypes]

const stdin = process.openStdin()
process.stdout.write('Please enter the name of the property: ')

stdin.addListener('data', text => {
  const name = text.toString().trim()
  stdin.pause()

  let output = ''

  for (const type of OpenTypes) {
    const newInterface = baseString.replace(/base/g, name).replace(/Y/g, type)
    output += newInterface.replace(/X/g, type.charAt(0).toUpperCase() + type.slice(1))
  }
  fs.writeFileSync('./output.ts', output, { encoding: 'utf8' })
})
