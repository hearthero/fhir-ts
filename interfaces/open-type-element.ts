import { AnyPrimitive } from './primitives'
import {
  Address,
  Age,
  Annotation,
  Attachment,
  CodeableConcept,
  Coding,
  ContactPoint,
  Count,
  Distance,
  Duration,
  HumanName,
  Identifier,
  Money,
  Period,
  Quantity,
  Ratio,
  SampledData,
  Signature,
  Timing
} from './foundation/base-types/general-purpose'
import { Dosage, Meta, Reference } from './foundation/base-types/special-purpose'
import {
  ContactDetail,
  Contributor,
  DataRequirement,
  Expression,
  ParameterDefinition, RelatedArtifact, TriggerDefinition, UsageContext
} from './foundation/base-types/metadata'

type AnyDataType = Address | Age | Annotation | Attachment | CodeableConcept<any> | Coding | ContactPoint | Count | Distance | Duration | HumanName | Identifier | Money | Period | Quantity | Range | Ratio | Reference<any> | SampledData | Signature | Timing;

type MetaDataTypes = ContactDetail | Contributor | DataRequirement | Expression | ParameterDefinition | RelatedArtifact | TriggerDefinition | UsageContext;

type SpecialTypes = Dosage | Meta;

/*
 The type is represented by the wildcard symbol "*" in FHIR documentation. However Typescript cannot directly reference variable names for type checks. In FHIR OpenTypeElements end with [x] which is replaced with the Title cased name of the data type.
 */
export type OpenTypeElement = AnyPrimitive | AnyDataType | MetaDataTypes | SpecialTypes;
