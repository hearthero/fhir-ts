import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Age,
  CodeableConcept,
  Duration,
  Identifier,
  Period,
  Quantity,
  Range, Timing
} from '../foundation/base-types/general-purpose'
import { canonical, code, date, dateTime, id, markdown, uri } from '../primitives'
import {
  ActionCardinalityBehavior,
  ActionConditionKind, ActionGroupingBehavior, ActionParticipantRole, ActionParticipantType,
  ActionPrecheckBehavior, ActionRequiredBehavior, ActionSelectionBehavior, ActionType,
  Condition_Problem_DiagnosisCodes,
  DefinitionTopic,
  GoalCategory,
  GoalPriority,
  GoalStartEvent,
  Jurisdiction,
  LOINCCodes,
  PlanDefinitionType,
  PublicationStatus,
  RequestPriority,
  SNOMEDCTClinicalFindings,
  SubjectType
} from '../value-sets/value-sets-4-0'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Group } from '../administration/group'
import {
  ContactDetail, DataRequirement,
  Expression,
  RelatedArtifact,
  TriggerDefinition,
  UsageContext
} from '../foundation/base-types/metadata'
import { ActivityDefinition } from './activity-definition'
import { Questionaire } from '../foundation/content-management/questionaire'
import { Library } from '../clinical-reasoning/library'
import { StructureMap } from '../implementation-support/structure-map'

/*
This resource allows for the definition of various types of plans as a sharable, consumable, and executable artifact. The resource is general enough to support the description of a broad range of clinical artifacts such as clinical decision support rules, order sets and protocols.
 */
export type PlanDefinition = PlanDefinitionBase & subject;

interface PlanDefinitionBase extends DomainResource {
  url?: uri;
  identifier?: Identifier[];
  version?: string;
  name?: string;
  title?: string;
  subtitle?: string;
  type?: CodeableConcept<PlanDefinitionType>;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  usage?: string;
  copyright?: markdown;
  approvalDate?: date;
  lastReviewDate?: date;
  effectivePeriod?: Period;
  topic?: CodeableConcept<DefinitionTopic>[];
  author?: ContactDetail[];
  editor?: ContactDetail[];
  reviewer?: ContactDetail[];
  endorser?: ContactDetail[];
  relatedArtifact?: RelatedArtifact[];
  library?: canonical<Library>[];
  goal?: BackboneElement & {
    category?: CodeableConcept<GoalCategory>;
    description: CodeableConcept<SNOMEDCTClinicalFindings>;
    priority?: CodeableConcept<GoalPriority>;
    start?: CodeableConcept<GoalStartEvent>;
    addresses?: CodeableConcept<Condition_Problem_DiagnosisCodes>[];
    documentation?: RelatedArtifact[];
    target?: targetObj[];
  }[];
  action?: actionObj[];
}

type subject = subjectCodeableConcept | subjectReference;

interface subjectCodeableConcept {
  subjectCodeableConcept?: CodeableConcept<SubjectType>;
}

interface subjectReference {
  subjectReference?: Reference<Group>;
}

type targetObj = targetObjBase & (detailQuantity | detailRange | detailCodeableConcept);

interface targetObjBase extends BackboneElement {
  measure?: CodeableConcept<LOINCCodes>;
  due?: Duration;
}

interface detailQuantity {
  detailQuantity?: Quantity;
}

interface detailRange {
  detailRange?: Range;
}

interface detailCodeableConcept {
  detailCodeableConcept?: CodeableConcept<any>;
}

type actionObj = actionObjBase & subject & timing & definition;

interface actionObjBase extends BackboneElement {
  prefix?: string;
  title?: string;
  description?: string;
  textEquivalent?: string;
  priority?: code<RequestPriority>;
  code?: CodeableConcept<any>[];
  reason?: CodeableConcept<any>[];
  documentation?: RelatedArtifact[];
  goalId?: id[];
  trigger?: TriggerDefinition[];
  condition?: BackboneElement & {
    kind: code<ActionConditionKind>;
    expression?: Expression;
  }[];
  input?: DataRequirement[];
  output?: DataRequirement[];
  relatedAction?: relatedActionObj[];
  participant?: BackboneElement & {
    type: code<ActionParticipantType>;
    role?: CodeableConcept<ActionParticipantRole>;
  }[];
  type?: CodeableConcept<ActionType>;
  groupingBehavior?: code<ActionGroupingBehavior>;
  selectionBehavior?: code<ActionSelectionBehavior>;
  requiredBehavior?: code<ActionRequiredBehavior>;
  precheckBehavior?: code<ActionPrecheckBehavior>;
  cardinalityBehavior?: code<ActionCardinalityBehavior>;
  transform?: canonical<StructureMap>;
  dynamicValue?: BackboneElement & {
    path?: string;
    expression?: Expression;
  }[];
  action?: actionObj;
}

type timing = timingDateTime | timingAge | timingPeriod | timingDuration | timingRange | timingTiming;

type definition = definitionCanonical | definitionUri;

interface timingTiming {
  timingTiming?: Timing;
}

interface timingDateTime {
  timingDateTime?: dateTime;
}

interface timingAge {
  timingAge?: Age;
}

interface timingPeriod {
  timingPeriod?: Period;
}

interface timingRange {
  timingRange?: Range;
}

interface timingDuration {
  timingDuration?: Duration;
}

interface definitionCanonical {
  definitionCanonical?: canonical<ActivityDefinition | PlanDefinition | Questionaire>;
}

interface definitionUri {
  definitionUri?: uri;
}
