import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { ArrayOfOneOrMore } from '../primitives'
import { Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { HealthcareService } from '../administration/healthcare-service'
import { Location } from '../administration/location'
import { Device } from '../administration/device'
import { PracticeSettingCodeValueSet, ServiceCategory } from '../value-sets/value-sets-4-0'

/*
A container for slots of time that may be available for booking appointments.
 */
export interface Schedule extends DomainResource {
  identifier?: Identifier[];
  active?: boolean;
  serviceCategory?: CodeableConcept<ServiceCategory>[];
  speciality?: CodeableConcept<PracticeSettingCodeValueSet>;
  actor?: ArrayOfOneOrMore<Reference<Patient | Practitioner | PractitionerRole | RelatedPerson | Device | HealthcareService | Location>>;
  planningHorizon?: Period;
  comment?: string;
}
