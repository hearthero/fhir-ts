import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  Identifier,
  Period, Range,
  SimpleQuantity,
  Timing
} from '../foundation/base-types/general-purpose'
import { canonical, code, dateTime, uri } from '../primitives'
import { ActivityDefinition } from './activity-definition'
import { PlanDefinition } from './plan-definition'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Location } from '../administration/location'
import { Device } from '../administration/device'
import { Encounter } from '../administration/encounter'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { CareTeam } from '../clinical/careteam'
import { HealthcareService } from '../administration/healthcare-service'
import { RelatedPerson } from '../administration/related-person'
import { Condition } from '../clinical/condition'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Provenance } from '../security/provenance'
import {
  Condition_Problem_DiagnosisCodes,
  FHIRDeviceTypes,
  ParticipantRoles,
  RequestIntent,
  RequestPriority,
  RequestStatus
} from '../value-sets/value-sets-4-0'
import { Coverage } from '../financial/coverage'
import { ClaimResponse } from '../financial/claim-response'

/*
Represents a request for a patient to employ a medical device. The device may be an implantable device, or an external assistive device, such as a walker.
 */
export type DeviceRequest = DeviceRequestBase & (codeReference | codeCodeableConcept) & occurrence;

interface DeviceRequestBase extends DomainResource {
  identifier?: Identifier[];
  instantiatesCanonical?: canonical<ActivityDefinition | PlanDefinition>[];
  instantiatesUri?: uri[];
  basedOn?: Reference<any>[];
  priorRequest?: Reference<any>[];
  groupIdentifier?: Identifier;
  status?: code<RequestStatus>;
  intent: code<RequestIntent>;
  priority?: code<RequestPriority>;
  parameter?: parameterObj[];
  subject: Reference<Patient | Group | Location | Device>;
  encounter?: Reference<Encounter>;
  authoredOn?: dateTime;
  requester?: Reference<Device | Practitioner | PractitionerRole | Organization>;
  performerType?: CodeableConcept<ParticipantRoles>;
  performer?: Reference<Practitioner | PractitionerRole | Organization | CareTeam | HealthcareService | Patient | Device | RelatedPerson>;
  reasonCode?: CodeableConcept<Condition_Problem_DiagnosisCodes>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport | DocumentReference>[];
  insurance?: Reference<Coverage | ClaimResponse>[];
  supportingInfo?: Reference<any>[];
  note?: Annotation[];
  relevantHistory?: Reference<Provenance>[];
}

type occurrence = occurrenceDateTime | occurrencePeriod | occurrenceTiming;

interface codeReference {
  codeReference: Reference<Device>;
}

interface codeCodeableConcept {
  codeCodeableConcept: CodeableConcept<FHIRDeviceTypes>;
}

interface occurrenceDateTime {
  occurrenceDateTime?: dateTime;
}

interface occurrencePeriod {
  occurrencePeriod?: Period;
}

interface occurrenceTiming {
  occurrenceTiming?: Timing;
}

type parameterObj = parameterObjBase & value;

interface parameterObjBase extends BackboneElement {
  code?: CodeableConcept<any>;
}

type value = valueCodeableConcept | valueQuantity | valueRange | valueBoolean;

interface valueCodeableConcept {
  valueCodeableConcept?: CodeableConcept<any>;
}

interface valueRange {
  valueRange?: Range;
}

interface valueQuantity {
  valueQuantity?: SimpleQuantity;
}

interface valueBoolean {
  valueBoolean?: boolean;
}
