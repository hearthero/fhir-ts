import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { ArrayOfOneOrMore, code, dateTime, instant, positiveInt, unsignedInt } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Condition } from '../clinical/condition'
import { Procedure } from '../clinical/procedure'
import { Observation } from '../diagnostics/observation'
import { ImmunizationRecommendation } from '../medications/immunization-recommendation'
import { Slot } from './slot'
import { ServiceRequest } from './service-request'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Device } from '../administration/device'
import { HealthcareService } from '../administration/healthcare-service'
import { Location } from '../administration/location'
import {
  AppointmentCancellationReason,
  AppointmentStatus,
  EncounterReasonCodes, ParticipantRequired,
  ParticipantType, ParticipationStatus,
  PracticeSettingCodeValueSet,
  ServiceCategory,
  ServiceType, v2_0276
} from '../value-sets/value-sets-4-0'

/*
A booking of a healthcare event among patient(s), practitioner(s), related person(s) and/or device(s) for a specific date/time. This may result in one or more Encounter(s).
 */
export interface Appointment extends DomainResource {
  identifier?: Identifier[];
  status: code<AppointmentStatus>;
  cancelationReason?: CodeableConcept<AppointmentCancellationReason>;
  serviceCategory?: CodeableConcept<ServiceCategory>[];
  serviceType?: CodeableConcept<ServiceType>[];
  specialty?: CodeableConcept<PracticeSettingCodeValueSet>[];
  appointmentType?: CodeableConcept<v2_0276>;
  reasonCode?: CodeableConcept<EncounterReasonCodes>[];
  reasonReference?: Reference<Condition | Procedure | Observation | ImmunizationRecommendation>[];
  priority?: unsignedInt;
  description?: string;
  supportingInformation?: Reference<any>[];
  start?: instant;
  end?: instant;
  minutesDuration?: positiveInt;
  slot?: Reference<Slot>[];
  created?: dateTime;
  comment?: string;
  patientInstruction?: string;
  basedOn?: Reference<ServiceRequest>[];
  participant: ArrayOfOneOrMore<participantObj>;
  requestedPeriod?: Period[];
}

interface participantObj extends BackboneElement {
  type: CodeableConcept<ParticipantType>;
  actor?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson | Device | HealthcareService | Location>;
  required?: code<ParticipantRequired>;
  status: code<ParticipationStatus>;
  period?: Period;
}
