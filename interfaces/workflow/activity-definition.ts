import { DomainResource } from '../foundation/framework/domain-resource'
import { canonical, code, date, dateTime, markdown, uri } from '../primitives'
import {
  Age,
  CodeableConcept, Duration,
  Identifier,
  Period, Range,
  SimpleQuantity,
  Timing
} from '../foundation/base-types/general-purpose'
import {
  ActionParticipantRole,
  ActionParticipantType,
  DefinitionTopic,
  Jurisdiction, ProcedureCodes_SNOMEDCT,
  PublicationStatus,
  RequestIntent, RequestPriority, RequestResourceType,
  SNOMEDCTBodyStructures, SNOMEDCTMedicationCodes
} from '../value-sets/value-sets-4-0'
import { ContactDetail, Expression, RelatedArtifact, UsageContext } from '../foundation/base-types/metadata'
import { StructureDefinition } from '../conformance/structure-definition'
import { BackboneElement, Dosage, Reference } from '../foundation/base-types/special-purpose'
import { Location } from '../administration/location'
import { Medication } from '../medications/medication'
import { Substance } from '../administration/substance'
import { Library } from '../clinical-reasoning/library'
import { ObservationDefinition } from '../administration/observation-definition'
import { SpecimenDefinition } from '../diagnostics/specimen-definition'
import { StructureMap } from '../implementation-support/structure-map'

/*
This resource allows for the definition of some activity to be performed, independent of a particular patient, practitioner, or other performance context.
 */
export type ActivityDefinition = ActivityDefinitionBase & timing & product;

interface ActivityDefinitionBase extends DomainResource {
  url?: uri;
  identifier?: Identifier[];
  version?: string;
  name?: string;
  title?: string;
  subtitle?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  usage?: string;
  copyright?: markdown;
  approvalDate?: date;
  lastReviewDate?: date;
  effectivePeriod?: Period;
  topic?: CodeableConcept<DefinitionTopic>[];
  author?: ContactDetail[];
  editor?: ContactDetail[];
  reviewer?: ContactDetail[];
  endorser?: ContactDetail[];
  relatedArtifact?: RelatedArtifact[];
  library?: canonical<Library>[];
  kind?: code<RequestResourceType>;
  profile?: canonical<StructureDefinition>;
  code?: CodeableConcept<ProcedureCodes_SNOMEDCT>;
  intent?: code<RequestIntent>;
  priority?: code<RequestPriority>;
  doNotPerform?: boolean;
  location?: Reference<Location>;
  participant?: BackboneElement & {
    type: code<ActionParticipantType>;
    role?: CodeableConcept<ActionParticipantRole>;
  }[];
  quantity?: SimpleQuantity;
  dosage?: Dosage[];
  bodySite?: CodeableConcept<SNOMEDCTBodyStructures>[];
  specimenRequirement?: Reference<SpecimenDefinition>[];
  observationRequirement?: Reference<ObservationDefinition>[];
  observationResultRequirement?: Reference<ObservationDefinition>[];
  transform?: canonical<StructureMap>;
  dynamicValue?: BackboneElement & {
    path: string;
    expression: Expression;
  }[];
}

type timing = timingTiming | timingDateTime | timingAge | timingPeriod | timingRange | timingDuration;

type product = productReference | productCodeableConcept;

interface timingTiming {
  timingTiming?: Timing;
}

interface timingDateTime {
  timingDateTime?: dateTime;
}

interface timingAge {
  timingAge?: Age;
}

interface timingPeriod {
  timingPeriod?: Period;
}

interface timingRange {
  timingRange?: Range;
}

interface timingDuration {
  timingDuration?: Duration;
}

interface productReference {
  productReference?: Reference<Medication | Substance>;
}

interface productCodeableConcept {
  productCodeableConcept?: CodeableConcept<SNOMEDCTMedicationCodes>;
}
