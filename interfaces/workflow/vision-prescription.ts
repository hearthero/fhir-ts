import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, Identifier, SimpleQuantity } from '../foundation/base-types/general-purpose'
import { ArrayOfOneOrMore, code, dateTime, decimal, integer } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Encounter } from '../administration/encounter'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import {
  ExampleVisionPrescriptionProductCodes,
  FinancialResourceStatusCodes,
  VisionBase,
  VisionEyes
} from '../value-sets/value-sets-4-0'

/*
An authorization for the provision of glasses and/or contact lenses to a patient.
 */
export interface VisionPrescription extends DomainResource {
  identifier?: Identifier[];
  status: code<FinancialResourceStatusCodes>;
  created: dateTime;
  patient: Reference<Patient>;
  encounter?: Reference<Encounter>;
  dateWritten: dateTime;
  prescriber: Reference<Practitioner | PractitionerRole>;
  lensSpecification: ArrayOfOneOrMore<lensSpecificationObj>;
}

interface lensSpecificationObj {
  product: CodeableConcept<ExampleVisionPrescriptionProductCodes>;
  eye: code<VisionEyes>;
  sphere?: decimal;
  cylinder?: decimal;
  axis?: integer;
  prism?: BackboneElement & {
    amount: decimal;
    base: code<VisionBase>;
  }[];
  add?: decimal;
  power?: decimal;
  backCurve?: decimal;
  diameter?: decimal;
  duration?: SimpleQuantity;
  color?: string;
  brand?: string;
  note?: Annotation[];
}
