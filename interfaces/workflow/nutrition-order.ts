import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  Identifier, Ratio,
  SimpleQuantity,
  Timing
} from '../foundation/base-types/general-purpose'
import { canonical, code, dateTime, uri } from '../primitives'
import { ActivityDefinition } from './activity-definition'
import { PlanDefinition } from './plan-definition'
import { Patient } from '../administration/patient'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Encounter } from '../administration/encounter'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { AllergyIntolerance } from '../clinical/allergy-intolerance'
import {
  Diet, EnteralFormulaAdditiveTypeCode, EnteralFormulaTypeCodes, EnteralRouteCodes, FluidConsistencyTypeCodes,
  FoodTypeCodes,
  NutrientModifierCodes,
  RequestIntent,
  RequestStatus, SupplementTypeCodes, TextureModifiedFoodTypeCodes,
  TextureModifierCodes
} from '../value-sets/value-sets-4-0'

/*
A request to supply a diet, formula feeding (enteral) or oral nutritional supplement to a patient/resident.
 */
export interface NutritionOrder extends DomainResource {
  identifier?: Identifier[];
  instantiatesCanonical?: canonical<ActivityDefinition | PlanDefinition>[];
  instantiatesUri?: uri[];
  status: code<RequestStatus>;
  intent: code<RequestIntent>;
  patient: Reference<Patient>;
  encounter?: Reference<Encounter>;
  dateTime: dateTime;
  orderer?: Reference<Practitioner | PractitionerRole>;
  allergyIntolerance?: Reference<AllergyIntolerance>[];
  foodPreferenceModifier?: CodeableConcept<Diet>[];
  excludedFoodModifier?: CodeableConcept<FoodTypeCodes>[];
  oralDiet?: BackboneElement & {
    type?: CodeableConcept<Diet>[];
    schedule?: Timing[];
    nutrient?: BackboneElement & {
      modifier?: CodeableConcept<NutrientModifierCodes>;
      amount?: SimpleQuantity;
    }[];
    texture?: BackboneElement & {
      modifier?: CodeableConcept<TextureModifierCodes>;
      foodType?: CodeableConcept<TextureModifiedFoodTypeCodes>;
    }[];
    fluidConsistencyType?: CodeableConcept<FluidConsistencyTypeCodes>[];
    instruction?: string;
  };
  supplement?: BackboneElement & {
    type?: CodeableConcept<SupplementTypeCodes>;
    productName?: string;
    schedule?: Timing[];
    quantity?: SimpleQuantity;
    instruction?: string;
  }[];
  enteralFormula?: BackboneElement & {
    baseFormulaType?: CodeableConcept<EnteralFormulaTypeCodes>;
    baseFormulaProductName?: string;
    additiveType?: CodeableConcept<EnteralFormulaAdditiveTypeCode>;
    additiveProductName?: string;
    caloricDensity?: SimpleQuantity;
    routeofAdministration?: CodeableConcept<EnteralRouteCodes>;
    administration?: administrationObj[];
    maxVolumeToDeliver?: SimpleQuantity;
    administrationInstruction?: string;
  };
  note?: Annotation[];
}

type rate = rateQuantity | rateRatio;

type administrationObj = administrationObjBase & rate;

interface administrationObjBase extends BackboneElement {
  schedule?: Timing;
  quantity?: SimpleQuantity;
}

interface rateQuantity {
  rateQuantity?: SimpleQuantity;
}

interface rateRatio {
  rateRatio?: Ratio;
}
