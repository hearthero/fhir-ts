import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  Identifier,
  Period,
  Timing
} from '../foundation/base-types/general-purpose'
import { Reference } from '../foundation/base-types/special-purpose'
import { ServiceRequest } from './service-request'
import { Patient } from '../administration/patient'
import { code, dateTime } from '../primitives'
import { Group } from '../administration/group'
import { Procedure } from '../clinical/procedure'
import { Observation } from '../diagnostics/observation'
import { QuestionaireResponse } from '../foundation/content-management/questionaire-response'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Device } from '../administration/device'
import { Condition } from '../clinical/condition'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { DeviceUseStatementStatus, SNOMEDCTBodyStructures } from '../value-sets/value-sets-4-0'
import { Claim } from '../financial/claim'

/*
A record of a device being used by a patient where the record is the result of a report from the patient or another clinician.
 */
export type DeviceUseStatement = DeviceUseStatementBase & timing;

interface DeviceUseStatementBase extends DomainResource {
  identifier?: Identifier[];
  basedOn?: Reference<ServiceRequest>[];
  status: code<DeviceUseStatementStatus>;
  subject: Reference<Patient | Group>;
  derivedFrom?: Reference<ServiceRequest | Procedure | Claim | Observation | QuestionaireResponse | DocumentReference>[];
  recordedOn?: dateTime;
  source?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson>;
  device: Reference<Device>;
  reasonCode?: CodeableConcept<any>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport | DocumentReference>[];
  bodySite?: CodeableConcept<SNOMEDCTBodyStructures>;
  note?: Annotation[];
}

type timing = timingTiming | timingPeriod | timingDateTime;

interface timingTiming {
  timingTiming?: Timing;
}

interface timingDateTime {
  timingDateTime?: dateTime;
}

interface timingPeriod {
  timingPeriod?: Period;
}
