import { DomainResource } from '../foundation/framework/domain-resource'
import {
  CodeableConcept,
  Identifier,
  Period,
  Quantity,
  Range,
  SimpleQuantity, Timing
} from '../foundation/base-types/general-purpose'
import { code, dateTime } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { Patient } from '../administration/patient'
import { RelatedPerson } from '../administration/related-person'
import { Device } from '../administration/device'
import { HealthcareService } from '../administration/healthcare-service'
import { Condition } from '../clinical/condition'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Location } from '../administration/location'
import { Medication } from '../medications/medication'
import { Substance } from '../administration/substance'
import {
  RequestPriority,
  SNOMEDCTSupplyItem,
  SupplyRequestReason,
  SupplyRequestStatus,
  SupplyType
} from '../value-sets/value-sets-4-0'

/*
A record of a request for a medication, substance or device used in the healthcare setting.
 */
export type SupplyRequest = SupplyRequestBase & item & occurrence;

interface SupplyRequestBase extends DomainResource {
  identifier?: Identifier[];
  status?: code<SupplyRequestStatus>;
  category?: CodeableConcept<SupplyType>;
  priority?: code<RequestPriority>;
  quantity: Quantity;
  parameter?: parameterObj[];
  authoredOn?: dateTime;
  requester?: Reference<Practitioner | PractitionerRole | Organization | Patient | RelatedPerson | Device>;
  supplier?: Reference<Organization | HealthcareService>[];
  reasonCode?: CodeableConcept<SupplyRequestReason>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport | DocumentReference>[];
  deliverFrom?: Reference<Organization | Location>;
  deliverTo?: Reference<Organization | Location | Patient>;
}

type item = itemCodeableConcept | itemReference;

type occurrence = occurrenceDateTime | occurrencePeriod | occurrenceTiming;

type parameterObj = parameterObjBase & value;

interface itemCodeableConcept {
  itemCodeableConcept: CodeableConcept<SNOMEDCTSupplyItem>;
}

interface itemReference {
  itemReference: Reference<Medication | Substance | Device>;
}

interface occurrenceDateTime {
  occurrenceDateTime?: dateTime;
}

interface occurrencePeriod {
  occurrencePeriod?: Period;
}

interface occurrenceTiming {
  occurrenceTiming?: Timing;
}

interface parameterObjBase extends BackboneElement {
  code?: CodeableConcept<any>;
}

type value = valueCodeableConcept | valueQuantity | valueRange | valueBoolean;

interface valueCodeableConcept {
  valueCodeableConcept?: CodeableConcept<any>;
}

interface valueRange {
  valueRange?: Range;
}

interface valueQuantity {
  valueQuantity?: SimpleQuantity;
}

interface valueBoolean {
  valueBoolean?: boolean;
}
