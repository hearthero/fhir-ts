import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier } from '../foundation/base-types/general-purpose'
import { Reference } from '../foundation/base-types/special-purpose'
import { Schedule } from './schedule'
import { code, instant } from '../primitives'
import {
  PracticeSettingCodeValueSet,
  ServiceCategory,
  ServiceType,
  SlotStatus,
  v2_0276
} from '../value-sets/value-sets-4-0'

/*
A slot of time on a schedule that may be available for booking appointments.
 */
export interface Slot extends DomainResource {
  identifier?: Identifier[];
  serviceCategory?: CodeableConcept<ServiceCategory>;
  serviceType?: CodeableConcept<ServiceType>;
  speciality?: CodeableConcept<PracticeSettingCodeValueSet>;
  appointmentType?: CodeableConcept<v2_0276>;
  schedule: Reference<Schedule>;
  status: code<SlotStatus>;
  start: instant;
  end: instant;
  overbooked?: boolean;
  comment?: string;
}
