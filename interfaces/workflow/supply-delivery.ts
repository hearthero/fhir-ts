import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Period, SimpleQuantity, Timing } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { SupplyRequest } from './supply-request'
import { code, dateTime } from '../primitives'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { Location } from '../administration/location'
import { Medication } from '../medications/medication'
import { Substance } from '../administration/substance'
import { Device } from '../administration/device'
import { Contract } from '../financial/contract'
import { SNOMEDCTSupplyItem, SupplyDeliveryStatus, SupplyItemType } from '../value-sets/value-sets-4-0'

/*
Record of delivery of what is supplied.
 */
export type SupplyDelivery = SupplyDeliveryBase & occurrence;

interface SupplyDeliveryBase extends DomainResource {
  identifier?: Identifier[];
  basedOn?: Reference<SupplyRequest>[];
  partOf?: Reference<SupplyDelivery | Contract>[];
  status?: code<SupplyDeliveryStatus>;
  patient?: Reference<Patient>;
  type?: CodeableConcept<SupplyItemType>;
  suppliedItem?: suppliedItemObj;
  supplier?: Reference<Practitioner | PractitionerRole | Organization>;
  destination?: Reference<Location>;
  receiver?: Reference<Practitioner | PractitionerRole>[];
}

type suppliedItemObj = suppliedItemObjBase & item;

interface suppliedItemObjBase extends BackboneElement {
  quantity?: SimpleQuantity;
}

type item = itemCodeableConcept | itemReference;

type occurrence = occurrenceDateTime | occurrencePeriod | occurrenceTiming;

interface itemCodeableConcept {
  itemCodeableConcept?: CodeableConcept<SNOMEDCTSupplyItem>;
}

interface itemReference {
  itemReference?: Reference<Medication | Substance | Device>;
}

interface occurrenceDateTime {
  occurrenceDateTime?: dateTime;
}

interface occurrencePeriod {
  occurrencePeriod?: Period;
}

interface occurrenceTiming {
  occurrenceTiming?: Timing;
}
