import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier } from '../foundation/base-types/general-purpose'
import { Reference } from '../foundation/base-types/special-purpose'
import { code, instant } from '../primitives'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { HealthcareService } from '../administration/healthcare-service'
import { Location } from '../administration/location'
import { Device } from '../administration/device'
import { ParticipantType, ParticipationStatus } from '../value-sets/value-sets-4-0'
import { Appointment } from './appointment'

/*
A reply to an appointment request for a patient and/or practitioner(s), such as a confirmation or rejection.
 */
export interface AppointmentResponse extends DomainResource {
  identifier?: Identifier[];
  appointment: Reference<Appointment>;
  start?: instant;
  end?: instant;
  participantType?: CodeableConcept<ParticipantType>[];
  actor?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson | Device | HealthcareService | Location>;
  participantStatus: code<ParticipationStatus>;
  comment?: string;
}
