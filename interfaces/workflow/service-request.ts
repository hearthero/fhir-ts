import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  Identifier, Period,
  Quantity,
  Range,
  Ratio, Timing
} from '../foundation/base-types/general-purpose'
import { canonical, code, dateTime, uri } from '../primitives'
import { Reference } from '../foundation/base-types/special-purpose'
import { CarePlan } from '../clinical/careplan'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Location } from '../administration/location'
import { Device } from '../administration/device'
import { Encounter } from '../administration/encounter'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { RelatedPerson } from '../administration/related-person'
import { CareTeam } from '../clinical/careteam'
import { HealthcareService } from '../administration/healthcare-service'
import { Condition } from '../clinical/condition'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Provenance } from '../security/provenance'
import {
  ParticipantRoles, ProcedureCodes_SNOMEDCT,
  ProcedureReasonCodes,
  RequestIntent,
  RequestPriority,
  RequestStatus,
  ServiceRequestCategoryCodes,
  ServiceRequestOrderDetailsCodes,
  SNOMEDCTBodyStructures,
  SNOMEDCTMedicationAsNeededReasonCodes, v3_ServiceDeliveryLocationRoleType
} from '../value-sets/value-sets-4-0'
import { Specimen } from '../diagnostics/specimen'
import { MedicationRequest } from '../medications/medication-request'
import { PlanDefinition } from './plan-definition'
import { ActivityDefinition } from './activity-definition'
import { Coverage } from '../financial/coverage'
import { ClaimResponse } from '../financial/claim-response'

/*
A record of a request for service such as diagnostic investigations, treatments, or operations to be performed.
 */
export type ServiceRequest = ServiceRequestBase & quantity & occurrence & asNeeded;

interface ServiceRequestBase extends DomainResource {
  identifier?: Identifier[];
  instantiatesCanonical?: canonical<ActivityDefinition | PlanDefinition>[];
  instantiatesUri?: uri[];
  basedOn?: Reference<CarePlan | ServiceRequest | MedicationRequest>[];
  replaces?: Reference<ServiceRequest>[];
  requisition?: Identifier;
  status: code<RequestStatus>;
  intent: code<RequestIntent>;
  category?: CodeableConcept<ServiceRequestCategoryCodes>[];
  priority?: code<RequestPriority>;
  doNotPerform?: boolean;
  code?: CodeableConcept<ProcedureCodes_SNOMEDCT>;
  orderDetail?: CodeableConcept<ServiceRequestOrderDetailsCodes>;
  subject: Reference<Patient | Group | Location | Device>;
  encounter?: Reference<Encounter>;
  authoredOn?: dateTime;
  requester?: Reference<Practitioner | PractitionerRole | Organization | Patient | RelatedPerson | Device>;
  performerType?: CodeableConcept<ParticipantRoles>;
  performer?: Reference<Practitioner | PractitionerRole | Organization | CareTeam | HealthcareService | Patient | Device | RelatedPerson>[];
  locationCode?: CodeableConcept<v3_ServiceDeliveryLocationRoleType>[];
  locationReference?: Reference<Location>[];
  reasonCode?: CodeableConcept<ProcedureReasonCodes>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport | DocumentReference>[];
  insurance?: Reference<Coverage | ClaimResponse>[];
  supportingInfo?: Reference<any>[];
  specimen?: Reference<Specimen>[];
  bodySite?: CodeableConcept<SNOMEDCTBodyStructures>[];
  note?: Annotation[];
  patientInstruction?: string;
  relevantHistory?: Reference<Provenance>;
}

type quantity = quantityQuantity | quantityRatio | quantityRange;

interface quantityQuantity {
  quantityQuantity?: Quantity;
}

interface quantityRatio {
  quantityRatio?: Ratio;
}

interface quantityRange {
  quantityRange?: Range;
}

type occurrence = occurrenceDateTime | occurrencePeriod | occurrenceTiming;

interface occurrenceDateTime {
  occurrenceDateTime?: dateTime;
}

interface occurrencePeriod {
  occurrencePeriod?: Period;
}

interface occurrenceTiming {
  occurrenceTiming?: Timing;
}

type asNeeded = asNeededBoolean | asNeededCodeableConcept;

interface asNeededBoolean {
  asNeededBoolean?: boolean;
}

interface asNeededCodeableConcept {
  asNeededCodeableConcept?: CodeableConcept<SNOMEDCTMedicationAsNeededReasonCodes>;
}
