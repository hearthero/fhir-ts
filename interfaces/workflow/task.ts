import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { canonical, code, dateTime, positiveInt, uri } from '../primitives'
import { ActivityDefinition } from './activity-definition'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Encounter } from '../administration/encounter'
import { Device } from '../administration/device'
import { Organization } from '../administration/organization'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { CareTeam } from '../clinical/careteam'
import { HealthcareService } from '../administration/healthcare-service'
import { Location } from '../administration/location'
import { Provenance } from '../security/provenance'
import { Group } from '../administration/group'
import {
  ProcedurePerformerRoleCodes,
  RequestPriority,
  TaskCode,
  TaskIntent,
  TaskStatus
} from '../value-sets/value-sets-4-0'
import { Coverage } from '../financial/coverage'
import { ClaimResponse } from '../financial/claim-response'

/*
A task to be performed.
 */
export interface Task extends DomainResource {
  identifier?: Identifier[];
  instantiatesCanonical?: canonical<ActivityDefinition>;
  instantiatesUri?: uri;
  basedOn?: Reference<any>[];
  groupIdentifier?: Identifier;
  partOf?: Reference<Task>[];
  status: code<TaskStatus>;
  statusReason?: CodeableConcept<any>;
  businessStatus?: CodeableConcept<any>;
  intent: code<TaskIntent>;
  priority?: code<RequestPriority>;
  code?: CodeableConcept<TaskCode>;
  description?: string;
  focus?: Reference<any>;
  for?: Reference<any>;
  encounter?: Reference<Encounter>;
  executionPeriod?: Period;
  authoredOn?: dateTime;
  lastModified?: dateTime;
  requester?: Reference<Device | Organization | Patient | Practitioner | PractitionerRole | RelatedPerson>;
  performerType?: CodeableConcept<ProcedurePerformerRoleCodes>[];
  owner?: Reference<Practitioner | PractitionerRole | Organization | CareTeam | HealthcareService | Patient | Device | RelatedPerson>;
  location?: Reference<Location>;
  reasonCode?: CodeableConcept<any>;
  reasonReference?: Reference<any>;
  insurance?: Reference<Coverage | ClaimResponse>[];
  note?: Annotation[];
  relevantHistory?: Reference<Provenance>[];
  restriction?: BackboneElement & {
    repetitions?: positiveInt;
    period?: Period;
    recipient?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson | Group | Organization>[];
  };
  input?: BackboneElement & {
    type: CodeableConcept<any>;
    value: any; // TODO
  }[];
  output?: BackboneElement & {
    type: CodeableConcept<any>;
    value: any; // TODO
  }[];
}
