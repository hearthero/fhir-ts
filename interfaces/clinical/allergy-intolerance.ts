import { DomainResource } from '../foundation/framework/domain-resource'
import { Age, Annotation, CodeableConcept, Identifier, Period, Range } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Encounter } from '../administration/encounter'
import { ArrayOfOneOrMore, code, dateTime } from '../primitives'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import {
  AllergyIntoleranceCategory,
  AllergyIntoleranceClinicalStatusCodes,
  AllergyIntoleranceCriticality, AllergyIntoleranceSeverity,
  AllergyIntoleranceSubstance_Product_ConditionAndNegationCodes,
  AllergyIntoleranceType,
  AllergyIntoleranceVerificationStatusCodes,
  SNOMEDCTRouteCodes,
  SubstanceCode
} from '../value-sets/value-sets-4-0'

/*
Risk of harmful or undesirable, physiological response which is unique to an individual and associated with exposure to a substance.
 */
export type AllergyIntolerance = AllergyIntoleranceBase & (onsetDateTime | onsetAge | onsetPeriod | onsetRange | onsetString);

interface AllergyIntoleranceBase extends DomainResource {
  identifier?: Identifier[];
  clinicalStatus?: CodeableConcept<AllergyIntoleranceClinicalStatusCodes>;
  verificationStatus?: CodeableConcept<AllergyIntoleranceVerificationStatusCodes>;
  type?: code<AllergyIntoleranceType>;
  category?: code<AllergyIntoleranceCategory>[];
  criticality?: code<AllergyIntoleranceCriticality>;
  code?: CodeableConcept<AllergyIntoleranceSubstance_Product_ConditionAndNegationCodes>;
  patient: Reference<Patient>;
  encounter?: Reference<Encounter>;
  recordedDate?: dateTime;
  recorder?: Reference<Practitioner | PractitionerRole | Patient | RelatedPerson>;
  asserter?: Reference<Patient | RelatedPerson | Practitioner | PractitionerRole>;
  lastOccurrence?: dateTime;
  note?: Annotation[];
  reaction?: BackboneElement & {
    substance?: CodeableConcept<SubstanceCode>;
    manifestation: ArrayOfOneOrMore<CodeableConcept<SNOMEDCTRouteCodes>>;
    description?: string;
    onset?: dateTime;
    severity?: code<AllergyIntoleranceSeverity>;
    exposureRoute?: CodeableConcept<SNOMEDCTRouteCodes>;
    note?: Annotation[];
  }[];
}

interface onsetDateTime {
  onsetDateTime?: dateTime;
}

interface onsetAge {
  onsetAge?: Age;
}

interface onsetPeriod {
  onsetPeriod?: Period;
}

interface onsetRange {
  onsetRange?: Range;
}

interface onsetString {
  onsetString?: string;
}
