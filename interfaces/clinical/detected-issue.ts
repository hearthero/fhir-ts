import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { code, dateTime, uri } from '../primitives'
import { Patient } from '../administration/patient'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Device } from '../administration/device'
import {
  DetectedIssueCategory, DetectedIssueMitigationAction,
  DetectedIssueSeverity,
  ManifestationAndSymptomCodes,
  ObservationStatus
} from '../value-sets/value-sets-4-0'

/*
Indicates an actual or potential clinical issue with or between one or more active or proposed clinical actions for a patient; e.g. Drug-drug interaction, Ineffective treatment frequency, Procedure-condition conflict, etc.
 */
export type DetectedIssue = DetectedIssueBase & (identifiedDateTime | identifiedPeriod);

interface DetectedIssueBase extends DomainResource {
  identifier?: Identifier[];
  status: code<ObservationStatus>;
  code?: CodeableConcept<DetectedIssueCategory>;
  severity?: code<DetectedIssueSeverity>;
  patient?: Reference<Patient>;
  author?: Reference<Practitioner | PractitionerRole | Device>;
  implicated?: Reference<any>;
  evidence?: BackboneElement & {
    code?: CodeableConcept<ManifestationAndSymptomCodes>[];
    detail?: Reference<any>[];
  }[];
  detail?: string;
  reference?: uri;
  mitigation?: BackboneElement & {
    action: CodeableConcept<DetectedIssueMitigationAction>;
    date?: dateTime;
    author?: Reference<Practitioner | PractitionerRole>;
  }[];
}

interface identifiedDateTime {
  identifiedDateTime?: dateTime;
}

interface identifiedPeriod {
  identifiedPeriod?: Period;
}
