import { DomainResource } from '../foundation/framework/domain-resource'
import { Age, Annotation, CodeableConcept, Identifier, Period, Range } from '../foundation/base-types/general-purpose'
import { canonical, code, date, dateTime, uri } from '../primitives'
import { Questionaire } from '../foundation/content-management/questionaire'
import { OperationDefinition } from '../conformance/operation-definition'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import {
  AdministrativeGender, Condition_Problem_DiagnosisCodes, ConditionOutcomeCodes,
  FamilyHistoryAbsentReason,
  FamilyHistoryStatus,
  SNOMEDCTClinicalFindings, v3_FamilyMember
} from '../value-sets/value-sets-4-0'
import { Condition } from './condition'
import { AllergyIntolerance } from './allergy-intolerance'
import { QuestionaireResponse } from '../foundation/content-management/questionaire-response'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { PlanDefinition } from '../workflow/plan-definition'
import { Measure } from '../clinical-reasoning/measure'
import { ActivityDefinition } from '../workflow/activity-definition'

/*
Significant health conditions for a person related to the patient relevant in the context of care for the patient.
 */
export type FamilyMemberHistory = FamilyMemberHistoryBase & (bornPeriod | bornDate | bornString) & (ageAge | ageRange | ageString)
  & (deceasedBoolean | deceasedAge | deceasedRange | deceasedDate | deceasedString);

interface FamilyMemberHistoryBase extends DomainResource {
  identifier?: Identifier[];
  instantiatesCanonical?: canonical<PlanDefinition | Questionaire | ActivityDefinition | Measure | OperationDefinition>[];
  instantiatesUri?: uri[];
  status: code<FamilyHistoryStatus>;
  dataAbsentReason?: CodeableConcept<FamilyHistoryAbsentReason>;
  patient?: Reference<Patient>;
  date?: dateTime;
  name?: string;
  relationship: CodeableConcept<v3_FamilyMember>;
  sex?: CodeableConcept<AdministrativeGender>;
  estimatedAge?: boolean;
  reasonCode?: CodeableConcept<SNOMEDCTClinicalFindings>[];
  reasonReference?: Reference<Condition | Observation | AllergyIntolerance | QuestionaireResponse | DiagnosticReport | DocumentReference>[];
  note?: Annotation[];
  condition?: conditionObj[];
}

interface bornPeriod {
  bornPeriod?: Period;
}

interface bornDate {
  bornDate?: date;
}

interface bornString {
  bornString?: string;
}

interface ageAge {
  ageAge?: Age;
}

interface ageRange {
  ageRange?: ageRange;
}

interface ageString {
  ageString?: ageString;
}

interface deceasedBoolean {
  deceasedBoolean?: boolean;
}

interface deceasedAge {
  deceasedAge?: Age;
}

interface deceasedRange {
  deceasedRange?: Range;
}

interface deceasedDate {
  deceasedDate?: Date;
}

interface deceasedString {
  deceasedString?: string;
}

type conditionObj = conditionObjBase & (onsetAge | onsetRange | onsetPeriod | onsetString);

interface conditionObjBase extends BackboneElement {
  code: CodeableConcept<Condition_Problem_DiagnosisCodes>;
  outcome?: CodeableConcept<ConditionOutcomeCodes>;
  contributedToDeath?: boolean;
  note?: Annotation[];
}

interface onsetAge {
  onsetAge?: Age;
}

interface onsetRange {
  onsetRange?: Range;
}

interface onsetPeriod {
  onsetPeriod?: Period;
}

interface onsetString {
  onsetString?: string;
}
