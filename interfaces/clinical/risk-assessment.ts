import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, Identifier, Period, Range } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { code, dateTime, decimal } from '../primitives'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { Condition } from './condition'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Device } from '../administration/device'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { ObservationStatus, RiskProbability } from '../value-sets/value-sets-4-0'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'

/*
An assessment of the likely outcome(s) for a patient or other subject as well as the likelihood of each outcome.
 */
export type RiskAssessment = RiskAssessmentBase & (occurrenceDateTime | occurrencePeriod);

interface RiskAssessmentBase extends DomainResource {
  identifier?: Identifier[];
  basedOn?: Reference<any>;
  parent?: Reference<any>;
  status: code<ObservationStatus>;
  method?: CodeableConcept<any>;
  code?: CodeableConcept<any>;
  subject: Reference<Patient | Group>;
  encounter?: Reference<Encounter>;
  condition?: Reference<Condition>;
  performer?: Reference<Practitioner | PractitionerRole | Device>;
  reasonCode?: CodeableConcept<any>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport | DocumentReference>[];
  basis?: Reference<any>[];
  prediction?: predictionObj[];
  mitigation?: string;
  note?: Annotation[];
}

interface occurrenceDateTime {
  occurrenceDateTime?: dateTime;
}

interface occurrencePeriod {
  occurrencePeriod?: Period;
}

type predictionObj = predictionObjBase & (probabilityDecimal | probabilityRange) & (whenPeriod | whenRange);

interface predictionObjBase extends BackboneElement {
  outcome?: CodeableConcept<any>;
  qualitativeRisk?: CodeableConcept<RiskProbability>;
  relativeRisk?: decimal;
  rationale?: string;
}

interface probabilityDecimal {
  probabilityDecimal?: decimal;
}

interface probabilityRange {
  probabilityRange?: Range;
}

interface whenPeriod {
  whenPeriod?: Period;
}

interface whenRange {
  whenRange?: Range;
}
