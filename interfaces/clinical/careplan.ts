import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  Identifier,
  Period,
  SimpleQuantity, Timing
} from '../foundation/base-types/general-purpose'
import { canonical, code, dateTime, uri } from '../primitives'
import { Questionaire } from '../foundation/content-management/questionaire'
import { OperationDefinition } from '../conformance/operation-definition'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Device } from '../administration/device'
import { RelatedPerson } from '../administration/related-person'
import { Organization } from '../administration/organization'
import { Condition } from './condition'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Location } from '../administration/location'
import { HealthcareService } from '../administration/healthcare-service'
import { Substance } from '../administration/substance'
import { PlanDefinition } from '../workflow/plan-definition'
import { ActivityDefinition } from '../workflow/activity-definition'
import { CareTeam } from './careteam'
import { Goal } from './goal'
import { DeviceRequest } from '../workflow/device-request'
import { MedicationRequest } from '../medications/medication-request'
import { NutritionOrder } from '../workflow/nutrition-order'
import { Task } from '../workflow/task'
import { ServiceRequest } from '../workflow/service-request'
import { VisionPrescription } from '../workflow/vision-prescription'
import { Medication } from '../medications/medication'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { Observation } from '../diagnostics/observation'
import {
  CarePlanActivityKind,
  CarePlanActivityOutcome, CarePlanActivityStatus,
  CarePlanCategory,
  CarePlanIntent,
  ProcedureCodes_SNOMEDCT,
  RequestStatus,
  SNOMEDCTClinicalFindings
} from '../value-sets/value-sets-4-0'
import { Appointment } from '../workflow/appointment'
import { Measure } from '../clinical-reasoning/measure'
import { CommunicationRequest } from '../administration/communication-request'
import { RequestGroup } from '../clinical-reasoning/request-group'

/*
Describes the intention of how one or more practitioners intend to deliver care for a particular patient, group or community for a period of time, possibly limited to care for a specific condition or set of conditions.
 */
export interface CarePlan extends DomainResource {
  identifier?: Identifier[];
  instantiatesCanonical?: canonical<PlanDefinition | Questionaire | Measure | ActivityDefinition | OperationDefinition>[];
  instantiatesUri?: uri[];
  basedOn?: Reference<CarePlan>[];
  replaces?: Reference<CarePlan>[];
  partOf?: Reference<CarePlan>[];
  status: code<RequestStatus>;
  intent: code<CarePlanIntent>;
  category?: CodeableConcept<CarePlanCategory>[];
  title?: string;
  description?: string;
  subject: Reference<Patient | Group>;
  encounter?: Reference<Encounter>;
  period?: Period;
  created?: dateTime;
  author?: Reference<Patient | Practitioner | PractitionerRole | Device | RelatedPerson | Organization | CareTeam>;
  contributor?: Reference<Patient | Practitioner | PractitionerRole | Device | RelatedPerson | Organization | CareTeam>[];
  careTeam?: Reference<CareTeam>[];
  addresses?: Reference<Condition>[];
  supportingInfo?: Reference<any>[];
  goal?: Reference<Goal>[];
  activity?: BackboneElement & {
    outcomeCodeableConcept?: CodeableConcept<CarePlanActivityOutcome>[];
    outcomeReference?: Reference<any>;
    progress?: Annotation[];
    reference?: Reference<Appointment | CommunicationRequest | DeviceRequest | MedicationRequest | NutritionOrder | Task | ServiceRequest | VisionPrescription | RequestGroup>;
    detail?: detailObj;
  }[];
  note?: Annotation[];
}

type detailObj = detailObjBase & (scheduledTiming | scheduledPeriod | scheduledString) & (productCodeableConcept | productReference);

interface detailObjBase extends BackboneElement {
  kind?: code<CarePlanActivityKind>;
  instantiatesCanonical?: canonical<PlanDefinition | ActivityDefinition | Questionaire | Measure | OperationDefinition>[];
  instantiatesUri?: uri[];
  code?: CodeableConcept<ProcedureCodes_SNOMEDCT>;
  reasonCode?: CodeableConcept<SNOMEDCTClinicalFindings>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport | DocumentReference>[];
  goal?: Reference<Goal>[];
  status: code<CarePlanActivityStatus>;
  statusReason?: CodeableConcept<any>;
  doNotPerform?: boolean;
  location?: Reference<Location>;
  performer?: Reference<Practitioner | PractitionerRole | Organization | RelatedPerson | Patient | CareTeam | HealthcareService | Device>[];
  dailyAmount?: SimpleQuantity;
  quantity?: SimpleQuantity;
  description?: string;
}

interface scheduledTiming {
  scheduledTiming?: Timing;
}

interface scheduledPeriod {
  scheduledPeriod?: Period;
}

interface scheduledString {
  scheduledString?: string;
}

interface productCodeableConcept {
  productCodeableConcept?: CodeableConcept<any>;
}

interface productReference {
  productReference?: Reference<Medication | Substance>;
}
