import { DomainResource } from '../foundation/framework/domain-resource'
import { Age, Annotation, CodeableConcept, Identifier, Period, Range } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { dateTime } from '../primitives'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { ClinicalImpression } from './clinical-impression'
import {
  Condition_DiagnosisSeverity,
  Condition_Problem_DiagnosisCodes, ConditionCategoryCodes,
  ConditionClinicalStatusCodes,
  ConditionStage,
  ConditionStageType,
  ConditionVerificationStatus,
  ManifestationAndSymptomCodes,
  SNOMEDCTBodyStructures
} from '../value-sets/value-sets-4-0'

/*
A clinical condition, problem, diagnosis, or other event, situation, issue, or clinical concept that has risen to a level of concern.
 */
export type Condition = ConditionBase & (onsetDateTime | onsetAge | onsetPeriod | onsetRange | onsetString)
  & (abatementDateTime | abatementAge | abatementPeriod | abatementRange | abatementString);

interface ConditionBase extends DomainResource {
  identifier?: Identifier[];
  clinicalStatus?: CodeableConcept<ConditionClinicalStatusCodes>;
  verificationStatus?: CodeableConcept<ConditionVerificationStatus>;
  category?: CodeableConcept<ConditionCategoryCodes>[];
  severity?: CodeableConcept<Condition_DiagnosisSeverity>;
  code?: CodeableConcept<Condition_Problem_DiagnosisCodes>;
  bodySite?: CodeableConcept<SNOMEDCTBodyStructures>[];
  subject: Reference<Patient | Group>;
  encounter?: Reference<Encounter>;
  recordedDate?: dateTime;
  recorder?: Reference<Practitioner | PractitionerRole | Patient | RelatedPerson>;
  asserter?: Reference<Practitioner | PractitionerRole | Patient | RelatedPerson>;
  stage?: BackboneElement & {
    summary?: CodeableConcept<ConditionStage>;
    assessment?: Reference<ClinicalImpression | DiagnosticReport | Observation>[];
    type?: CodeableConcept<ConditionStageType>;
  }[];
  evidence?: BackboneElement & {
    code?: CodeableConcept<ManifestationAndSymptomCodes>[];
    detail?: Reference<any>[];
  }[];
  note?: Annotation[];
}

interface onsetDateTime {
  onsetDateTime?: dateTime;
}

interface onsetAge {
  onsetAge?: Age;
}

interface onsetPeriod {
  onsetPeriod?: Period;
}

interface onsetRange {
  onsetRange?: Range;
}

interface onsetString {
  onsetString?: string;
}

interface abatementDateTime {
  onsetDateTime?: dateTime;
}

interface abatementAge {
  onsetAge?: Age;
}

interface abatementPeriod {
  onsetPeriod?: Period;
}

interface abatementRange {
  onsetRange?: Range;
}

interface abatementString {
  onsetString?: string;
}
