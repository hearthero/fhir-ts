import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, ContactPoint, Identifier, Period } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { code } from '../primitives'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Organization } from '../administration/organization'
import { Condition } from './condition'
import {
  CareTeamCategory,
  CareTeamStatus,
  ParticipantRoles,
  SNOMEDCTClinicalFindings
} from '../value-sets/value-sets-4-0'

/*
The Care Team includes all the people and organizations who plan to participate in the coordination and delivery of care for a patient.
 */
export interface CareTeam extends DomainResource {
  identifier?: Identifier[];
  status?: code<CareTeamStatus>;
  category?: CodeableConcept<CareTeamCategory>[];
  name?: string;
  subject?: Reference<Patient | Group>;
  encounter?: Reference<Encounter>;
  period?: Period;
  participant?: BackboneElement & {
    role?: CodeableConcept<ParticipantRoles>[];
    member?: Reference<Practitioner | PractitionerRole | RelatedPerson | Patient | Organization | CareTeam>;
    onBehalfOf?: Reference<Organization>;
    period?: Period;
  }[];
  reasonCode?: CodeableConcept<SNOMEDCTClinicalFindings>[];
  reasonReference?: Reference<Condition>[];
  managingOrganization?: Reference<Organization>[];
  telecom?: ContactPoint[];
  note?: Annotation[];
}
