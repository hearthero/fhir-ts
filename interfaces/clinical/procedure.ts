import { DomainResource } from '../foundation/framework/domain-resource'
import { Age, Annotation, CodeableConcept, Identifier, Period, Range } from '../foundation/base-types/general-purpose'
import { canonical, code, dateTime, uri } from '../primitives'
import { Questionaire } from '../foundation/content-management/questionaire'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { RelatedPerson } from '../administration/related-person'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { Device } from '../administration/device'
import { Location } from '../administration/location'
import { Condition } from './condition'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Composition } from '../foundation/content-management/composition'
import { Substance } from '../administration/substance'
import {
  Condition_Problem_DiagnosisCodes,
  EventStatus,
  FHIRDeviceTypes,
  ProcedureCategoryCodes_SNOMEDCT,
  ProcedureCodes_SNOMEDCT, ProcedureDeviceActionCodes, ProcedureFollowUpCodes_SNOMEDCT,
  ProcedureNotPerformedReason_SNOMEDCT, ProcedureOutcomeCodes_SNOMEDCT, ProcedurePerformerRoleCodes,
  ProcedureReasonCodes,
  SNOMEDCTBodyStructures
} from '../value-sets/value-sets-4-0'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { Observation } from '../diagnostics/observation'
import { CarePlan } from './careplan'
import { OperationDefinition } from '../conformance/operation-definition'
import { ServiceRequest } from '../workflow/service-request'
import { PlanDefinition } from '../workflow/plan-definition'
import { Measure } from '../clinical-reasoning/measure'
import { ActivityDefinition } from '../workflow/activity-definition'
import { Medication } from '../medications/medication'

/*
An action that is or was performed on or for a patient. This can be a physical intervention like an operation, or less invasive like long term services, counseling, or hypnotherapy.
 */
export type Procedure = ProcedureBase & (performedDateTime | performedPeriod | performedString | performedAge | performedRange);

interface ProcedureBase extends DomainResource {
  identifier?: Identifier[];
  instantiatesCanonical?: canonical<PlanDefinition | ActivityDefinition | Measure | OperationDefinition | Questionaire>[];
  instantiatesUri?: uri[];
  basedOn?: Reference<CarePlan | ServiceRequest>[];
  partOf?: Reference<Procedure | Observation>[];
  status: code<EventStatus>;
  statusReason?: CodeableConcept<ProcedureNotPerformedReason_SNOMEDCT>;
  category?: CodeableConcept<ProcedureCategoryCodes_SNOMEDCT>;
  code?: CodeableConcept<ProcedureCodes_SNOMEDCT>;
  subject: Reference<Patient | Group>;
  encounter?: Reference<Encounter>;
  recorder?: Reference<Patient | RelatedPerson | Practitioner | PractitionerRole>;
  asserter?: Reference<Patient | RelatedPerson | Practitioner | PractitionerRole>;
  performer?: BackboneElement & {
    function?: CodeableConcept<ProcedurePerformerRoleCodes>;
    actor: Reference<Practitioner | PractitionerRole | Organization | Patient | RelatedPerson | Device>;
    onBehalfOf?: Reference<Organization>;
  }[];
  location?: Reference<Location>;
  reasonCode?: CodeableConcept<ProcedureReasonCodes>[];
  reasonReference?: Reference<Condition | Observation | Procedure | DiagnosticReport | DocumentReference>[];
  bodySite?: CodeableConcept<SNOMEDCTBodyStructures>;
  outcome?: CodeableConcept<ProcedureOutcomeCodes_SNOMEDCT>;
  report?: Reference<DiagnosticReport | DocumentReference | Composition>[];
  complication?: CodeableConcept<Condition_Problem_DiagnosisCodes>[];
  complicationDetail?: Reference<Condition>[];
  followUp?: CodeableConcept<ProcedureFollowUpCodes_SNOMEDCT>[];
  note?: Annotation[];
  focalDevice?: BackboneElement & {
    action?: CodeableConcept<ProcedureDeviceActionCodes>;
    manipulated: Reference<Device>;
  }[];
  usedReference?: Reference<Device | Medication | Substance>[];
  usedCode?: CodeableConcept<FHIRDeviceTypes>[];
}

interface performedDateTime {
  performedDateTime?: dateTime;
}

interface performedPeriod {
  performedPeriod?: Period;
}

interface performedString {
  performedString?: string;
}

interface performedAge {
  performedAge?: Age;
}

interface performedRange {
  performedRange?: Range;
}
