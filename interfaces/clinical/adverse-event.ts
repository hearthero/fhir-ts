import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier } from '../foundation/base-types/general-purpose'
import { code, dateTime } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Practitioner } from '../administration/practitioner'
import { RelatedPerson } from '../administration/related-person'
import { Encounter } from '../administration/encounter'
import { Condition } from './condition'
import { Location } from '../administration/location'
import { PractitionerRole } from '../administration/practitioner-role'
import { Device } from '../administration/device'
import { AllergyIntolerance } from './allergy-intolerance'
import { FamilyMemberHistory } from './family-member-history'
import { Procedure } from './procedure'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Substance } from '../administration/substance'
import { Observation } from '../diagnostics/observation'
import {
  AdverseEventActuality,
  AdverseEventCategory, AdverseEventCausalityAssessment, AdverseEventCausalityMethod, AdverseEventOutcome,
  AdverseEventSeriousness, AdverseEventSeverity,
  SNOMEDCTClinicalFindings
} from '../value-sets/value-sets-4-0'
import { Media } from '../diagnostics/media'
import { Immunization } from '../medications/immunization'
import { MedicationAdministration } from '../medications/medication-administration'
import { MedicationStatement } from '../medications/medication-statement'
import { Medication } from '../medications/medication'
import { ResearchStudy } from '../administration/research-study'

/*
Actual or potential/avoided event causing unintended physical injury resulting from or contributed to by medical care, a research study or other healthcare setting factors that requires additional monitoring, treatment, or hospitalization, or that results in death.
 */
export interface AdverseEvent extends DomainResource {
  identifier?: Identifier;
  actuality: code<AdverseEventActuality>;
  category?: CodeableConcept<AdverseEventCategory>[];
  event?: CodeableConcept<SNOMEDCTClinicalFindings>;
  subject: Reference<Patient | Group | Practitioner | RelatedPerson>;
  encounter?: Reference<Encounter>;
  date?: dateTime;
  detected?: dateTime;
  recordedDate?: dateTime;
  resultingCondition?: Reference<Condition>[];
  location?: Reference<Location>;
  seriousness?: CodeableConcept<AdverseEventSeriousness>;
  severity?: CodeableConcept<AdverseEventSeverity>;
  outcome?: CodeableConcept<AdverseEventOutcome>;
  recorder?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson>;
  contributor?: Reference<Practitioner | PractitionerRole | Device>[];
  suspectEntity?: BackboneElement & {
    instance: Reference<Immunization | Procedure | Substance | Medication | MedicationAdministration | MedicationStatement | Device>;
    causality?: BackboneElement & {
      assessment?: CodeableConcept<AdverseEventCausalityAssessment>;
      productRelatedness?: string;
      author?: Reference<Practitioner | PractitionerRole>;
      method?: CodeableConcept<AdverseEventCausalityMethod>;
    }[];
  }[];
  subjectMedicalHistory?: Reference<Condition | Observation | AllergyIntolerance | FamilyMemberHistory | Immunization | Procedure | Media | DocumentReference>[];
  referenceDocument?: Reference<DocumentReference>[];
  study?: Reference<ResearchStudy>[];
}
