import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept, Duration,
  Identifier,
  Quantity,
  Range,
  Ratio
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Organization } from '../administration/organization'
import { code, date, integer } from '../primitives'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Condition } from './condition'
import {
  GoalAchievementStatus, GoalCategory,
  GoalLifecycleStatus, GoalPriority,
  LOINCCodes,
  SNOMEDCTClinicalFindings
} from '../value-sets/value-sets-4-0'
import { Observation } from '../diagnostics/observation'
import { ServiceRequest } from '../workflow/service-request'
import { RiskAssessment } from './risk-assessment'
import { NutritionOrder } from '../workflow/nutrition-order'
import { MedicationStatement } from '../medications/medication-statement'

/*
Describes the intended objective(s) for a patient, group or organization care, for example, weight loss, restoring an activity of daily living, obtaining herd immunity via immunization, meeting a process improvement objective, etc.
 */
export type Goal = GoalBase & (startDate | startCodeableConcept);

interface GoalBase extends DomainResource {
  identifier?: Identifier[];
  lifecycleStatus?: code<GoalLifecycleStatus>;
  achievementStatus?: CodeableConcept<GoalAchievementStatus>;
  category?: CodeableConcept<GoalCategory>[];
  priority?: CodeableConcept<GoalPriority>;
  description: CodeableConcept<SNOMEDCTClinicalFindings>;
  subject: Reference<Patient | Group | Organization>;
  target?: targetObj[];
  statusDate?: date;
  statusReason?: string;
  expressedBy?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson>;
  addresses?: Reference<Condition | Observation | MedicationStatement | NutritionOrder | ServiceRequest | RiskAssessment>[];
  note?: Annotation[];
  outcomeCode?: CodeableConcept<SNOMEDCTClinicalFindings>[];
  outcomeReference?: Reference<Observation>[];
}

interface startDate {
  startDate?: date;
}

interface startCodeableConcept {
  startCodeableConcept?: CodeableConcept<any>;
}

type targetObj = targetObjBase & (detailQuantity | detailRange | detailCodeableConcept | detailString | detailBoolean | detailInteger | detailRatio) & (dueDate | dueDuration);

interface targetObjBase extends BackboneElement {
  measure?: CodeableConcept<LOINCCodes>;
}

interface detailQuantity {
  detailQuantity?: Quantity;
}

interface detailRange {
  detailRange?: Range;
}

interface detailCodeableConcept {
  detailCodeableConcept?: CodeableConcept<any>;
}

interface detailString {
  detailString?: string;
}

interface detailBoolean {
  detailBoolean?: boolean;
}

interface detailInteger {
  detailInteger?: integer;
}

interface detailRatio {
  detailRatio?: Ratio;
}

interface dueDate {
  dueDate?: date;
}

interface dueDuration {
  dueDuration?: Duration;
}
