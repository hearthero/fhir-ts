import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { code, dateTime, uri } from '../primitives'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Condition } from './condition'
import { AllergyIntolerance } from './allergy-intolerance'
import { QuestionaireResponse } from '../foundation/content-management/questionaire-response'
import { FamilyMemberHistory } from './family-member-history'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { RiskAssessment } from './risk-assessment'
import { ImagingStudy } from '../diagnostics/imaging-study'
import { Media } from '../diagnostics/media'
import {
  ClinicalImpressionPrognosis, ClinicalImpressionStatus,
  Condition_Problem_DiagnosisCodes,
  InvestigationType
} from '../value-sets/value-sets-4-0'

/*
A record of a clinical assessment performed to determine what problem(s) may affect the patient and before planning the treatments or management strategies that are best to manage a patient's condition. Assessments are often 1:1 with a clinical consultation / encounter, but this varies greatly depending on the clinical workflow. This resource is called "ClinicalImpression" rather than "ClinicalAssessment" to avoid confusion with the recording of assessment tools such as Apgar score.
 */
export type ClinicalImpression = ClinicalImpressionBase & (effectiveDateTime | effectivePeriod);

interface ClinicalImpressionBase extends DomainResource {
  identifier?: Identifier[];
  status: code<ClinicalImpressionStatus>;
  statusReason?: CodeableConcept<any>;
  code?: CodeableConcept<any>;
  description?: string;
  subject: Reference<Patient | Group>;
  encounter?: Reference<Encounter>;
  date?: dateTime;
  assessor?: Reference<Practitioner | PractitionerRole>;
  previous?: Reference<ClinicalImpression>;
  problem?: Reference<Condition | AllergyIntolerance>[];
  investigation?: BackboneElement & {
    code: CodeableConcept<InvestigationType>;
    item?: Reference<Observation | QuestionaireResponse | FamilyMemberHistory | DiagnosticReport | RiskAssessment | ImagingStudy | Media>[];
  }[];
  protocol?: uri[];
  summary?: string;
  finding?: BackboneElement & {
    itemCodeableConcept?: CodeableConcept<Condition_Problem_DiagnosisCodes>;
    itemReference?: Reference<Condition | Observation | Media>;
    basis?: string;
  }[];
  prognosisCodeableConcept?: CodeableConcept<ClinicalImpressionPrognosis>[];
  prognosisReference?: Reference<RiskAssessment>[];
  supportingInfo?: Reference<any>;
  note?: Annotation[];
}

interface effectiveDateTime {
  effectiveDateTime?: dateTime;
}

interface effectivePeriod {
  effectivePeriod?: Period;
}
