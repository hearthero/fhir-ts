import { DomainResource } from '../foundation/framework/domain-resource'
import {
  CapabilityStatementKind,
  ConditionalDeleteStatus, ConditionalReadStatus,
  DocumentMode, EventCapabilityMode,
  FHIRVersion,
  Jurisdiction, MessageTransport,
  MimeType,
  PublicationStatus, ReferenceHandlingPolicy,
  ResourceType, ResourceVersionPolicy, RestfulCapabilityMode, RestfulSecurityService,
  SearchParamType, SystemRestfulInteraction, TypeRestfulInteraction
} from '../value-sets/value-sets-4-0'
import { ArrayOfOneOrMore, canonical, code, dateTime, markdown, unsignedInt, uri, url } from '../primitives'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { CodeableConcept, Coding } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { StructureDefinition } from './structure-definition'
import { MessageDefinition } from '../foundation/data-exchange/message-definition'
import { SearchParameter } from './search-parameter'
import { OperationDefinition } from './operation-definition'
import { ImplementationGuide } from './implementation-guide'
import { CompartmentDefinition } from './compartment-definition'
import { Organization } from '../administration/organization'

/*
A Capability Statement documents a set of capabilities (behaviors) of a FHIR Server for a particular version of FHIR that may be used as a statement of actual server functionality or a statement of required or desired server implementation.
 */
export interface CapabilityStatement extends DomainResource {
  url?: uri;
  version?: string;
  name?: string;
  title?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[]; // TRIAL
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  copyright?: markdown;
  kind: code<CapabilityStatementKind>;
  instantiates?: canonical<CapabilityStatement>;
  imports?: canonical<CapabilityStatement>; // TRIAL
  software?: BackboneElement & {
    name: string;
    version?: string;
    releaseDate?: dateTime;
  }[];
  implementation?: BackboneElement & {
    description: string;
    url?: url;
    custodian?: Reference<Organization>; // TRIAL
  };
  fhirVersion: code<FHIRVersion>;
  format: ArrayOfOneOrMore<code<MimeType>[]>;
  patchFormat?: code<MimeType>[];
  implementationGuide?: canonical<ImplementationGuide>[];
  rest?: BackboneElement & {
    mode: code<RestfulCapabilityMode>;
    documentation?: markdown;
    security?: BackboneElement & { // TRIAL
      cors?: boolean;
      service?: CodeableConcept<RestfulSecurityService>[];
      description?: markdown;
    };
    resource?: BackboneElement & {
      type: code<ResourceType>;
      profile?: canonical<StructureDefinition>;
      supportedProfile?: canonical<StructureDefinition>[]; // TRIAL
      documentation?: markdown;
      interaction?: BackboneElement & {
        code: code<TypeRestfulInteraction>;
        documentation?: markdown;
      };
      versioning?: code<ResourceVersionPolicy>; // TRIAL
      readHistory?: boolean; // TRIAL
      updateCreate?: boolean; // TRIAL
      conditionalCreate?: boolean; // TRIAL
      conditionalRead?: code<ConditionalReadStatus>; // TRIAL
      conditionalUpdate?: boolean; // TRIAL
      conditionalDelete?: code<ConditionalDeleteStatus>; // TRIAL
      referencePolicy?: code<ReferenceHandlingPolicy>[]; // TRIAL
      searchInclude?: string[]; // TRIAL
      searchRevInclude?: string[]; // TRIAL
      searchParam?: searchParamObj[];
      operation?: operationObj[];
    }
    interaction?: BackboneElement & {
      code: code<SystemRestfulInteraction>;
      documentation?: markdown;
    };
    searchParam?: searchParamObj[];
    operation?: operationObj[];
    compartment?: canonical<CompartmentDefinition>[];
  };
  messaging?: BackboneElement & { // TRIAL
    endpoint?: BackboneElement & {
      protocol: Coding<MessageTransport>;
      address: url;
    }[];
    reliableCache?: unsignedInt;
    documentation?: markdown;
    supportedMessage?: BackboneElement & {
      mode: code<EventCapabilityMode>;
      definition: canonical<MessageDefinition>;
    }[];
  }
  document?: BackboneElement & { // TRIAL
    mode: code<DocumentMode>;
    documentation?: markdown;
    profile: canonical<StructureDefinition>;
  }[]
}

interface searchParamObj extends BackboneElement {
  name: string;
  definition?: canonical<SearchParameter>;
  type: code<SearchParamType>;
  documentation?: markdown;
}

interface operationObj extends BackboneElement {
  name: string;
  definition: canonical<OperationDefinition>;
  documentation?: markdown;
}
