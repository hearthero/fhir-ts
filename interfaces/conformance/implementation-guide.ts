import { DomainResource } from '../foundation/framework/domain-resource'
import { ArrayOfOneOrMore, canonical, code, dateTime, id, markdown, uri, url } from '../primitives'
import {
  FHIRVersion, GuidePageGeneration, GuideParameterCode,
  Jurisdiction,
  PublicationStatus,
  ResourceType, SPDXLicense
} from '../value-sets/value-sets-4-0'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { CodeableConcept } from '../foundation/base-types/general-purpose'
import { StructureDefinition } from './structure-definition'
import { Binary } from '../foundation/framework/binary'

/*
A set of rules of how a particular interoperability or standards problem is solved - typically through the use of FHIR resources. This resource is used to gather all the parts of an implementation guide into a logical whole and to publish a computable definition of all the parts.
 */
export interface ImplementationGuide extends DomainResource {
  url: uri;
  version?: string;
  name: string;
  title?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  copyright?: markdown;
  packageId: id;
  license?: code<SPDXLicense>;
  fhirVersion?: ArrayOfOneOrMore<code<FHIRVersion>>;
  dependsOn?: BackboneElement & {
    uri: canonical<ImplementationGuide>;
    packageId?: id;
    version?: string;
  }[];
  global?: BackboneElement & {
    type: code<ResourceType>;
    profile: canonical<StructureDefinition>;
  };
  definition?: BackboneElement & {
    grouping?: BackboneElement & {
      name: string;
      description?: string;
    }[];
    resource: ArrayOfOneOrMore<resourceObjForDefinition & (exampleBoolean | exampleCanonical)>;
    page?: pageObj;
    parameter?: BackboneElement & {
      code: code<GuideParameterCode>;
      value: string;
    }[];
    template?: BackboneElement & {
      code: code;
      source: string;
      scope?: string;
    }[];
  };
  manifest?: BackboneElement & {
    rendering?: url;
    resource: ArrayOfOneOrMore<resourceObjForManifest & (exampleBoolean | exampleCanonical)>;
    page?: BackboneElement & {
      name: string;
      title?: string;
      anchor?: string;
    }[];
    image?: string[];
    other?: string[];
  }
}

interface resourceObjForDefinition extends BackboneElement {
  reference: Reference<any>;
  fhirVersion?: code<FHIRVersion>[];
  name?: string;
  description?: string;
}

interface resourceObjForManifest extends BackboneElement {
  reference: Reference<any>;
  relativePath?: url;
}

interface pageObjBare extends BackboneElement {
  title: string;
  generation: code<GuidePageGeneration>;
  page?: pageObj[];
}

interface nameUrl {
  nameUrl?: url;
  nameReference: never;
}

interface nameReference {
  nameUrl: never;
  nameReference?: Reference<Binary>;
}

type pageObj = pageObjBare & (nameUrl | nameReference);

interface exampleBoolean {
  exampleBoolean?: boolean;
  exampleCanonical: never;
}

interface exampleCanonical {
  exampleBoolean: never;
  exampleCanonical?: canonical<StructureDefinition>;
}
