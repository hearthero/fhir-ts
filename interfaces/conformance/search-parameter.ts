import { DomainResource } from '../foundation/framework/domain-resource'
import { ArrayOfOneOrMore, canonical, code, dateTime, markdown, uri } from '../primitives'
import {
  Jurisdiction,
  PublicationStatus,
  ResourceType,
  SearchComparator, SearchModifierCode,
  SearchParamType, XPathUsageType
} from '../value-sets/value-sets-4-0'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { CodeableConcept } from '../foundation/base-types/general-purpose'
import { BackboneElement } from '../foundation/base-types/special-purpose'

/*
A search parameter that defines a named search item that can be used to search/filter on a resource.
 */
export interface SearchParameter extends DomainResource {
  url: uri;
  version?: string;
  name: string;
  derivedFrom?: canonical<SearchParameter>;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>;
  purpose?: markdown;
  code: code;
  base: ArrayOfOneOrMore<code<ResourceType>[]>;
  type: code<SearchParamType>;
  expression?: string;
  xpath?: string;
  xpathUsage?: code<XPathUsageType>;
  target?: code<ResourceType>[];
  multipleOr?: boolean;
  multipleAnd?: boolean;
  comparator?: code<SearchComparator>[];
  modifier?: code<SearchModifierCode>[];
  chain?: string[];
  component?: BackboneElement & {
    definition: canonical<SearchParameter>;
    expression: string;
  }[];
}
