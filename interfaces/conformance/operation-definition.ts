import { DomainResource } from '../foundation/framework/domain-resource'
import {
  BindingStrength, FHIRAllTypes,
  Jurisdiction, OperationKind, OperationParameterUse,
  PublicationStatus,
  ResourceType,
  SearchParamType
} from '../value-sets/value-sets-4-0'
import { canonical, code, dateTime, integer, markdown, uri } from '../primitives'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { CodeableConcept } from '../foundation/base-types/general-purpose'
import { StructureDefinition } from './structure-definition'
import { BackboneElement } from '../foundation/base-types/special-purpose'
import { ValueSet } from '../terminology/value-set'

interface parameterObj extends BackboneElement {
  name: code;
  use: code<OperationParameterUse>;
  min: integer;
  max: integer;
  documentation?: string;
  type?: code<FHIRAllTypes>;
  targetProfile?: canonical<StructureDefinition>[];
  searchType?: code<SearchParamType>;
  binding?: BackboneElement & {
    strength: code<BindingStrength>;
    valueSet: canonical<ValueSet>;
  };
  referencedFrom?: BackboneElement & {
    source: string;
    sourceId?: string;
  }[];
  part?: parameterObj[];
}

/*
A formal computable definition of an operation (on the RESTful interface) or a named query (using the search interaction).
 */
export interface OperationDefinition extends DomainResource {
  url?: uri;
  version?: string;
  name: string;
  title?: string;
  status: code<PublicationStatus>;
  kind: code<OperationKind>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[]; // TRIAL
  jurisdiction: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  affectsState?: boolean;
  code: code;
  comment?: markdown;
  base?: canonical<OperationDefinition>;
  resource?: code<ResourceType>[];
  system: boolean;
  type: boolean;
  instance: boolean;
  inputProfile?: canonical<StructureDefinition>;
  outputProfile?: canonical<StructureDefinition>;
  parameter?: parameterObj[];
  overload?: BackboneElement & {
    parameterName?: string[];
    comment?: string;
  }[];
}
