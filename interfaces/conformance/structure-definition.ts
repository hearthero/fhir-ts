import { DomainResource } from '../foundation/framework/domain-resource'
import { canonical, code, dateTime, id, markdown, uri } from '../primitives'
import { CodeableConcept, Coding, Identifier } from '../foundation/base-types/general-purpose'
import {
  DefinitionUseCodes, ExtensionContextType, FHIRDefinedType,
  FHIRVersion,
  Jurisdiction,
  PublicationStatus,
  StructureDefinitionKind, TypeDerivationRule
} from '../value-sets/value-sets-4-0'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { BackboneElement, ElementDefinition } from '../foundation/base-types/special-purpose'

/*
A definition of a FHIR structure. This resource is used to describe the underlying resources, data types defined in FHIR, and also for describing extensions and constraints on resources and data types.
 */
export interface StructureDefinition extends DomainResource {
  url: uri;
  identifier?: Identifier[];
  version?: string;
  name: string;
  title?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  copyright?: markdown;
  keyword?: Coding<DefinitionUseCodes>[];
  fhirVersion?: code<FHIRVersion>;
  mapping?: BackboneElement & {
    identity: id;
    uri?: uri;
    name?: string;
    comment?: string;
  }[];
  kind: code<StructureDefinitionKind>;
  abstract: boolean;
  context?: BackboneElement & {
    type: code<ExtensionContextType>;
    expression: string;
  }[];
  contextInvariant?: string[];
  type: uri<FHIRDefinedType>;
  baseDefinition?: canonical<StructureDefinition>;
  derivation: code<TypeDerivationRule>;
  snapshot?: BackboneElement & {
    element: ElementDefinition[];
  };
  differential: BackboneElement & {
    element: ElementDefinition[];
  };
}
