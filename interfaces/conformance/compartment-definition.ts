import { DomainResource } from '../foundation/framework/domain-resource'
import { CompartmentType, PublicationStatus, ResourceType } from '../value-sets/value-sets-4-0'
import { code, dateTime, markdown, uri } from '../primitives'
import { BackboneElement } from '../foundation/base-types/special-purpose'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'

/*
A compartment definition that defines how resources are accessed on a server.
 */
export interface CompartmentDefinition extends DomainResource {
  url: uri;
  version?: string;
  name: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  purpose?: markdown;
  code: code<CompartmentType>;
  search: boolean;
  resource?: BackboneElement & {
    code: code<ResourceType>;
    param?: string[];
    documentation?: string;
  }
}
