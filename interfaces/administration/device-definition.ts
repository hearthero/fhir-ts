import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  ContactPoint,
  Identifier,
  Quantity
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { code, uri } from '../primitives'
import { Organization } from './organization'
import { DeviceNameType, DeviceSafety, FHIRDeviceTypes } from '../value-sets/value-sets-4-0'
import { ProductShelfLife } from '../medications/product-shelf-life'
import { ProdCharacteristic } from '../medications/prod-characteristic'

/*
The characteristics, operational status and capabilities of a medical-related component of a medical device.
 */
export type DeviceDefinition = DeviceDefinitionBase & (manufacturerString | manufacturerReference);

interface DeviceDefinitionBase extends DomainResource {
  identifier?: Identifier[];
  udiDeviceIdentifier?: BackboneElement & {
    deviceIdentifier?: string;
    issuer?: uri;
    jurisdiction?: uri;
  }[];
  deviceName?: BackboneElement & {
    name: string;
    type: code<DeviceNameType>;
  }[];
  modelNumber?: string;
  type?: CodeableConcept<FHIRDeviceTypes>;
  specialization?: BackboneElement & {
    systemType: string;
    version?: string;
  }[];
  version?: string[];
  safety?: CodeableConcept<DeviceSafety>[];
  shelfLifeStorage?: ProductShelfLife[];
  physicalCharacteristics?: ProdCharacteristic;
  languageCode?: CodeableConcept<any>[];
  capability?: BackboneElement & {
    type: CodeableConcept<any>;
    description?: CodeableConcept<any>;
  }[];
  property?: BackboneElement & {
    type: CodeableConcept<any>;
    valueQuantity?: Quantity[];
    valueCode?: CodeableConcept<any>;
  }[];
  owner?: Reference<Organization>;
  contact?: ContactPoint[];
  url?: uri;
  onlineInformation?: uri;
  note?: Annotation[];
  quantity?: Quantity;
  parentDevice?: Reference<DeviceDefinition>;
  material?: BackboneElement & {
    substance: CodeableConcept<any>;
    alternate?: boolean;
    allergenicIndicator?: boolean;
  }[];
}

interface manufacturerString {
  manufacturerString?: string;
}

interface manufacturerReference {
  manufacturerReference?: Reference<Organization>;
}
