import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Address,
  Attachment,
  CodeableConcept,
  ContactPoint,
  HumanName,
  Identifier, Period
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import {
  AdministrativeGender,
  CommonLanguages,
  LinkType,
  MaritalStatus,
  PatientContactRelationship
} from '../value-sets/value-sets-4-0'
import { code, date, dateTime, integer } from '../primitives'
import { RelatedPerson } from './related-person'
import { Organization } from './organization'
import { Practitioner } from './practitioner'
import { PractitionerRole } from './practitioner-role'

/*
Demographics and other administrative information about an individual or animal receiving care or other health-related services.
 */
export type Patient = PatientBase & (deceasedBoolean | deceasedDateTime) & (multipleBirthBoolean | multipleBirthInteger);

interface PatientBase extends DomainResource {
  identifier?: Identifier[];
  active?: boolean;
  name?: HumanName[];
  telecom?: ContactPoint[];
  gender?: code<AdministrativeGender>;
  birthDate?: date;
  address?: Address[];
  maritalStatus?: CodeableConcept<MaritalStatus>;
  photo?: Attachment[];
  contact?: BackboneElement & {
    relationship?: CodeableConcept<PatientContactRelationship>[];
    name?: HumanName;
    telecom?: ContactPoint[];
    address?: Address;
    gender?: code<AdministrativeGender>;
    organization?: Reference<Organization>;
    period?: Period;
  }[];
  communication?: BackboneElement & {
    language: CodeableConcept<CommonLanguages>;
    preferred?: boolean;
  }[];
  generalPractitioner?: Reference<Organization | Practitioner | PractitionerRole>[];
  managingOrganization?: Reference<Organization>;
  link?: BackboneElement & {
    other: Reference<Patient | RelatedPerson>;
    type: code<LinkType>;
  }
}

interface deceasedBoolean {
  deceasedBoolean?: boolean;
}

interface deceasedDateTime {
  deceasedDateTime: dateTime;
}

interface multipleBirthBoolean {
  multipleBirthBoolean?: boolean;
}

interface multipleBirthInteger {
  multipleBirthInteger?: integer;
}
