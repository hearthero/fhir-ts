import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { code } from '../primitives'
import { Reference } from '../foundation/base-types/special-purpose'
import { Patient } from './patient'
import { Location } from './location'
import { Group } from './group'
import { Organization } from './organization'
import { Practitioner } from './practitioner'
import { Encounter } from './encounter'
import { PractitionerRole } from './practitioner-role'
import { Medication } from '../medications/medication'
import { Procedure } from '../clinical/procedure'
import { PlanDefinition } from '../workflow/plan-definition'
import { Device } from './device'
import { FlagCategory, FlagCode, FlagStatus } from '../value-sets/value-sets-4-0'

/*
Prospective warnings of potential issues when providing care to the patient.
 */
export interface Flag extends DomainResource {
  identifier?: Identifier[];
  status: code<FlagStatus>;
  category?: CodeableConcept<FlagCategory>[];
  code?: CodeableConcept<FlagCode>;
  subject: Reference<Patient | Location | Group | Organization | Practitioner | PlanDefinition | Medication | Procedure>;
  period?: Period;
  encounter?: Reference<Encounter>;
  author?: Reference<Device | Organization | Patient | Practitioner | PractitionerRole>;
}
