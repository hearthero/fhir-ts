import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Attachment,
  CodeableConcept,
  ContactPoint,
  Identifier, Period
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import {
  CommonLanguages,
  DaysOfWeek,
  PracticeSettingCodeValueSet, Program, ReferralMethod,
  ServiceCategory, ServiceProvisionConditions,
  ServiceType
} from '../value-sets/value-sets-4-0'
import { code, markdown, time } from '../primitives'
import { Organization } from './organization'
import { Location } from './location'
import { Endpoint } from './endpoint'

/*
The details of a healthcare service available at a location.
 */
export interface HealthcareService extends DomainResource {
  identifier?: Identifier[];
  active?: boolean;
  providedBy?: Reference<Organization>;
  category?: CodeableConcept<ServiceCategory>[];
  type?: CodeableConcept<ServiceType>[];
  speciality?: CodeableConcept<PracticeSettingCodeValueSet>;
  location?: Reference<Location>;
  name?: string;
  comment?: string;
  extraDetails?: markdown;
  photo?: Attachment;
  telecom?: ContactPoint[];
  coverageArea?: Reference<Location>[];
  serviceProvisionCode?: CodeableConcept<ServiceProvisionConditions>[];
  eligibility?: BackboneElement & {
    code?: CodeableConcept<any>;
    comment?: markdown;
  }
  program?: CodeableConcept<Program>[];
  characteristic?: CodeableConcept<any>;
  communication?: CodeableConcept<CommonLanguages>;
  referralMethod?: CodeableConcept<ReferralMethod>;
  appointmentRequired?: boolean;
  availableTime?: BackboneElement & {
    daysOfWeek?: code<DaysOfWeek>[];
    allDay?: boolean;
    availableStartTime?: time;
    availableEndTime?: time;
  }[];
  notAvailable?: BackboneElement & {
    description: string;
    during?: Period;
  }[];
  availabilityExceptions?: string;
  endpoint?: Reference<Endpoint>[]
}
