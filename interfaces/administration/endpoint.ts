import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Coding, ContactPoint, Identifier, Period } from '../foundation/base-types/general-purpose'
import { Organization } from './organization'
import { ArrayOfOneOrMore, code, url } from '../primitives'
import { EndpointConnectionType, EndpointPayloadType, EndpointStatus, MimeType } from '../value-sets/value-sets-4-0'
import { Reference } from '../foundation/base-types/special-purpose'

/*
The technical details of an endpoint that can be used for electronic services, such as for web services providing XDS.b or a REST endpoint for another FHIR server. This may include any security context information.
 */
export interface Endpoint extends DomainResource {
  identifier?: Identifier[];
  status: code<EndpointStatus>;
  connectionType: Coding<EndpointConnectionType>;
  name?: string;
  managingOrganization?: Reference<Organization>;
  contact?: ContactPoint[];
  period?: Period;
  payloadType: ArrayOfOneOrMore<CodeableConcept<EndpointPayloadType>>;
  payloadMimeType?: code<MimeType>[];
  address: url;
  header?: string[];
}
