import { DomainResource } from '../foundation/framework/domain-resource'
import { code, dateTime } from '../primitives'
import { Annotation, Attachment, CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import {
  CommunicationCategory,
  RequestPriority,
  RequestStatus, v3_ActReason,
  v3_ParticipationMode
} from '../value-sets/value-sets-4-0'
import { Patient } from './patient'
import { Group } from './group'
import { Encounter } from './encounter'
import { Practitioner } from './practitioner'
import { PractitionerRole } from './practitioner-role'
import { Organization } from './organization'
import { RelatedPerson } from './related-person'
import { Device } from './device'
import { CareTeam } from '../clinical/careteam'
import { HealthcareService } from './healthcare-service'
import { Condition } from '../clinical/condition'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { DocumentReference } from '../foundation/content-management/document-reference'

/*
A request to convey information; e.g. the CDS system proposes that an alert be sent to a responsible provider, the CDS system proposes that the public health agency be notified about a reportable condition.
 */
export type CommunicationRequest = CommunicationRequestBase & occurrence;

interface CommunicationRequestBase extends DomainResource {
  identifier?: Identifier[];
  basedOn?: Reference<any>[];
  replaces?: Reference<CommunicationRequest>[];
  groupIdentifier?: Identifier;
  status: code<RequestStatus>;
  statusReason?: CodeableConcept<any>;
  category?: CodeableConcept<CommunicationCategory>[];
  priority?: code<RequestPriority>;
  doNotPerform?: boolean;
  medium?: CodeableConcept<v3_ParticipationMode>[];
  subject?: Reference<Patient | Group>;
  about?: Reference<any>[];
  encounter?: Reference<Encounter>;
  payload?: (BackboneElement & content)[];
  authoredOn?: dateTime;
  requester?: Reference<Practitioner | PractitionerRole | Organization | Patient | RelatedPerson | Device>;
  recipient?: Reference<Device | Organization | Patient | Practitioner | PractitionerRole | RelatedPerson | Group | CareTeam | HealthcareService>[];
  sender?: Reference<Device | Organization | Patient | Practitioner | PractitionerRole | RelatedPerson | HealthcareService>;
  reasonCode?: CodeableConcept<v3_ActReason>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport | DocumentReference>[];
  note?: Annotation[];
}

type occurrence = occurrenceDateTime | occurrencePeriod;

interface occurrenceDateTime {
  occurrenceDateTime?: dateTime;
}

interface occurrencePeriod {
  occurrencePeriod?: Period;
}

type content = contentString | contentAttachment | contentReference;

interface contentString {
  contentString: string;
}

interface contentAttachment {
  contentAttachment: Attachment;
}

interface contentReference {
  contentReference: Reference<any>;
}
