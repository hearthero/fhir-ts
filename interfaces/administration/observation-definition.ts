import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Range } from '../foundation/base-types/general-purpose'
import {
  AdministrativeGender,
  LOINCCodes,
  ObservationCategoryCodes, ObservationDataType,
  ObservationMethods, ObservationRangeCategory, ObservationReferenceRangeAppliesToCodes,
  ObservationReferenceRangeMeaningCodes, UCUMCodes
} from '../value-sets/value-sets-4-0'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { code, decimal, integer } from '../primitives'
import { ValueSet } from '../terminology/value-set'

/*
Set of definitional characteristics for a kind of observation or measurement produced or consumed by an orderable health care service.
 */
export interface ObservationDefinition extends DomainResource {
  category?: CodeableConcept<ObservationCategoryCodes>[];
  code: CodeableConcept<LOINCCodes>;
  identifier?: Identifier[];
  permittedDataType?: code<ObservationDataType>[];
  multipleResultsAllowed?: boolean;
  method?: CodeableConcept<ObservationMethods>;
  preferredReportName?: string;
  quantitativeDetails?: BackboneElement & {
    customaryUnit?: CodeableConcept<UCUMCodes>;
    unit?: CodeableConcept<UCUMCodes>;
    conversionFactor?: decimal;
    decimalPrecision?: integer;
  };
  qualifiedInterval?: BackboneElement & {
    category?: code<ObservationRangeCategory>;
    context?: CodeableConcept<ObservationReferenceRangeMeaningCodes>;
    appliesTo?: CodeableConcept<ObservationReferenceRangeAppliesToCodes>[];
    gender?: code<AdministrativeGender>;
    age?: Range;
    gestationalAge?: Range;
    condition?: string;
  }[];
  validCodedValueSet?: Reference<ValueSet>;
  normalCodedValueSet?: Reference<ValueSet>;
  abnormalCodedValueSet?: Reference<ValueSet>;
  criticalCodedValueSet?: Reference<ValueSet>;
}
