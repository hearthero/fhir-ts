import { DomainResource } from '../foundation/framework/domain-resource'
import {
  CodeableConcept,
  ContactPoint,
  Identifier, Period
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { DaysOfWeek, PracticeSettingCodeValueSet } from '../value-sets/value-sets-4-0'
import { code, time } from '../primitives'
import { Practitioner } from './practitioner'
import { Organization } from './organization'
import { Location } from './location'
import { Endpoint } from './endpoint'
import { HealthcareService } from './healthcare-service'

/*
A specific set of Roles/Locations/specialties/services that a practitioner may perform at an organization for a period of time.
 */
export interface PractitionerRole extends DomainResource {
  identifier?: Identifier[];
  active?: boolean;
  period?: Period;
  practitioner?: Reference<Practitioner>;
  organization?: Reference<Organization>;
  code?: CodeableConcept<PractitionerRole>;
  speciality?: CodeableConcept<PracticeSettingCodeValueSet>;
  location?: Reference<Location>;
  healthcareService?: Reference<HealthcareService>;
  telecom?: ContactPoint[];
  availableTime?: BackboneElement & {
    daysOfWeek?: code<DaysOfWeek>[];
    allDay?: boolean;
    availableStartTime?: time;
    availableEndTime?: time;
  }[];
  notAvailable?: BackboneElement & {
    description: string;
    during?: Period;
  }[];
  availabilityExceptions?: string;
  endpoint?: Reference<Endpoint>[]
}
