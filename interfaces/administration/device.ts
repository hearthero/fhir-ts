import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  ContactPoint,
  Identifier,
  Quantity
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { base64Binary, code, dateTime, uri } from '../primitives'
import { Patient } from './patient'
import { Organization } from './organization'
import { Location } from './location'
import { DeviceDefinition } from './device-definition'
import {
  DeviceNameType,
  DeviceType,
  FHIRDeviceStatus,
  FHIRDeviceStatusReason,
  UDIEntryType
} from '../value-sets/value-sets-4-0'

/*
A type of a manufactured item that is used in the provision of healthcare without being substantially changed through that activity. The device may be a medical or non-medical device.
 */
export interface Device extends DomainResource {
  identifier?: Identifier[];
  definition?: Reference<DeviceDefinition>;
  udiCarrier?: BackboneElement & {
    deviceIdentifier?: string;
    issuer?: uri;
    jurisdiction?: uri;
    carrierAIDC?: base64Binary;
    carrierHRF?: string;
    entryType?: code<UDIEntryType>;
  }[];
  status?: code<FHIRDeviceStatus>;
  statusReason?: CodeableConcept<FHIRDeviceStatusReason>[];
  distinctIdentifier?: string;
  manufacturer?: string;
  manufacturerDate?: dateTime;
  expirationDate?: dateTime;
  lotNumber?: string;
  serialNumber?: string;
  deviceName?: BackboneElement & {
    name: string;
    type: code<DeviceNameType>;
  }[];
  modelNumber?: string;
  partNumber?: string;
  type?: CodeableConcept<DeviceType>;
  specialization?: BackboneElement & {
    systemType: CodeableConcept<any>;
    version?: string;
  }[];
  version?: BackboneElement & {
    type?: CodeableConcept<any>;
    component?: Identifier;
    value: string;
  }[];
  property?: BackboneElement & {
    type: CodeableConcept<any>;
    valueQuantity?: Quantity[];
    valueCode?: CodeableConcept<any>[];
  }[];
  patient?: Reference<Patient>;
  owner?: Reference<Organization>;
  contact?: ContactPoint[];
  location?: Reference<Location>;
  url?: uri;
  note?: Annotation[];
  safety?: CodeableConcept<any>[];
  parent?: Reference<Device>;
}
