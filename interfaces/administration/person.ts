import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Address,
  Attachment,
  ContactPoint,
  HumanName,
  Identifier
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from './patient'
import { AdministrativeGender, IdentityAssuranceLevel } from '../value-sets/value-sets-4-0'
import { code, date } from '../primitives'
import { RelatedPerson } from './related-person'
import { Practitioner } from './practitioner'
import { Organization } from './organization'

/*
Demographics and administrative information about a person independent of a specific health-related context.
 */
export interface Person extends DomainResource {
  identifier?: Identifier[];
  name?: HumanName[];
  telecom?: ContactPoint[];
  gender?: code<AdministrativeGender>;
  birthDate?: date;
  address?: Address[];
  photo?: Attachment[];
  managingOrganization?: Reference<Organization>;
  active?: boolean;
  link?: BackboneElement & {
    target: Reference<Patient | Practitioner | RelatedPerson | Person>;
    assurance?: code<IdentityAssuranceLevel>;
  }[];
}
