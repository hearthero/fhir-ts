import { DomainResource } from '../foundation/framework/domain-resource'
import { Address, CodeableConcept, Coding, ContactPoint, Identifier } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { code, decimal, time } from '../primitives'
import { Organization } from './organization'
import {
  DaysOfWeek,
  LocationMode,
  LocationStatus,
  LocationType,
  v2_0116,
  v3_ServiceDeliveryLocationRoleType
} from '../value-sets/value-sets-4-0'
import { Endpoint } from './endpoint'

/*
Details and position information for a physical place where services are provided and resources and participants may be stored, found, contained, or accommodated.
 */
export interface Location extends DomainResource {
  identifier?: Identifier[];
  status?: code<LocationStatus>;
  operationalStatus?: Coding<v2_0116>;
  name?: string;
  alias?: string[];
  description?: string;
  mode?: code<LocationMode>;
  type?: CodeableConcept<v3_ServiceDeliveryLocationRoleType>;
  telecom?: ContactPoint[];
  address?: Address;
  physicalType?: CodeableConcept<LocationType>;
  position?: BackboneElement & {
    longitude: decimal;
    latitude: decimal;
    altitude?: decimal;
  };
  managingOrganization?: Reference<Organization>;
  partOf?: Reference<Location>;
  hoursOfOperation?: BackboneElement & {
    daysOfWeek?: code<DaysOfWeek>[];
    allDay?: boolean;
    openingTime?: time;
    closingTime?: time;
  }[];
  availabilityExceptions?: string;
  endpoint?: Reference<Endpoint>;
}
