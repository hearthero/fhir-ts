import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Address,
  Attachment,
  CodeableConcept,
  ContactPoint,
  HumanName,
  Identifier, Period
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { AdministrativeGender, CommonLanguages, v2_0360_2_7 } from '../value-sets/value-sets-4-0'
import { code, date } from '../primitives'
import { Organization } from './organization'

/*
A person who is directly or indirectly involved in the provisioning of healthcare.
 */
export interface Practitioner extends DomainResource {
  identifier?: Identifier[];
  active?: boolean;
  name?: HumanName[];
  telecom?: ContactPoint[];
  address?: Address[];
  gender?: code<AdministrativeGender>;
  birthDate?: date;
  photo?: Attachment[];
  qualification?: BackboneElement & {
    identifier?: Identifier[];
    code: CodeableConcept<v2_0360_2_7>;
    period?: Period;
    issuer?: Reference<Organization>;
  }[];
  communication?: CodeableConcept<CommonLanguages>[];
}
