import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Timing } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Device } from './device'
import { code, instant } from '../primitives'
import {
  DeviceMetricAndComponentTypes, DeviceMetricCalibrationState, DeviceMetricCalibrationType, DeviceMetricCategory,
  DeviceMetricColor,
  DeviceMetricOperationalStatus
} from '../value-sets/value-sets-4-0'

/*
Describes a measurement, calculation or setting capability of a medical device.
 */

export interface DeviceMetric extends DomainResource {
  identifier?: Identifier[];
  type: CodeableConcept<DeviceMetricAndComponentTypes>;
  unit?: CodeableConcept<DeviceMetricAndComponentTypes>;
  source?: Reference<Device>;
  parent?: Reference<Device>;
  operationalStatus?: code<DeviceMetricOperationalStatus>;
  color?: code<DeviceMetricColor>;
  category: code<DeviceMetricCategory>;
  measurementPeriod?: Timing;
  calibration?: BackboneElement & {
    type?: code<DeviceMetricCalibrationType>;
    state?: code<DeviceMetricCalibrationState>;
    time?: instant;
  }[];
}
