import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Coding, Duration, Identifier, Period } from '../foundation/base-types/general-purpose'
import { code, positiveInt } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from './patient'
import { Group } from './group'
import { EpisodeOfCare } from './episode-of-care'
import { Practitioner } from './practitioner'
import { PractitionerRole } from './practitioner-role'
import { RelatedPerson } from './related-person'
import {
  AdmitSource,
  ConditionalDeleteStatus, DiagnosisRole, Diet, DischargeDisposition, EncounterLocationStatus, EncounterReasonCodes,
  EncounterStatus,
  EncounterType, LocationType, ParticipantType, ServiceType, SpecialArrangements, SpecialCourtesy, v2_0092,
  v3_ActEncounterCode, v3_ActPriority
} from '../value-sets/value-sets-4-0'
import { Location } from './location'
import { Organization } from './organization'
import { ServiceRequest } from '../workflow/service-request'
import { Appointment } from '../workflow/appointment'
import { Procedure } from '../clinical/procedure'
import { Observation } from '../diagnostics/observation'
import { ImmunizationRecommendation } from '../medications/immunization-recommendation'
import { Condition } from '../clinical/condition'
import { Account } from './account'

/*
An interaction between a patient and healthcare provider(s) for the purpose of providing healthcare service(s) or assessing the health status of a patient.
 */
export interface Encounter extends DomainResource {
  identifier?: Identifier[];
  status: code<EncounterStatus>;
  statusHistory?: BackboneElement & {
    status: code<EncounterStatus>;
    period: Period;
  }[];
  class: Coding<v3_ActEncounterCode>;
  classHistory?: BackboneElement & {
    class: Coding<v3_ActEncounterCode>;
    period: Period;
  }[];
  type?: CodeableConcept<EncounterType>;
  serviceType?: CodeableConcept<ServiceType>;
  priority?: CodeableConcept<v3_ActPriority>;
  subject?: Reference<Patient | Group>;
  episodeOfCare?: Reference<EpisodeOfCare>[];
  basedOn?: Reference<ServiceRequest>[];
  participant?: BackboneElement & {
    type?: CodeableConcept<ParticipantType>[];
    period?: Period;
    individual?: Reference<Practitioner | PractitionerRole | RelatedPerson>;
  }[];
  appointment?: Reference<Appointment>[];
  period?: Period;
  length?: Duration;
  reasonCode?: CodeableConcept<EncounterReasonCodes>[];
  reasonReference?: Reference<ConditionalDeleteStatus | Procedure | Observation | ImmunizationRecommendation>[];
  diagnosis?: BackboneElement & {
    condition: Reference<Condition | Procedure>;
    use?: CodeableConcept<DiagnosisRole>;
    rank?: positiveInt;
  }[];
  account?: Reference<Account>[];
  hospitalization?: BackboneElement & {
    preAdmissionIdentifier?: Identifier;
    origin?: Reference<Location | Organization>;
    admitSource?: CodeableConcept<AdmitSource>;
    reAdmission?: CodeableConcept<v2_0092>;
    dietPreference?: CodeableConcept<Diet>[];
    specialCourtesy?: CodeableConcept<SpecialCourtesy>[];
    specialArrangement?: CodeableConcept<SpecialArrangements>[];
    destination?: Reference<Location | Organization>;
    dischargeDisposition?: CodeableConcept<DischargeDisposition>;
  };
  location?: BackboneElement & {
    location: Reference<Location>;
    status?: code<EncounterLocationStatus>;
    physicalType?: CodeableConcept<LocationType>;
    period?: Period;
  }[];
  serviceProvider?: Reference<Organization>;
  partOf?: Reference<Encounter>;
}
