import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from './patient'
import { Practitioner } from './practitioner'
import { PractitionerRole } from './practitioner-role'
import { Location } from './location'
import { HealthcareService } from './healthcare-service'
import { Organization } from './organization'
import { code, positiveInt } from '../primitives'
import { RelatedPerson } from './related-person'
import { Device } from './device'
import { Coverage } from '../financial/coverage'
import { AccountStatus, AccountTypes } from '../value-sets/value-sets-4-0'

/*
A financial tool for tracking value accrued for a particular purpose. In the healthcare field, used to track charges for a patient, cost centers, etc.
 */
export interface Account extends DomainResource {
  identifier?: Identifier[];
  status: code<AccountStatus>;
  type?: CodeableConcept<AccountTypes>;
  name?: string;
  subject?: Reference<Patient | Device | Practitioner | PractitionerRole | Location | HealthcareService | Organization>[];
  servicePeriod?: Period;
  coverage?: BackboneElement & {
    coverage: Reference<Coverage>;
    priority?: positiveInt;
  }[];
  owner?: Reference<Organization>;
  description?: string;
  guarantor?: BackboneElement & {
    party: Reference<Patient | RelatedPerson | Organization>;
    onHold?: boolean;
    period?: Period;
  }[];
  partOf?: Reference<Account>;
}
