import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Address,
  CodeableConcept,
  ContactPoint,
  HumanName,
  Identifier
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Endpoint } from './endpoint'
import { ContactEntityType, OrganizationType } from '../value-sets/value-sets-4-0'

/*
A formally or informally recognized grouping of people or organizations formed for the purpose of achieving some form of collective action. Includes companies, institutions, corporations, departments, community groups, healthcare practice groups, payer/insurer, etc.
 */
export interface Organization extends DomainResource {
  identifier?: Identifier[];
  active?: boolean;
  type?: CodeableConcept<OrganizationType>[];
  name?: string;
  alias?: string[];
  telecom?: ContactPoint[];
  address?: Address[];
  partOf?: Reference<Organization>;
  contact?: BackboneElement & {
    purpose?: CodeableConcept<ContactEntityType>;
    name?: HumanName;
    telecom?: ContactPoint[];
    address?: Address;
  }[];
  endpoint?: Reference<Endpoint>[];
}
