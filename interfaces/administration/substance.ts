import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Ratio, SimpleQuantity } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { code, dateTime } from '../primitives'
import { FHIRSubstanceStatus, SubstanceCategoryCodes, SubstanceCode } from '../value-sets/value-sets-4-0'

/*
A homogeneous material with a definite composition.
 */
export interface Substance extends DomainResource {
  identifier?: Identifier[];
  status?: code<FHIRSubstanceStatus>;
  category?: CodeableConcept<SubstanceCategoryCodes>[];
  code: CodeableConcept<SubstanceCode>;
  description?: string;
  instance?: BackboneElement & {
    identifier?: Identifier;
    expiry?: dateTime;
    quantity?: SimpleQuantity;
  }[];
  ingredient?: ingredientObj[];
}

type ingredientObj = ingredientObjBase & (substanceCodeableConcept | substanceReference);

interface ingredientObjBase extends BackboneElement {
  quantity?: Ratio;
}

interface substanceCodeableConcept {
  substanceCodeableConcept?: CodeableConcept<SubstanceCode>;
}

interface substanceReference {
  substanceReference?: Reference<Substance>;
}
