import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { code, positiveInt } from '../primitives'
import { Patient } from './patient'
import { Organization } from './organization'
import { Practitioner } from './practitioner'
import { PractitionerRole } from './practitioner-role'
import { Account } from './account'
import { CareTeam } from '../clinical/careteam'
import { ServiceRequest } from '../workflow/service-request'
import { DiagnosisRole, EpisodeOfCareStatus, EpisodeOfCareType } from '../value-sets/value-sets-4-0'
import { Condition } from '../clinical/condition'

/*
An association between a patient and an organization / healthcare provider(s) during which time encounters may occur. The managing organization assumes a level of responsibility for the patient during this time.
 */
export interface EpisodeOfCare extends DomainResource {
  identifier?: Identifier[];
  status: code<EpisodeOfCareStatus>;
  statusHistory?: BackboneElement & {
    status: code<EpisodeOfCareStatus>;
    period: Period;
  }[];
  type?: CodeableConcept<EpisodeOfCareType>;
  diagnosis?: BackboneElement & {
    condition: Reference<Condition>;
    role?: CodeableConcept<DiagnosisRole>;
    rank?: positiveInt;
  }[];
  patient: Reference<Patient>;
  managingOrganization?: Reference<Organization>;
  period?: Period;
  referralRequest?: Reference<ServiceRequest>[];
  careManager?: Reference<Practitioner | PractitionerRole>;
  team?: Reference<CareTeam>[];
  account?: Reference<Account>[];
}
