import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Address,
  Attachment,
  CodeableConcept,
  ContactPoint, HumanName,
  Identifier,
  Period
} from '../foundation/base-types/general-purpose'
import { Patient } from './patient'
import { AdministrativeGender, CommonLanguages, PatientRelationshipType } from '../value-sets/value-sets-4-0'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { code, date } from '../primitives'

/*
RelatedPersons typically have a personal or non-healthcare-specific professional relationship to the patient. A RelatedPerson resource is primarily used for attribution of information, since RelatedPersons are often a source of information about the patient. For keeping information about people for contact purposes for a patient, use a Patient's Contact element. Some individuals may serve as both a Patient's Contact and a Related Person.

Example RelatedPersons are:

- A patient's wife or husband
- A patient's relatives or friends
- A neighbor bringing a patient to the hospital
- The owner or trainer of a horse
- A patient's attorney or guardian
- A Guide Dog
 */
export interface RelatedPerson extends DomainResource {
  identifier?: Identifier[];
  active?: boolean;
  patient: Reference<Patient>;
  relationship?: CodeableConcept<PatientRelationshipType>;
  name?: HumanName[];
  telecom?: ContactPoint[];
  gender?: code<AdministrativeGender>;
  birthDate?: date;
  address?: Address[];
  photo?: Attachment[];
  period?: Period;
  communication?: BackboneElement & {
    language: CodeableConcept<CommonLanguages>;
    preferred?: boolean;
  }
}
