import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Period, Quantity, Range } from '../foundation/base-types/general-purpose'
import { code, unsignedInt } from '../primitives'
import { RelatedPerson } from './related-person'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from './patient'
import { Practitioner } from './practitioner'
import { Organization } from './organization'
import { PractitionerRole } from './practitioner-role'
import { GroupType } from '../value-sets/value-sets-4-0'
import { Device } from './device'
import { Medication } from '../medications/medication'
import { Substance } from './substance'

/*
Represents a defined collection of entities that may be discussed or acted upon collectively but which are not expected to act collectively, and are not formally or legally recognized; i.e. a collection of entities that isn't an Organization.
 */
export interface Group extends DomainResource {
  identifier?: Identifier[];
  active?: boolean;
  type: code<GroupType>;
  actual: boolean;
  code?: CodeableConcept<any>;
  name?: string;
  quantity?: unsignedInt;
  managingEntity?: Reference<Organization | RelatedPerson | Practitioner | PractitionerRole>;
  characteristic?: Characteristic[];
  member?: BackboneElement & {
    entity?: Reference<Patient | Practitioner | PractitionerRole | Device | Medication | Substance | Group>;
    period?: Period;
    inactive?: boolean;
  }
}

type Characteristic = CharacteristicBase & (valueCodeableConcept | valueBoolean | valueQuantity | valueRange | valueReference);

interface CharacteristicBase extends BackboneElement {
  code: CodeableConcept<any>;
  exclude: boolean;
  period?: Period;
}

interface valueCodeableConcept {
  valueCodeableConcept?: CodeableConcept<any>;
}

interface valueBoolean {
  valueBoolean?: boolean;
}

interface valueQuantity {
  valueQuantity?: Quantity;
}

interface valueRange {
  valueRange?: Range;
}

interface valueReference {
  valueReference?: Reference<any>;
}
