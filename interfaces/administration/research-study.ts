import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { PlanDefinition } from '../workflow/plan-definition'
import { code, markdown } from '../primitives'
import { ContactDetail, RelatedArtifact } from '../foundation/base-types/metadata'
import {
  Condition_Problem_DiagnosisCodes,
  Jurisdiction, ResearchStudyObjectiveType,
  ResearchStudyPhase,
  ResearchStudyPrimaryPurposeType, ResearchStudyReasonStopped,
  ResearchStudyStatus
} from '../value-sets/value-sets-4-0'
import { Group } from './group'
import { Organization } from './organization'
import { Practitioner } from './practitioner'
import { PractitionerRole } from './practitioner-role'
import { Location } from './location'

/*
A process where a researcher or organization plans and then executes a series of steps intended to increase the field of healthcare-related knowledge. This includes studies of safety, efficacy, comparative effectiveness and other information about medications, devices, therapies and other interventional and investigative techniques. A ResearchStudy involves the gathering of information about human or animal subjects.
 */
export interface ResearchStudy extends DomainResource {
  identifier?: Identifier[];
  title?: string;
  protocol?: Reference<PlanDefinition>[];
  partOf?: Reference<ResearchStudy>[];
  status: code<ResearchStudyStatus>;
  primaryPurposeType?: CodeableConcept<ResearchStudyPrimaryPurposeType>;
  phase?: CodeableConcept<ResearchStudyPhase>;
  category?: CodeableConcept<any>[];
  focus?: CodeableConcept<any>[];
  condition?: CodeableConcept<Condition_Problem_DiagnosisCodes>[];
  contact?: ContactDetail[];
  relatedArtifact?: RelatedArtifact[];
  keyword?: CodeableConcept<any>[];
  location?: CodeableConcept<Jurisdiction>[];
  description?: markdown;
  enrollment?: Reference<Group>[];
  period?: Period;
  sponsor?: Reference<Organization>;
  principalInvestigator?: Reference<Practitioner | PractitionerRole>;
  site?: Reference<Location>[];
  reasonStopped?: CodeableConcept<ResearchStudyReasonStopped>;
  note?: Annotation[];
  arm?: BackboneElement & {
    name: string;
    type?: CodeableConcept<any>;
    description?: string;
  }[];
  objective?: BackboneElement & {
    name?: string;
    type?: CodeableConcept<ResearchStudyObjectiveType>;
  }[];
}
