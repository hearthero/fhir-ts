import { Attachment, CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { Reference } from '../foundation/base-types/special-purpose'
import { Group } from '../administration/group'
import { DomainResource } from '../foundation/framework/domain-resource'
import { code, date, dateTime, markdown, uri } from '../primitives'
import {
  DefinitionTopic,
  Jurisdiction,
  LibraryType,
  PublicationStatus,
  SubjectType
} from '../value-sets/value-sets-4-0'
import {
  ContactDetail,
  DataRequirement,
  ParameterDefinition,
  RelatedArtifact,
  UsageContext
} from '../foundation/base-types/metadata'

/*
The Library resource is a general-purpose container for knowledge asset definitions. It can be used to describe and expose existing knowledge assets such as logic libraries and information model descriptions, as well as to describe a collection of knowledge assets.
 */
export type Library = LibraryBase & subject;

interface LibraryBase extends DomainResource {
  url?: uri;
  identifier?: Identifier[];
  version?: string;
  name?: string;
  title?: string;
  subtitle?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  type: CodeableConcept<LibraryType>;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  usage?: string;
  copyright?: markdown;
  approvalDate?: date;
  lastReviewDate?: date;
  effectivePeriod?: Period;
  topic?: CodeableConcept<DefinitionTopic>[];
  author?: ContactDetail[];
  editor?: ContactDetail[];
  reviewer?: ContactDetail[];
  endorser?: ContactDetail[];
  relatedArtifact?: RelatedArtifact[];
  parameter?: ParameterDefinition[];
  dataRequirement?: DataRequirement[];
  content?: Attachment[];
}

type subject = subjectCodeableConcept | subjectReference;

interface subjectCodeableConcept {
  subjectCodeableConcept?: CodeableConcept<SubjectType>;
}

interface subjectReference {
  subjectReference?: Reference<Group>;
}
