import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Age,
  Annotation,
  CodeableConcept, Duration,
  Identifier,
  Period,
  Range,
  Timing
} from '../foundation/base-types/general-purpose'
import { canonical, code, dateTime, id, uri } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import {
  ActionCardinalityBehavior,
  ActionConditionKind,
  ActionGroupingBehavior,
  ActionPrecheckBehavior,
  ActionRelationshipType,
  ActionRequiredBehavior,
  ActionSelectionBehavior,
  ActionType,
  RequestIntent,
  RequestPriority,
  RequestStatus
} from '../value-sets/value-sets-4-0'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { Device } from '../administration/device'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Condition } from '../clinical/condition'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Expression, RelatedArtifact } from '../foundation/base-types/metadata'
import { RelatedPerson } from '../administration/related-person'

/*
A group of related requests that can be used to capture intended activities that have inter-dependencies such as "give this medication after that one".
 */
export interface RequestGroup extends DomainResource {
  identifier?: Identifier[];
  instantiatesCanonical?: canonical<any>[];
  instantiatesUri?: uri[];
  basedOn?: Reference<any>[];
  replaces?: Reference<any>[];
  groupIdentifier?: Identifier[];
  status: code<RequestStatus>;
  intent: code<RequestIntent>;
  priority?: code<RequestPriority>;
  code?: CodeableConcept<any>;
  subject?: Reference<Patient | Group>;
  encounter?: Reference<Encounter>;
  authoredOn?: dateTime;
  author?: Reference<Device | Practitioner | PractitionerRole>;
  reasonCode?: CodeableConcept<any>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport | DocumentReference>[];
  note?: Annotation[];
  action?: actionObj[];
}

type actionObj = actionObjBase & timing;

interface actionObjBase extends BackboneElement {
  prefix?: string;
  title?: string;
  description?: string;
  textEquivalent?: string;
  priority?: code<RequestPriority>;
  code?: CodeableConcept<any>;
  documentation?: RelatedArtifact[];
  condition?: BackboneElement & {
    kind: code<ActionConditionKind>;
    expression?: Expression;
  }[];
  relatedAction?: relatedActionObj[];
  participant?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson | Device>[];
  type?: CodeableConcept<ActionType>;
  groupingBehavior?: code<ActionGroupingBehavior>;
  selectionBehavior?: code<ActionSelectionBehavior>;
  requiredBehavior?: code<ActionRequiredBehavior>;
  precheckBehavior?: code<ActionPrecheckBehavior>;
  cardinalityBehavior?: code<ActionCardinalityBehavior>;
  resource?: Reference<any>;
  action?: actionObj[];
}

type timing = timingDateTime | timingAge | timingPeriod | timingDuration | timingRange | timingTiming;

interface timingTiming {
  timingTiming?: Timing;
}

interface timingDateTime {
  timingDateTime?: dateTime;
}

interface timingAge {
  timingAge?: Age;
}

interface timingPeriod {
  timingPeriod?: Period;
}

interface timingRange {
  timingRange?: Range;
}

interface timingDuration {
  timingDuration?: Duration;
}

type relatedActionObj = relatedActionObjBase & offset;

interface relatedActionObjBase extends BackboneElement {
  actionId: id;
  relationship: code<ActionRelationshipType>;
}

type offset = offsetDuration | offsetRange;

interface offsetDuration {
  offsetDuration?: Duration;
}

interface offsetRange {
  offsetRange?: Range;
}
