import { DomainResource } from '../foundation/framework/domain-resource'
import { canonical, code, date, dateTime, markdown, uri } from '../primitives'
import { CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import {
  CompositeMeasureScoring,
  DefinitionTopic,
  Jurisdiction, MeasureImprovementNotation, MeasurePopulationType,
  MeasureScoring, MeasureType,
  PublicationStatus,
  SubjectType
} from '../value-sets/value-sets-4-0'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Group } from '../administration/group'
import { ContactDetail, Expression, RelatedArtifact, UsageContext } from '../foundation/base-types/metadata'
import { Library } from './library'

/*
The Measure resource provides the definition of a quality measure.
 */
export type Measure = MeasureBase & subject;

interface MeasureBase extends DomainResource {
  url?: uri;
  identifier?: Identifier[];
  version?: string;
  name?: string;
  title?: string;
  subtitle?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  usage?: string;
  copyright?: markdown;
  approvalDate?: date;
  lastReviewDate?: date;
  effectivePeriod?: Period;
  topic?: CodeableConcept<DefinitionTopic>[];
  author?: ContactDetail[];
  editor?: ContactDetail[];
  reviewer?: ContactDetail[];
  endorser?: ContactDetail[];
  relatedArtifact?: RelatedArtifact[];
  library?: canonical<Library>[];
  disclaimer?: markdown;
  scoring?: CodeableConcept<MeasureScoring>;
  compositeScoring?: CodeableConcept<CompositeMeasureScoring>;
  type?: CodeableConcept<MeasureType>;
  riskAdjustment?: string;
  rateAggregation?: string;
  rationale?: markdown;
  clinicalRecommendationStatement?: markdown;
  improvementNotation?: CodeableConcept<MeasureImprovementNotation>;
  definition?: markdown[];
  guidance?: markdown;
  group?: BackboneElement & {
    code?: CodeableConcept<any>;
    description?: string;
    population?: BackboneElement & {
      code?: CodeableConcept<MeasurePopulationType>;
      description?: string;
      criteria: Expression;
    }[];
    stratifier?: BackboneElement & {
      code?: CodeableConcept<any>;
      description?: string;
      criteria?: Expression;
      component?: BackboneElement & {
        code?: CodeableConcept<any>;
        description?: string;
        criteria: Expression;
      }[];
    }[];
  }[];
}

type subject = subjectCodeableConcept | subjectReference;

interface subjectCodeableConcept {
  subjectCodeableConcept?: CodeableConcept<SubjectType>;
}

interface subjectReference {
  subjectReference?: Reference<Group>;
}
