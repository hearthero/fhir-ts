import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Money, Period, SimpleQuantity } from '../foundation/base-types/general-purpose'
import { ArrayOfOneOrMore, code, date, dateTime, positiveInt } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { Location } from '../administration/location'
import { Coverage } from './coverage'
import { Condition } from '../clinical/condition'
import {
  BenefitCategoryCodes,
  EligibilityRequestPurpose,
  FinancialResourceStatusCodes, ICD_10Codes, ModifierTypeCodes,
  ProcessPriorityCodes, USCLSCodes
} from '../value-sets/value-sets-4-0'

/*
The CoverageEligibilityRequest provides patient and insurance coverage information to an insurer for them to respond, in the form of an CoverageEligibilityResponse, with information regarding whether the stated coverage is valid and in-force and optionally to provide the insurance details of the policy.
 */
export type CoverageEligibilityRequest = CoverageEligibilityRequestBase & serviced;

interface CoverageEligibilityRequestBase extends DomainResource {
  identifier?: Identifier[];
  status: code<FinancialResourceStatusCodes>;
  priority?: CodeableConcept<ProcessPriorityCodes>;
  purpose: ArrayOfOneOrMore<code<EligibilityRequestPurpose>>;
  patient: Reference<Patient>;
  created: dateTime;
  enterer?: Reference<Practitioner | PractitionerRole>;
  provider?: Reference<Practitioner | PractitionerRole | Organization>;
  insurer: Reference<Organization>;
  facility?: Reference<Location>;
  supportingInfo?: BackboneElement & {
    sequence: positiveInt;
    information: Reference<any>;
    appliesToAll?: boolean;
  }[];
  insurance?: BackboneElement & {
    focal?: boolean;
    coverage: Reference<Coverage>;
    businessArrangement?: string;
  }[];
  item?: BackboneElement & {
    supportingInfoSequence?: positiveInt[];
    category?: CodeableConcept<BenefitCategoryCodes>;
    productOrService?: CodeableConcept<USCLSCodes>;
    modifier?: CodeableConcept<ModifierTypeCodes>[];
    provider?: Reference<Practitioner | PractitionerRole>;
    quantity?: SimpleQuantity;
    unitPrice?: Money;
    facility?: Reference<Location | Organization>;
    diagnosis?: (BackboneElement & diagnosis)[];
    detail?: Reference<any>[];
  }[];
}

type serviced = servicedDate | servicedPeriod;

interface servicedDate {
  servicedDate?: date;
}

interface servicedPeriod {
  servicedPeriod?: Period;
}

type diagnosis = diagnosisCodeableConcept | diagnosisReference;

interface diagnosisCodeableConcept {
  diagnosisCodeableConcept?: CodeableConcept<ICD_10Codes>;
}

interface diagnosisReference {
  diagnosisReference?: Reference<Condition>;
}
