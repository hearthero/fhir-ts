import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Money, Period } from '../foundation/base-types/general-purpose'
import {
  ClaimProcessingCodes,
  FinancialResourceStatusCodes,
  FormCodes,
  NoteType,
  PaymentTypeCodes
} from '../value-sets/value-sets-4-0'
import { code, date, dateTime } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Organization } from '../administration/organization'
import { Task } from '../workflow/task'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'

/*
This resource provides the details including amount of a payment and allocates the payment items being paid.
 */
export interface PaymentReconciliation extends DomainResource {
  identifier?: Identifier[];
  status: code<FinancialResourceStatusCodes>;
  period?: Period;
  created: dateTime;
  paymentIssuer?: Reference<Organization>;
  request?: Reference<Task>;
  requestor?: Reference<Practitioner | PractitionerRole | Organization>;
  outcome?: code<ClaimProcessingCodes>;
  disposition?: string;
  paymentDate: date;
  paymentAmount: Money;
  paymentIdentifier?: Identifier;
  detail?: BackboneElement & {
    identifier?: Identifier;
    predecessor?: Identifier;
    type: CodeableConcept<PaymentTypeCodes>;
    request?: Reference<any>;
    submitter?: Reference<Practitioner | PractitionerRole | Organization>;
    response?: Reference<any>;
    date?: date;
    responsible?: Reference<PractitionerRole>;
    payee?: Reference<Practitioner | PractitionerRole | Organization>;
    amount?: Money;
  }[];
  formCode?: CodeableConcept<FormCodes>;
  processNote?: BackboneElement & {
    type?: code<NoteType>;
    text?: string;
  }[];
}
