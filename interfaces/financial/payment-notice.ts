import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Money } from '../foundation/base-types/general-purpose'
import { FinancialResourceStatusCodes, PaymentStatusCodes } from '../value-sets/value-sets-4-0'
import { Reference } from '../foundation/base-types/special-purpose'
import { code, date, dateTime } from '../primitives'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { PaymentReconciliation } from './payment-reconciliation'

/*
This resource provides the status of the payment for goods and services rendered, and the request and response resource references.
 */
export interface PaymentNotice extends DomainResource {
  identifier?: Identifier[];
  status: code<FinancialResourceStatusCodes>;
  request?: Reference<any>;
  response?: Reference<any>;
  created: dateTime;
  provider?: Reference<Practitioner | PractitionerRole | Organization>;
  payment: Reference<PaymentReconciliation>;
  paymentDate?: date;
  payee?: Reference<Practitioner | PractitionerRole | Organization>;
  recipient: Reference<Organization>;
  amount: Money;
  paymentStatus?: CodeableConcept<PaymentStatusCodes>;
}
