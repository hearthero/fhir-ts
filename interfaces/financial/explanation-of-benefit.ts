import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Address,
  Attachment,
  CodeableConcept,
  Identifier, Money,
  Period,
  Quantity, SimpleQuantity
} from '../foundation/base-types/general-purpose'
import { ArrayOfOneOrMore, code, date, dateTime, decimal, positiveInt, unsignedInt } from '../primitives'
import {
  AdjudicationReasonCodes,
  AdjudicationValueCodes,
  BenefitCategoryCodes, BenefitTypeCodes,
  ClaimCareTeamRoleCodes,
  ClaimInformationCategoryCodes,
  ClaimPayeeTypeCodes,
  ClaimProcessingCodes,
  ClaimTypeCodes, CommonLanguages,
  ExampleClaimSubTypeCodes, ExampleDiagnosisOnAdmissionCodes, ExampleDiagnosisRelatedGroupCodes,
  ExampleDiagnosisTypeCodes, ExamplePaymentTypeCodes, ExampleProcedureTypeCodes, ExampleProgramReasonCodes,
  ExampleProviderQualificationCodes,
  ExampleRelatedClaimRelationshipCodes, ExampleRevenueCenterCodes, ExampleServicePlaceCodes,
  ExceptionCodes, ExplanationOfBenefitStatus, FormCodes,
  FundsReservationCodes, ICD_10Codes, ICD_10ProcedureCodes,
  MissingToothReasonCodes, ModifierTypeCodes, NetworkTypeCodes, NoteType, OralSiteCodes, PaymentAdjustmentReasonCodes,
  ProcessPriorityCodes, SurfaceCodes, UnitTypeCodes, USCLSCodes,
  Use, v3_ActIncidentCode
} from '../value-sets/value-sets-4-0'
import { Patient } from '../administration/patient'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { Claim } from './claim'
import { MedicationRequest } from '../medications/medication-request'
import { VisionPrescription } from '../workflow/vision-prescription'
import { RelatedPerson } from '../administration/related-person'
import { ServiceRequest } from '../workflow/service-request'
import { Location } from '../administration/location'
import { ClaimResponse } from './claim-response'
import { Condition } from '../clinical/condition'
import { Procedure } from '../clinical/procedure'
import { Device } from '../administration/device'
import { Coverage } from './coverage'
import { Encounter } from '../administration/encounter'

/*
This resource provides: the claim details; adjudication details from the processing of a Claim; and optionally account balance information, for informing the subscriber of the benefits provided.
 */
export interface ExplanationOfBenefit extends DomainResource {
  identifier?: Identifier[];
  status: code<ExplanationOfBenefitStatus>;
  type: CodeableConcept<ClaimTypeCodes>;
  subType?: CodeableConcept<ExampleClaimSubTypeCodes>;
  use: code<Use>;
  patient: Reference<Patient>;
  billablePeriod?: Period;
  created: dateTime;
  enterer?: Reference<Practitioner | PractitionerRole>;
  insurer: Reference<Organization>;
  provider: Reference<Practitioner | PractitionerRole | Organization>;
  priority?: CodeableConcept<ProcessPriorityCodes>;
  fundsReserveRequested?: CodeableConcept<FundsReservationCodes>;
  fundsReserve?: CodeableConcept<FundsReservationCodes>;
  related?: BackboneElement & {
    claim?: Reference<Claim>;
    relationship?: CodeableConcept<ExampleRelatedClaimRelationshipCodes>;
    reference?: Identifier;
  }[];
  prescription?: Reference<MedicationRequest | VisionPrescription>;
  originalPrescription?: Reference<MedicationRequest>;
  payee?: BackboneElement & {
    type?: CodeableConcept<ClaimPayeeTypeCodes>;
    party?: Reference<Practitioner | PractitionerRole | Organization | Patient | RelatedPerson>;
  };
  referral?: Reference<ServiceRequest>;
  facility?: Reference<Location>;
  claim?: Reference<Claim>;
  claimResponse?: Reference<ClaimResponse>;
  outcome: code<ClaimProcessingCodes>;
  disposition?: string;
  preAuthRef?: string[];
  preAuthRefPeriod?: Period[];
  careTeam?: BackboneElement & {
    sequence: positiveInt;
    provider: Reference<Practitioner | PractitionerRole | Organization>;
    responsible?: boolean;
    role?: CodeableConcept<ClaimCareTeamRoleCodes>;
    qualification?: CodeableConcept<ExampleProviderQualificationCodes>;
  }[];
  supportingInfo?: supportingInfoObj[];
  diagnosis?: diagnosisObj[];
  procedure?: procedureObj[];
  precedence?: positiveInt;
  insurance: ArrayOfOneOrMore<insuranceObj>;
  accident?: accidentObj;
  item?: itemObj[];
  addItem?: itemObj[];
  adjudication?: adjudicationObj[];
  total?: BackboneElement & {
    category: CodeableConcept<AdjudicationValueCodes>;
    amount: Money;
  }[];
  payment?: BackboneElement & {
    type?: CodeableConcept<ExamplePaymentTypeCodes>;
    adjustment?: Money;
    adjustmentReason?: CodeableConcept<PaymentAdjustmentReasonCodes>;
    date?: date;
    amount?: Money;
    identifier?: Identifier;
  };
  formCode?: CodeableConcept<FormCodes>;
  form?: Attachment;
  processNote?: BackboneElement & {
    number?: positiveInt;
    type?: code<NoteType>;
    text?: string;
    language?: CodeableConcept<CommonLanguages>;
  }[];
  benefitPeriod?: Period;
  benefitBalance?: BackboneElement & {
    category: CodeableConcept<BenefitCategoryCodes>;
    excluded?: boolean;
    name?: string;
    description?: string;
    network?: CodeableConcept<NetworkTypeCodes>;
    unit?: CodeableConcept<UnitTypeCodes>;
    term?: CodeableConcept<BenefitTypeCodes>;
    financial?: financialObj[];
  }[];
}

type supportingInfoObj = supportingInfoObjBase & timing & value;

interface supportingInfoObjBase extends BackboneElement {
  sequence: positiveInt;
  category: CodeableConcept<ClaimInformationCategoryCodes>;
  code?: CodeableConcept<ExceptionCodes>;
  reason?: CodeableConcept<MissingToothReasonCodes>;
}

type diagnosisObj = diagnosisObjBase & diagnosis;

interface diagnosisObjBase extends BackboneElement {
  sequence: positiveInt;
  type?: CodeableConcept<ExampleDiagnosisTypeCodes>;
  onAdmission?: CodeableConcept<ExampleDiagnosisOnAdmissionCodes>;
  packageCode?: CodeableConcept<ExampleDiagnosisRelatedGroupCodes>;
}

type procedureObj = procedureObjBase & procedure;

interface procedureObjBase extends BackboneElement {
  sequence: positiveInt;
  type?: CodeableConcept<ExampleProcedureTypeCodes>[];
  date?: dateTime;
  udi?: Reference<Device>[];
}

interface insuranceObj extends BackboneElement {
  sequence: positiveInt;
  focal: boolean;
  identifier?: Identifier;
  coverage: Reference<Coverage>;
  businessArrangement?: string;
  preAuthRef?: string[];
  claimResponse?: Reference<ClaimResponse>;
}

type accidentObj = accidentObjBase & location;

interface accidentObjBase extends BackboneElement {
  date: date;
  type: CodeableConcept<v3_ActIncidentCode>;
}

interface adjudicationObj extends BackboneElement {
  category: CodeableConcept<AdjudicationValueCodes>;
  reason?: CodeableConcept<AdjudicationReasonCodes>;
  amount?: Money;
  value?: decimal;
}

type itemObj = itemObjBase & serviced & location2;

interface itemObjBase extends BackboneElement {
  sequence: positiveInt;
  careTeamSequence: positiveInt[];
  diagnosisSequence: positiveInt[];
  procedureSequence: positiveInt[];
  informationSequence: positiveInt[];
  revenue?: CodeableConcept<ExampleRevenueCenterCodes>;
  category?: CodeableConcept<BenefitCategoryCodes>;
  productOrService: CodeableConcept<USCLSCodes>;
  modifier?: CodeableConcept<ModifierTypeCodes>[];
  programCode?: CodeableConcept<ExampleProgramReasonCodes>[];
  quantity?: SimpleQuantity;
  unitPrice?: Money;
  factor?: decimal;
  net?: Money;
  udi?: Reference<Device>[];
  bodySite?: CodeableConcept<OralSiteCodes>;
  subSite?: CodeableConcept<SurfaceCodes>[];
  encounter?: Reference<Encounter>[];
  noteNumber?: positiveInt[];
  adjudication?: adjudicationObj[];
  detail?: BackboneElement & {
    sequence: positiveInt;
    revenue?: CodeableConcept<ExampleRevenueCenterCodes>;
    category?: CodeableConcept<BenefitCategoryCodes>;
    productOrService: CodeableConcept<USCLSCodes>;
    modifier?: CodeableConcept<ModifierTypeCodes>[];
    programCode?: CodeableConcept<ExampleProgramReasonCodes>[];
    quantity?: SimpleQuantity;
    unitPrice?: Money;
    factor?: decimal;
    net?: Money;
    udi?: Reference<Device>[];
    noteNumber?: positiveInt[];
    adjudication?: adjudicationObj[];
    subDetail?: BackboneElement & {
      sequence: positiveInt;
      revenue?: CodeableConcept<ExampleRevenueCenterCodes>;
      category?: CodeableConcept<BenefitCategoryCodes>;
      productOrService: CodeableConcept<USCLSCodes>;
      modifier?: CodeableConcept<ModifierTypeCodes>[];
      programCode?: CodeableConcept<ExampleProgramReasonCodes>[];
      quantity?: SimpleQuantity;
      unitPrice?: Money;
      factor?: decimal;
      net?: Money;
      udi?: Reference<Device>[];
      noteNumber?: positiveInt[];
      adjudication?: adjudicationObj[];
    }[];
  }[];
}

type timing = timingDate | timingPeriod;

type value = valueBoolean | valueString | valueQuantity | valueAttachment | valueReference;

type diagnosis = diagnosisCodeableConcept | diagnosisReference;

type procedure = procedureCodeableConcept | procedureReference;

type location = locationAddress | locationReference;

type serviced = servicedDate | servicedPeriod;

type location2 = locationCodeableConcept | locationAddress | locationReference;

interface timingDate {
  timingDate?: date;
}

interface timingPeriod {
  timingPeriod?: Period;
}

interface valueBoolean {
  valueBoolean?: boolean;
}

interface valueString {
  valueString?: string;
}

interface diagnosisCodeableConcept {
  diagnosisCodeableConcept: CodeableConcept<ICD_10Codes>;
}

interface diagnosisReference {
  diagnosisReference: Reference<Condition>;
}

interface valueQuantity {
  valueQuantity?: Quantity;
}

interface valueAttachment {
  valueAttachment?: Attachment;
}

interface valueReference {
  valueReference?: Reference<any>;
}

interface procedureCodeableConcept {
  procedureCodeableConcept: CodeableConcept<ICD_10ProcedureCodes>;
}

interface procedureReference {
  procedureReference: Reference<Procedure>;
}

interface locationCodeableConcept {
  locationCodeableConcept?: CodeableConcept<ExampleServicePlaceCodes>;
}

interface locationAddress {
  locationAddress?: Address;
}

interface locationReference {
  locationReference?: Reference<Location>;
}

interface servicedDate {
  servicedDate?: date;
}

interface servicedPeriod {
  servicedPeriod?: Period;
}

type financialObj = financialObjBase & allowed & used;

interface financialObjBase extends BackboneElement {
  type: CodeableConcept<BenefitTypeCodes>;
}

type allowed = allowedUnsignedInt | allowedString | allowedMoney;

type used = usedUnsignedInt | usedMoney;

interface allowedUnsignedInt {
  allowedUnsignedInt?: unsignedInt;
}

interface allowedString {
  allowedString?: string;
}

interface allowedMoney {
  allowedMoney?: Money;
}

interface usedUnsignedInt {
  usedUnsignedInt?: unsignedInt;
}

interface usedMoney {
  usedMoney?: Money;
}
