import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Money, Period, SimpleQuantity } from '../foundation/base-types/general-purpose'
import { ArrayOfOneOrMore, code, positiveInt } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { RelatedPerson } from '../administration/related-person'
import { Organization } from '../administration/organization'
import { Contract } from './contract'
import {
  CoverageClassCodes, CoverageCopayTypeCodes,
  CoverageTypeAndSelfPayCodes, ExampleCoverageFinancialExceptionCodes,
  FinancialResourceStatusCodes, SubscriberRelationshipCodes
} from '../value-sets/value-sets-4-0'

/*
Financial instrument which may be used to reimburse or pay for health care products and services. Includes both insurance and self-payment.
 */
export interface Coverage extends DomainResource {
  identifier?: Identifier[];
  status: code<FinancialResourceStatusCodes>;
  type?: CodeableConcept<CoverageTypeAndSelfPayCodes>;
  policyHolder?: Reference<Patient | RelatedPerson | Organization>;
  subscriber?: Reference<Patient | RelatedPerson>;
  subscriberId?: string;
  beneficiary: Reference<Patient>;
  dependent?: string;
  relationship?: CodeableConcept<SubscriberRelationshipCodes>;
  period?: Period;
  payor: ArrayOfOneOrMore<Reference<Organization | Patient | RelatedPerson>>;
  class?: BackboneElement & {
    type: CodeableConcept<CoverageClassCodes>;
    value: string;
    name?: string;
  }[];
  order?: positiveInt;
  network?: string;
  costToBeneficiary?: costToBeneficiaryObj[];
  subrogation?: boolean;
  contract?: Reference<Contract>[];
}

type costToBeneficiaryObj = costToBeneficiaryObjBase & value;

interface costToBeneficiaryObjBase extends BackboneElement {
  type?: CodeableConcept<CoverageCopayTypeCodes>;
  exception?: BackboneElement & {
    type: CodeableConcept<ExampleCoverageFinancialExceptionCodes>;
    period?: Period;
  }[];
}

type value = valueQuantity | valueMoney;

interface valueQuantity {
  valueQuantity: SimpleQuantity;
}

interface valueMoney {
  valueMoney: Money;
}
