import { DomainResource } from '../foundation/framework/domain-resource'
import { Identifier } from '../foundation/base-types/general-purpose'
import { code, dateTime } from '../primitives'
import { Reference } from '../foundation/base-types/special-purpose'
import { Organization } from '../administration/organization'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Patient } from '../administration/patient'
import { Coverage } from './coverage'
import { FinancialResourceStatusCodes } from '../value-sets/value-sets-4-0'

/*
This resource provides the insurance enrollment details to the insurer regarding a specified coverage.
 */
export interface EnrollmentRequest extends DomainResource {
  identifier?: Identifier[];
  status?: code<FinancialResourceStatusCodes>;
  created?: dateTime;
  insurer?: Reference<Organization>;
  provider?: Reference<Practitioner | PractitionerRole | Organization>;
  candidate?: Reference<Patient>;
  coverage?: Reference<Coverage>;
}
