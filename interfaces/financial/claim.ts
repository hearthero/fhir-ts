import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Address,
  Attachment,
  CodeableConcept,
  Identifier,
  Money,
  Period,
  Quantity,
  SimpleQuantity
} from '../foundation/base-types/general-purpose'
import {
  BenefitCategoryCodes,
  ClaimCareTeamRoleCodes,
  ClaimInformationCategoryCodes,
  ClaimPayeeTypeCodes,
  ClaimTypeCodes,
  ExampleClaimSubTypeCodes,
  ExampleDiagnosisOnAdmissionCodes, ExampleDiagnosisRelatedGroupCodes,
  ExampleDiagnosisTypeCodes, ExampleProcedureTypeCodes, ExampleProgramReasonCodes,
  ExampleProviderQualificationCodes,
  ExampleRelatedClaimRelationshipCodes, ExampleRevenueCenterCodes, ExampleServicePlaceCodes,
  ExceptionCodes,
  FinancialResourceStatusCodes,
  FundsReservationCodes,
  ICD_10Codes, ICD_10ProcedureCodes,
  MissingToothReasonCodes,
  ModifierTypeCodes, OralSiteCodes,
  ProcessPriorityCodes, SurfaceCodes,
  USCLSCodes,
  Use, v3_ActIncidentCode
} from '../value-sets/value-sets-4-0'
import { ArrayOfOneOrMore, code, date, dateTime, decimal, positiveInt } from '../primitives'
import { Patient } from '../administration/patient'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { DeviceRequest } from '../workflow/device-request'
import { MedicationRequest } from '../medications/medication-request'
import { VisionPrescription } from '../workflow/vision-prescription'
import { RelatedPerson } from '../administration/related-person'
import { ServiceRequest } from '../workflow/service-request'
import { Location } from '../administration/location'
import { Device } from '../administration/device'
import { Coverage } from './coverage'
import { Encounter } from '../administration/encounter'
import { Condition } from '../clinical/condition'
import { Procedure } from '../clinical/procedure'
import { ClaimResponse } from './claim-response'

/*
A provider issued list of professional services and products which have been provided, or are to be provided, to a patient which is sent to an insurer for reimbursement.
 */
export interface Claim extends DomainResource {
  identifier?: Identifier[];
  status: code<FinancialResourceStatusCodes>;
  type: CodeableConcept<ClaimTypeCodes>;
  subType?: CodeableConcept<ExampleClaimSubTypeCodes>;
  use: code<Use>;
  patient: Reference<Patient>;
  billablePeriod?: Period;
  created: dateTime;
  enterer?: Reference<Practitioner | PractitionerRole>;
  insurer?: Reference<Organization>;
  provider: Reference<Practitioner | PractitionerRole>;
  priority: CodeableConcept<ProcessPriorityCodes>;
  fundsReserve?: CodeableConcept<FundsReservationCodes>;
  related?: BackboneElement & {
    claim?: Reference<Claim>;
    relationship?: CodeableConcept<ExampleRelatedClaimRelationshipCodes>;
    reference?: Identifier;
  }[];
  prescription?: Reference<DeviceRequest | MedicationRequest | VisionPrescription>;
  originalPrescription?: Reference<DeviceRequest | MedicationRequest |VisionPrescription>;
  payee?: BackboneElement & {
    type: CodeableConcept<ClaimPayeeTypeCodes>;
    party?: Reference<Practitioner | PractitionerRole | Organization | Patient | RelatedPerson>;
  };
  referral?: Reference<ServiceRequest>;
  facility?: Reference<Location>;
  careTeam?: BackboneElement & {
    sequence: positiveInt;
    provider: Reference<Practitioner | PractitionerRole | Organization>;
    responsible?: boolean;
    role?: CodeableConcept<ClaimCareTeamRoleCodes>;
    qualification?: CodeableConcept<ExampleProviderQualificationCodes>;
  }[];
  supportingInfo?: supportingInfoObj[];
  diagnosis?: diagnosisObj[];
  procedure?: procedureObj[];
  insurance: ArrayOfOneOrMore<insuranceObj>;
  accident?: accidentObj[];
  item?: itemObj[];
  total?: Money;
}

type supportingInfoObj = supportingInfoObjBase & timing & value;

interface supportingInfoObjBase extends BackboneElement {
  sequence: positiveInt;
  category: CodeableConcept<ClaimInformationCategoryCodes>;
  code?: CodeableConcept<ExceptionCodes>;
  reason?: CodeableConcept<MissingToothReasonCodes>;
}

type diagnosisObj = diagnosisObjBase & diagnosis;

interface diagnosisObjBase extends BackboneElement {
  sequence: positiveInt;
  type?: CodeableConcept<ExampleDiagnosisTypeCodes>;
  onAdmission?: CodeableConcept<ExampleDiagnosisOnAdmissionCodes>;
  packageCode?: CodeableConcept<ExampleDiagnosisRelatedGroupCodes>;
}

type procedureObj = procedureObjBase & procedure;

interface procedureObjBase extends BackboneElement {
  sequence: positiveInt;
  type?: CodeableConcept<ExampleProcedureTypeCodes>[];
  date?: dateTime;
  udi?: Reference<Device>[];
}

interface insuranceObj extends BackboneElement {
  sequence: positiveInt;
  focal: boolean;
  identifier?: Identifier;
  coverage: Reference<Coverage>;
  businessArrangement?: string;
  preAuthRef?: string[];
  claimResponse?: Reference<ClaimResponse>;
}

type accidentObj = accidentObjBase & location;

interface accidentObjBase extends BackboneElement {
  date: date;
  type: CodeableConcept<v3_ActIncidentCode>;
}

type itemObj = itemObjBase & serviced & location2;

interface itemObjBase extends BackboneElement {
  sequence: positiveInt;
  careTeamSequence: positiveInt[];
  diagnosisSequence: positiveInt[];
  procedureSequence: positiveInt[];
  informationSequence: positiveInt[];
  revenue?: CodeableConcept<ExampleRevenueCenterCodes>;
  category?: CodeableConcept<BenefitCategoryCodes>;
  productOrService: CodeableConcept<USCLSCodes>;
  modifier?: CodeableConcept<ModifierTypeCodes>[];
  programCode?: CodeableConcept<ExampleProgramReasonCodes>[];
  quantity?: SimpleQuantity;
  unitPrice?: Money;
  factor?: decimal;
  net?: Money;
  udi?: Reference<Device>[];
  bodySite?: CodeableConcept<OralSiteCodes>;
  subSite?: CodeableConcept<SurfaceCodes>[];
  encounter?: Reference<Encounter>[];
  detail?: BackboneElement & {
    sequence: positiveInt;
    revenue?: CodeableConcept<ExampleRevenueCenterCodes>;
    category?: CodeableConcept<BenefitCategoryCodes>;
    productOrService: CodeableConcept<USCLSCodes>;
    modifier?: CodeableConcept<ModifierTypeCodes>[];
    programCode?: CodeableConcept<ExampleProgramReasonCodes>[];
    quantity?: SimpleQuantity;
    unitPrice?: Money;
    factor?: decimal;
    net?: Money;
    udi?: Reference<Device>[];
    subDetail?: BackboneElement & {
      sequence: positiveInt;
      revenue?: CodeableConcept<ExampleRevenueCenterCodes>;
      category?: CodeableConcept<BenefitCategoryCodes>;
      productOrService: CodeableConcept<USCLSCodes>;
      modifier?: CodeableConcept<ModifierTypeCodes>[];
      programCode?: CodeableConcept<ExampleProgramReasonCodes>[];
      quantity?: SimpleQuantity;
      unitPrice?: Money;
      factor?: decimal;
      net?: Money;
      udi?: Reference<Device>[];
    }[];
  }[];
}

type timing = timingDate | timingPeriod;

type value = valueBoolean | valueString | valueQuantity | valueAttachment | valueReference;

type diagnosis = diagnosisCodeableConcept | diagnosisReference;

type procedure = procedureCodeableConcept | procedureReference;

type location = locationAddress | locationReference;

type serviced = servicedDate | servicedPeriod;

type location2 = locationCodeableConcept | locationAddress | locationReference;

interface timingDate {
  timingDate?: date;
}

interface timingPeriod {
  timingPeriod?: Period;
}

interface valueBoolean {
  valueBoolean?: boolean;
}

interface valueString {
  valueString?: string;
}

interface valueQuantity {
  valueQuantity?: Quantity;
}

interface valueAttachment {
  valueAttachment?: Attachment;
}

interface valueReference {
  valueReference?: Reference<any>;
}

interface diagnosisCodeableConcept {
  diagnosisCodeableConcept: CodeableConcept<ICD_10Codes>;
}

interface diagnosisReference {
  diagnosisReference: Reference<Condition>;
}

interface procedureCodeableConcept {
  procedureCodeableConcept: CodeableConcept<ICD_10ProcedureCodes>;
}

interface procedureReference {
  procedureReference: Reference<Procedure>;
}

interface locationCodeableConcept {
  locationCodeableConcept?: CodeableConcept<ExampleServicePlaceCodes>;
}

interface locationAddress {
  locationAddress?: Address;
}

interface locationReference {
  locationReference?: Reference<Location>;
}

interface servicedDate {
  servicedDate?: date;
}

interface servicedPeriod {
  servicedPeriod?: Period;
}
