import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  Attachment,
  CodeableConcept,
  Coding,
  Identifier, Money,
  Period,
  Quantity,
  Signature, SimpleQuantity, Timing
} from '../foundation/base-types/general-purpose'
import {
  ArrayOfOneOrMore,
  code,
  date,
  dateTime,
  decimal,
  integer,
  markdown,
  time,
  unsignedInt,
  uri
} from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Organization } from '../administration/organization'
import { Location } from '../administration/location'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Device } from '../administration/device'
import { Group } from '../administration/group'
import {
  ConsentContentClass,
  ContractActionCodes,
  ContractActorRoleCodes,
  ContractContentDerivationCodes,
  ContractResourceActionStatusCodes,
  ContractResourceAssetAvailiabilityCodes,
  ContractResourceAssetContextCodes,
  ContractResourceAssetScopeCodes,
  ContractResourceAssetSubtypeCodes,
  ContractResourceAssetTypeCodes, ContractResourceDecisionModeCodes,
  ContractResourceDefinitionSubtypeCodes,
  ContractResourceDefinitionTypeCodes,
  ContractResourceExpirationTypeCodes,
  ContractResourceLegalStateCodes, ContractResourcePartyRoleCodes,
  ContractResourcePublicationStatusCodes,
  ContractResourceScopeCodes,
  ContractResourceSecurityControlCodes,
  ContractResourceStatusCodes,
  ContractSignerTypeCodes,
  ContractSubtypeCodes,
  ContractTermSubtypeCodes,
  ContractTermTypeCodes,
  ContractTypeCodes, ProvenanceParticipantRole,
  ProvenanceParticipantType, v3_ActConsentDirective,
  v3_PurposeOfUse
} from '../value-sets/value-sets-4-0'
import { Encounter } from '../administration/encounter'
import { EpisodeOfCare } from '../administration/episode-of-care'
import { CareTeam } from '../clinical/careteam'
import { Substance } from '../administration/substance'
import { Condition } from '../clinical/condition'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Questionaire } from '../foundation/content-management/questionaire'
import { QuestionaireResponse } from '../foundation/content-management/questionaire-response'
import { Provenance } from '../security/provenance'
import { Composition } from '../foundation/content-management/composition'

/*
Legally enforceable, formally recorded unilateral or bilateral directive i.e., a policy or agreement.
 */
export type Contract = ContractBase & topic & legallyBinding;

interface ContractBase extends DomainResource {
  identifier?: Identifier[];
  url?: uri;
  version?: string;
  status?: code<ContractResourceStatusCodes>;
  legalState?: CodeableConcept<ContractResourceLegalStateCodes>;
  instantiatesCanonical?: Reference<Contract>;
  instantiatesUri?: uri;
  contentDerivative?: CodeableConcept<ContractContentDerivationCodes>;
  issued?: dateTime;
  applies?: Period;
  expirationType?: CodeableConcept<ContractResourceExpirationTypeCodes>;
  subject?: Reference<any>[];
  authority?: Reference<Organization>[];
  domain?: Reference<Location>[];
  site?: Reference<Location>[];
  name?: string;
  title?: string;
  subtitle?: string;
  alias?: string[];
  author?: Reference<Patient | Practitioner | PractitionerRole | Organization>;
  scope?: CodeableConcept<ContractResourceScopeCodes>;
  type?: CodeableConcept<ContractTypeCodes>;
  subType?: CodeableConcept<ContractSubtypeCodes>[];
  contentDefinition?: BackboneElement & {
    type?: CodeableConcept<ContractResourceDefinitionTypeCodes>;
    subType?: CodeableConcept<ContractResourceDefinitionSubtypeCodes>[];
    publisher?: Reference<Practitioner | PractitionerRole | Organization>;
    publicationDate?: dateTime;
    publicationStatus: code<ContractResourcePublicationStatusCodes>;
    copyright?: markdown;
  };
  term?: termObj[];
  supportingInfo?: Reference<any>[];
  relevantHistory?: Reference<Provenance>[];
  signer?: BackboneElement & {
    type: Coding<ContractSignerTypeCodes>;
    party: Reference<Organization | Patient | Practitioner | PractitionerRole | RelatedPerson>;
    signature: ArrayOfOneOrMore<Signature>;
  }[];
  friendly?: (BackboneElement & content)[];
  legal?: (BackboneElement & content)[];
  rule?: (BackboneElement & content)[];
}

type topic = topicCodeableConcept | topicReference;

interface topicCodeableConcept {
  topicCodeableConcept?: CodeableConcept<any>;
}

interface topicReference {
  topicReference?: Reference<any>;
}

type legallyBinding = legallyBindingAttachment | legallyBindingReference;

interface legallyBindingAttachment {
  legallyBindingAttachment?: Attachment;
}

interface legallyBindingReference {
  legallyBindingReference?: Reference<Composition | DocumentReference | QuestionaireResponse | Contract>;
}

type termObj = termObjBase & topic;

interface termObjBase extends BackboneElement {
  identifier?: Identifier;
  issued?: dateTime;
  applies?: Period;
  type?: CodeableConcept<ContractTermTypeCodes>;
  subType?: CodeableConcept<ContractTermSubtypeCodes>;
  text?: string;
  securityLabel?: BackboneElement & {
    number?: unsignedInt[];
    classification: Coding<ContractResourceScopeCodes>;
    category?: Coding<ContractResourceScopeCodes>[];
    control?: Coding<ContractResourceSecurityControlCodes>[];
  }[];
  offer: offerObj;
  asset?: BackboneElement & {
    scope?: CodeableConcept<ContractResourceAssetScopeCodes>;
    type?: CodeableConcept<ContractResourceAssetTypeCodes>[];
    typeReference?: Reference<any>[];
    subtype?: CodeableConcept<ContractResourceAssetSubtypeCodes>[];
    relationship?: Coding<ConsentContentClass>;
    context?: BackboneElement & {
      reference?: Reference<any>;
      code?: CodeableConcept<ContractResourceAssetContextCodes>[];
      text?: string;
    }[];
    condition?: string;
    periodType?: CodeableConcept<ContractResourceAssetAvailiabilityCodes>[];
    period?: Period[];
    usePeriod?: Period[];
    text?: string;
    linkId?: string[];
    answer?: (BackboneElement & value)[];
    securityLabelNumber?: unsignedInt[];
    valuedItem?: valuedItemObj[];
  }[];
  action?: actionObj[];
  group?: termObj[];
}

type entity = entityCodeableConcept | entityReference;

interface entityCodeableConcept {
  entityCodeableConcept?: CodeableConcept<any>;
}

interface entityReference {
  entityReference?: Reference<any>;
}

type valuedItemObj = valuedItemBase & entity;

interface valuedItemBase extends BackboneElement {
  identifier?: Identifier;
  effectiveTime?: dateTime;
  quantity?: SimpleQuantity;
  unitPrice?: Money;
  factor?: decimal;
  points?: decimal;
  net?: Money;
  payment?: string;
  paymentDate?: dateTime;
  responsible?: Reference<Organization | Patient | Practitioner | PractitionerRole | RelatedPerson>;
  recipient?: Reference<Organization | Patient | Practitioner | PractitionerRole | RelatedPerson>;
  linkId?: string[];
  securityLabelNumber?: unsignedInt[];
}

type actionObj = actionObjBase & occurrence;

interface actionObjBase extends BackboneElement {
  doNotPerform?: boolean;
  type: CodeableConcept<ContractActionCodes>;
  subject?: BackboneElement & {
    reference: ArrayOfOneOrMore<Reference<Patient | RelatedPerson | Practitioner | PractitionerRole | Device | Group | Organization>>;
    role?: CodeableConcept<ContractActorRoleCodes>;
  }[];
  intent: CodeableConcept<v3_PurposeOfUse>;
  linkId?: string[];
  status: CodeableConcept<ContractResourceActionStatusCodes>;
  context?: Reference<Encounter | EpisodeOfCare>;
  contextLinkId?: string[];
  requester?: Reference<Patient | RelatedPerson | Practitioner | PractitionerRole | Device | Group | Organization>[];
  requesterLinkId?: string[];
  performerType?: CodeableConcept<ProvenanceParticipantType>[];
  performerRole?: CodeableConcept<ProvenanceParticipantRole>;
  performer?: Reference<RelatedPerson | Patient | Practitioner | PractitionerRole | CareTeam | Device | Substance | Organization | Location>;
  performerLinkId?: string[];
  reasonCode?: CodeableConcept<v3_PurposeOfUse>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport | DocumentReference | Questionaire | QuestionaireResponse>[];
  reason?: string[];
  reasonLinkId?: string[];
  note?: Annotation[];
  securityLabelNumber?: unsignedInt[];
}

interface offerObj extends BackboneElement {
  identifier?: Identifier[];
  party?: BackboneElement & {
    reference: ArrayOfOneOrMore<Reference<Patient | RelatedPerson | Practitioner | PractitionerRole | Device | Group | Organization>>;
    role: CodeableConcept<ContractResourcePartyRoleCodes>;
  }[];
  topic?: Reference<any>;
  type?: CodeableConcept<ContractTermTypeCodes>;
  decision?: CodeableConcept<v3_ActConsentDirective>;
  decisionMode?: CodeableConcept<ContractResourceDecisionModeCodes>[];
  answer?: (BackboneElement & value)[];
  text?: string;
  linkId?: string[];
  securityLabelNumber?: unsignedInt[];
}

type value = valueBoolean | valueDecimal | valueInteger | valueDate | valueDateTime | valueTime | valueString | valueUri | valueAttachment | valueCoding | valueQuantity | valueReference;

type occurrence = occurrenceDateTime | occurrencePeriod | occurrenceTiming;

interface valueBoolean {
  valueBoolean: boolean;
}

interface valueDecimal {
  valueDecimal: decimal;
}

interface valueInteger {
  valueInteger: integer;
}

interface valueDate {
  valueDate: date;
}

interface valueDateTime {
  valueDateTime: dateTime;
}

interface valueTime {
  valueTime: time;
}

interface valueString {
  valueString: string;
}

interface valueUri {
  valueUri: uri;
}

interface valueAttachment {
  valueAttachment: Attachment;
}

interface valueCoding {
  valueCoding: Coding;
}

interface valueQuantity {
  valueQuantity?: Quantity;
}

interface valueReference {
  valueReference?: Reference<any>;
}

interface occurrenceDateTime {
  occurrenceDateTime?: dateTime;
}

interface occurrencePeriod {
  occurrencePeriod?: Period;
}

interface occurrenceTiming {
  occurrenceTiming?: Timing;
}

type content = contentAttachment | contentReference;

interface contentAttachment {
  contentAttachment: Attachment;
}

interface contentReference {
  contentReference: Reference<Composition | DocumentReference | QuestionaireResponse>;
}
