import { DomainResource } from '../foundation/framework/domain-resource'
import { ArrayOfOneOrMore, code, date, dateTime, unsignedInt, uri } from '../primitives'
import { CodeableConcept, Identifier, Money, Period } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { CoverageEligibilityRequest } from './coverage-eligibility-request'
import { Coverage } from './coverage'
import {
  AdjudicationErrorCodes,
  BenefitCategoryCodes,
  BenefitTermCodes, BenefitTypeCodes,
  ClaimProcessingCodes,
  CoverageEligibilityResponseAuthSupportCodes,
  EligibilityResponsePurpose,
  FinancialResourceStatusCodes, FormCodes,
  ModifierTypeCodes,
  NetworkTypeCodes,
  UnitTypeCodes,
  USCLSCodes
} from '../value-sets/value-sets-4-0'

/*
This resource provides eligibility and plan details from the processing of an CoverageEligibilityRequest resource.
 */
export type CoverageEligibilityResponse = CoverageEligibilityResponseBase & serviced;

interface CoverageEligibilityResponseBase extends DomainResource {
  identifier?: Identifier[];
  status: code<FinancialResourceStatusCodes>;
  purpose: ArrayOfOneOrMore<code<EligibilityResponsePurpose>>;
  patient: Reference<Patient>;
  created: dateTime;
  requestor?: Reference<Practitioner | PractitionerRole | Organization>;
  request: Reference<CoverageEligibilityRequest>;
  outcome: code<ClaimProcessingCodes>;
  disposition?: string;
  insurer: Reference<Organization>;
  insurance?: BackboneElement & {
    coverage: Reference<Coverage>;
    inforce?: boolean;
    benefitPeriod?: Period;
    item?: BackboneElement & {
      category?: CodeableConcept<BenefitCategoryCodes>;
      productOrService?: CodeableConcept<USCLSCodes>;
      modifier?: CodeableConcept<ModifierTypeCodes>[];
      provider?: Reference<Practitioner | PractitionerRole>;
      excluded?: boolean;
      name?: string;
      description?: string;
      network?: CodeableConcept<NetworkTypeCodes>;
      unit?: CodeableConcept<UnitTypeCodes>;
      term?: CodeableConcept<BenefitTermCodes>;
      benefit?: benefitObj[];
      authorizationRequired?: boolean;
      authorizationSupporting?: CodeableConcept<CoverageEligibilityResponseAuthSupportCodes>[];
      authorizationUrl?: uri;
    }[];
  }[];
  preAuthRef?: string;
  form?: CodeableConcept<FormCodes>;
  error?: BackboneElement & {
    code: CodeableConcept<AdjudicationErrorCodes>;
  }[];
}

type serviced = servicedDate | servicedPeriod;

interface servicedDate {
  servicedDate?: date;
}

interface servicedPeriod {
  servicedPeriod?: Period;
}

type benefitObj = benefitObjBase & allowed & used;

interface benefitObjBase extends BackboneElement {
  type: CodeableConcept<BenefitTypeCodes>;
}

type allowed = allowedUnsignedInt | allowedString | allowedMoney;

type used = usedUnsignedInt | usedString | usedMoney;

interface allowedUnsignedInt {
  allowedUnsignedInt?: unsignedInt;
}

interface allowedString {
  allowedString?: string;
}

interface allowedMoney {
  allowedMoney?: Money;
}

interface usedUnsignedInt {
  usedUnsignedInt?: unsignedInt;
}

interface usedString {
  usedString?: string;
}

interface usedMoney {
  usedMoney?: Money;
}
