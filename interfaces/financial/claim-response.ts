import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Address,
  Attachment,
  CodeableConcept,
  Identifier,
  Money,
  Period,
  SimpleQuantity
} from '../foundation/base-types/general-purpose'
import {
  AdjudicationErrorCodes,
  AdjudicationReasonCodes,
  AdjudicationValueCodes,
  ClaimPayeeTypeCodes,
  ClaimProcessingCodes,
  ClaimTypeCodes,
  CommonLanguages,
  ExampleClaimSubTypeCodes, ExamplePaymentTypeCodes,
  ExampleProgramReasonCodes,
  ExampleServicePlaceCodes,
  FinancialResourceStatusCodes,
  FormCodes, FundsReservationCodes,
  ModifierTypeCodes,
  NoteType,
  OralSiteCodes,
  PaymentAdjustmentReasonCodes,
  SurfaceCodes,
  USCLSCodes,
  Use
} from '../value-sets/value-sets-4-0'
import { ArrayOfOneOrMore, code, date, dateTime, decimal, positiveInt } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Organization } from '../administration/organization'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Claim } from './claim'
import { CommunicationRequest } from '../administration/communication-request'
import { Coverage } from './coverage'
import { Location } from '../administration/location'

/*
This resource provides the adjudication details from the processing of a Claim resource.
 */
export interface ClaimResponse extends DomainResource {
  identifier?: Identifier[];
  status: code<FinancialResourceStatusCodes>;
  type: CodeableConcept<ClaimTypeCodes>;
  subType?: CodeableConcept<ExampleClaimSubTypeCodes>;
  use: code<Use>;
  patient: Reference<Patient>;
  created: dateTime;
  insurer: Reference<Organization>;
  requestor?: Reference<Practitioner | PractitionerRole | Organization>;
  request?: Reference<Claim>;
  outcome: code<ClaimProcessingCodes>;
  disposition?: string;
  preAuthRef?: string;
  preAuthPeriod?: Period;
  payeeType?: CodeableConcept<ClaimPayeeTypeCodes>;
  item?: BackboneElement & {
    itemSequence: positiveInt;
    noteNumber?: positiveInt[];
    adjudication: ArrayOfOneOrMore<adjudicationObj>;
    detail?: BackboneElement & {
      detailSequence: positiveInt;
      noteNumber?: positiveInt[];
      adjudication: ArrayOfOneOrMore<adjudicationObj>;
      subDetail?: BackboneElement & {
        subDetailSequence: positiveInt;
        noteNumber?: positiveInt[];
        adjudication?: adjudicationObj[];
      }[];
    }[];
  }[];
  addItem?: addItemObj[];
  adjudication?: adjudicationObj[];
  total?: BackboneElement & {
    category: CodeableConcept<AdjudicationValueCodes>;
    amount: Money;
  }[];
  payment?: BackboneElement & {
    type: CodeableConcept<ExamplePaymentTypeCodes>;
    adjustment?: Money;
    adjustmentReason?: CodeableConcept<PaymentAdjustmentReasonCodes>;
    date?: date;
    amount: Money;
    identifier?: Identifier;
  }[];
  fundsReserve?: CodeableConcept<FundsReservationCodes>;
  formCode?: CodeableConcept<FormCodes>;
  form?: Attachment;
  processNote?: BackboneElement & {
    number?: positiveInt;
    type?: code<NoteType>;
    text: string;
    language?: CodeableConcept<CommonLanguages>;
  }[];
  communicationRequest?: Reference<CommunicationRequest>[];
  insurance?: BackboneElement & {
    sequence: positiveInt;
    focal: boolean;
    coverage: Reference<Coverage>;
    businessArrangement?: string;
    claimResponse?: Reference<ClaimResponse>;
  }[];
  error?: BackboneElement & {
    itemSequence?: positiveInt;
    detailSequence?: positiveInt;
    subDetailSequence?: positiveInt;
    code: CodeableConcept<AdjudicationErrorCodes>;
  }[];
}

interface adjudicationObj extends BackboneElement {
  category: CodeableConcept<AdjudicationValueCodes>;
  reason?: CodeableConcept<AdjudicationReasonCodes>;
  amount?: Money;
  value?: decimal;
}

type addItemObj = addItemObjBase & serviced & location;

interface addItemObjBase extends BackboneElement {
  itemSequence?: positiveInt[];
  detailSequence?: positiveInt[];
  subdetailSequence?: positiveInt[];
  provider?: Reference<Practitioner | PractitionerRole | Organization>[];
  productOrService: CodeableConcept<USCLSCodes>;
  modifier?: CodeableConcept<ModifierTypeCodes>[];
  programCode?: CodeableConcept<ExampleProgramReasonCodes>[];
  quantity?: SimpleQuantity;
  unitPrice?: Money;
  factor?: decimal;
  net?: Money;
  bodySite?: CodeableConcept<OralSiteCodes>;
  subSite?: CodeableConcept<SurfaceCodes>;
  noteNumber?: positiveInt[];
  adjudication: ArrayOfOneOrMore<adjudicationObj>;
  detail?: BackboneElement & {
    productOrService: CodeableConcept<USCLSCodes>;
    modifier?: CodeableConcept<ModifierTypeCodes>[];
    quantity?: SimpleQuantity;
    unitPrice?: Money;
    factor?: decimal;
    net?: Money;
    noteNumber?: positiveInt[];
    adjudication: ArrayOfOneOrMore<adjudicationObj>;
    subDetail?: BackboneElement & {
      productOrService: CodeableConcept<USCLSCodes>;
      modifier?: CodeableConcept<ModifierTypeCodes>[];
      quantity?: SimpleQuantity;
      unitPrice?: Money;
      factor?: decimal;
      net?: Money;
      noteNumber?: positiveInt[];
      adjudication: ArrayOfOneOrMore<adjudicationObj>;
    }[];
  }[];
}

type serviced = servicedDate | servicedPeriod;

type location = locationCodeableConcept | locationAddress | locationReference;

interface servicedDate {
  servicedDate?: date;
}

interface servicedPeriod {
  servicedPeriod?: Period;
}

interface locationCodeableConcept {
  locationCodeableConcept?: CodeableConcept<ExampleServicePlaceCodes>;
}

interface locationAddress {
  locationAddress?: Address;
}

interface locationReference {
  locationReference?: Reference<Location>;
}
