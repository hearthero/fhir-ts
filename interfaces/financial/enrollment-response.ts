import { DomainResource } from '../foundation/framework/domain-resource'
import { Identifier } from '../foundation/base-types/general-purpose'
import { code, dateTime } from '../primitives'
import { Reference } from '../foundation/base-types/special-purpose'
import { EnrollmentRequest } from './enrollment-request'
import { Organization } from '../administration/organization'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { ClaimProcessingCodes, FinancialResourceStatusCodes } from '../value-sets/value-sets-4-0'

/*
This resource provides enrollment and plan details from the processing of an EnrollmentRequest resource.
 */
export interface EnrollmentResponse extends DomainResource {
  identifier?: Identifier[];
  status?: code<FinancialResourceStatusCodes>;
  request?: Reference<EnrollmentRequest>;
  outcome?: code<ClaimProcessingCodes>;
  disposition?: string;
  created?: dateTime;
  organization?: Reference<Organization>;
  requestProvider?: Reference<Practitioner | PractitionerRole | Organization>;
}
