type urlProtocol = 'http' | 'https' | 'ftp' | 'mailto' | 'mllp';

// FHIR Primitives (see https://www.hl7.org/fhir/datatypes.html#primitive)

export type integer = number;
export type decimal = number;
export type uri<T = string> = T;
export type url = `${urlProtocol}:${string}`;
export type canonical<T> = uri<T>;
export type base64Binary = string;
export type instant = string;
export type date = string;
export type dateTime = string;
export type time = string;
export type code<T = string> = T;
export type oid = string | uri;
export type id = string;
export type markdown = string;
export type unsignedInt = number;
export type positiveInt = number;
export type uuid = string | uri;

// Additional Types that are required but not named after FHIR Resources

export type AnyPrimitive = integer | decimal | uri | url | canonical<any> | base64Binary | instant | date | dateTime | time | code | oid | id | markdown | unsignedInt | positiveInt | uuid | string | number | boolean;
export type ArrayOfOneOrMore<T> = [T, ...T[]];

// Additional Types END
