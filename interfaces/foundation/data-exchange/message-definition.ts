import { DomainResource } from '../framework/domain-resource'
import { CodeableConcept, Identifier } from '../base-types/general-purpose'
import { canonical, code, dateTime, markdown, unsignedInt, uri } from '../../primitives'
import { Jurisdiction, PublicationStatus, ResourceType } from '../../value-sets/value-sets-4-0'
import { StructureDefinition } from '../../conformance/structure-definition'
import { ContactDetail, UsageContext } from '../base-types/metadata'
import { BackboneElement } from '../base-types/special-purpose'
import { ActivityDefinition } from '../../workflow/activity-definition'
import { PlanDefinition } from '../../workflow/plan-definition'

export interface MessageDefinition extends DomainResource {
  url?: uri;
  identifier: Identifier;
  version?: string;
  name?: string;
  title?: string;
  replaces?: canonical<MessageDefinition>[];
  status: code<PublicationStatus>;
  experimental?: boolean;
  date: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  copyright?: markdown;
  base?: canonical<MessageDefinition>;
  parent?: canonical<ActivityDefinition | PlanDefinition>[];
  event: unknown; // TODO
  category?: code<MessageSignificanceCategory>;
  focus?: BackboneElement & {
    code: code<ResourceType>;
    profile?: canonical<StructureDefinition>;
    min: unsignedInt;
    max?: string;
  };
  responseRequired?: code<MessageHeaderResponseRequest>;
  allowedResponse?: BackboneElement & {
    message: canonical<MessageDefinition>;
    situation?: markdown;
  }[];
  graph?: canonical<GraphDefinition>[];
}
