import { DomainResource } from '../framework/domain-resource'
import { code, instant, url } from '../../primitives'
import { ContactPoint } from '../base-types/general-purpose'
import { BackboneElement } from '../base-types/special-purpose'
import { MimeType } from '../../value-sets/value-sets-4-0'

/*
The subscription resource is used to define a push-based subscription from a server to another system. Once a subscription is registered with the server, the server checks every resource that is created or updated, and if the resource matches the given criteria, it sends a message on the defined "channel" so that another system can take an appropriate action.
 */
export interface Subscription extends DomainResource {
  status: code<SubscriptionStatus>;
  contact?: ContactPoint[];
  end?: instant;
  reason: string;
  criteria: string;
  error?: string;
  channel: BackboneElement & {
    type: code<SubscriptionChannelType>;
    endpoint?: url;
    payload?: code<MimeType>;
    header?: string[];
  };
}
