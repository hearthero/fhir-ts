import {DomainResource} from '../framework/domain-resource';
import {OperationOutcome} from './operation-outcome';
import { canonical, code, id, url } from '../../primitives'
import { BackboneElement, Reference } from '../base-types/special-purpose'
import { Device } from '../../administration/device'
import { Practitioner } from '../../administration/practitioner'
import { PractitionerRole } from '../../administration/practitioner-role'
import { Organization } from '../../administration/organization'
import { CodeableConcept, ContactPoint } from '../base-types/general-purpose'
import { MessageDefinition } from './message-definition'

/*
The header for a message exchange that is either requesting or responding to an action. The reference(s) that are the subject of the action as well as other information related to the action are typically transmitted in a bundle in which the MessageHeader resource instance is the first resource in the bundle.
 */
export interface MessageHeader extends DomainResource {
  event: unknown; // TODO
  destination?: BackboneElement & {
    name?: string;
    target?: Reference<Device>;
    endpoint: url;
    receiver?: Reference<Practitioner | PractitionerRole | Organization>;
  }[];
  sender?: Reference<Practitioner | PractitionerRole | Organization>;
  enterer?: Reference<Practitioner | PractitionerRole>;
  author?: Reference<Practitioner | PractitionerRole>;
  source: BackboneElement & {
    name?: string;
    software?: string;
    version?: string;
    contact?: ContactPoint;
    endpoint: url;
  };
  responsible?: Reference<Practitioner | PractitionerRole | Organization>;
  reason?: CodeableConcept<ExampleMessageReasonCodes>[];
  response?: BackboneElement & {
    identifier: id;
    code: code<ResponseType>;
    details?: Reference<OperationOutcome>;
  };
  focus?: Reference<any>[];
  definition?: canonical<MessageDefinition>;
}
