import { DomainResource } from '../framework/domain-resource'
import { code } from '../../primitives'
import { CodeableConcept } from '../base-types/general-purpose'
import { BackboneElement } from '../base-types/special-purpose'

/*
A collection of error, warning, or information messages that result from a system action.
 */
export interface OperationOutcome extends DomainResource {
  issue: BackboneElement & {
    severity: code<IssueSeverity>;
    code: code<IssueType>;
    details?: CodeableConcept<OperationOutcomeCodes>;
    diagnostics?: string;
    // location?: string[]; DEPRECATED
    expression?: string[];
  }
}
