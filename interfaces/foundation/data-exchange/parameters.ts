import { Resource } from '../framework/resource'

interface parameterObj extends Resource {
  name: string;
  value?: anyFHIR; // TODO
  resource?: Resource;
  part?: parameterObj[];
}

/*
This resource is a non-persisted resource used to pass information into and back from an operation. It has no other use, and there is no RESTful endpoint associated with it.
 */
export interface Parameters {
  parameter?: parameterObj[];
}
