import { Element } from './element'
import {
  canonical,
  code,
  date,
  dateTime,
  decimal,
  id,
  instant,
  integer,
  markdown, positiveInt,
  time,
  unsignedInt,
  uri
} from '../../primitives'
import { CodeableConcept, Coding, Identifier, Quantity, Ratio, SimpleQuantity, Timing } from './general-purpose'
import { StructureDefinition } from '../../conformance/structure-definition'
import {
  AggregationMode,
  BindingStrength,
  ConstraintSeverity,
  DiscriminatorType,
  DoseAndRateType,
  LOINCCodes,
  MimeType,
  PropertyRepresentation,
  ReferenceVersionRules,
  SlicingRules,
  SNOMEDCTAdditionalDosageInstructions,
  SNOMEDCTAdministrationMethodCodes,
  SNOMEDCTAnatomicalStructureForAdministrationSiteCodes,
  SNOMEDCTRouteCodes
} from '../../value-sets/value-sets-4-0'
import { ImplementationGuide } from '../../conformance/implementation-guide'
import { ValueSet } from '../../terminology/value-set'

export type xhtml = `${'<div xmlns="http://www.w3.org/1999/xhtml">'}${string}${'</div>'}`

/*
Many of the defined elements in a resource are references to other resources. Using these references, the resources combine to build a web of information about healthcare.

References are always defined and represented in one direction - from one resource (source) to another (target). The corresponding reverse relationship from the target to the source exists in a logical sense, but is not typically represented explicitly in the target resource. For external references, navigating these reverse relationships requires some external infrastructure to track the relationship between resources (the REST API provides one such infrastructure by providing the ability to search the reverse relationship by naming search parameters for the references and by providing support for reverse includes).
 */
export interface Reference<T> extends Element {
  reference?: string;
  type?: uri<T>;
  identifier?: Identifier;
  display?: string;
}

/*
  Each resource contains an element "meta", of type "Meta", which is a set of metadata that provides technical and workflow context to the resource. The metadata items are all optional, though some or all of them may be required in particular implementations or contexts of use.
 */
export interface Meta {
  versionId: id;
  lastUpdates: instant;
  source: uri;
  profile: canonical<StructureDefinition>;
  security: Coding;
  tag: Coding;
}

/*
  Any resource that is a DomainResource (all resources except Bundle, Parameters and Binary) may include a human-readable narrative that contains a summary of the resource and may be used to represent the content of the resource to a human.
 */
export interface Narrative {
  status: code;
  div: xhtml;
}

/*
  Every element in a resource or data type includes an optional "extension" child element that may be present any number of times.
 */
export interface Extension extends Element {
  url: uri;
  value: unknown; // TODO
}

/*
The base definition for complex elements defined as part of a resource definition - that is, elements that have children that are defined in the resource. Data Type elements do not use this type, though a few data types specialize it (Timing, Dosage, ElementDefinition). For instance, Patient.contact is an element that is defined as part of the patient resource, so it automatically has the type BackboneElement.
 */
export interface BackboneElement extends Element {
  modifierExtension?: Extension[];
}

interface IElementDefinition extends BackboneElement {
  path: string;
  representation?: code<PropertyRepresentation>[];
  sliceName?: string;
  sliceIsConstraining?: boolean;
  label?: string;
  code?: Coding<LOINCCodes>[];
  slicing?: Element & {
    discriminator?: Element & {
      type: code<DiscriminatorType>;
      path: string;
    };
    description?: string;
    ordered?: boolean;
    rules: code<SlicingRules>;
  },
  short?: string;
  definition?: markdown;
  comment?: markdown;
  requirements?: markdown;
  alias?: string[];
  min?: unsignedInt;
  max?: string;
  base?: Element & {
    path: string;
    min: unsignedInt;
    max: string;
  };
  contentReference?: uri;
  type?: Element & {
    code: uri // TODO: FHIRDefinedType
    profile?: canonical<StructureDefinition | ImplementationGuide>[];
    targetProfile?: canonical<StructureDefinition | ImplementationGuide>[];
    aggregation?: code<AggregationMode>[];
    versioning?: code<ReferenceVersionRules>;
  }[];
  defaultValue?: anyFHIR;
  meaningWhenMissing?: markdown;
  orderMeaning?: string;
  fixed?: anyFHIR;
  pattern?: anyFHIR;
  example?: Element & {
    label: string;
    value: anyFHIR;
  }[];
  maxLength?: integer;
  condition?: id[];
  constraint?: Element & {
    key: id;
    requirements?: string;
    severity: code<ConstraintSeverity>;
    human: string;
    expression?: string;
    xpath?: string;
    source?: canonical<StructureDefinition>;
  }[];
  mustSupport?: boolean;
  isModifier?: boolean;
  isModifierReason?: string;
  isSummary?: boolean;
  binding?: Element & {
    strength: code<BindingStrength>;
    description?: string;
    valueSet?: canonical<ValueSet>;
  };
  mapping?: Element & {
    identity: id;
    language?: code<MimeType>;
    map: string;
    comment?: string;
  }[];
}

// Helper types

type minValueDate = {
  minValueDate?: date;
  minValueDateTime: never;
  minValueInstant: never;
  minValueTime: never;
  minValueDecimal: never;
  minValueInteger: never;
  minValuePositiveInt: never;
  minValueUnsignedInt: never;
  minValueQuantity: never;
}

type minValueDateTime = {
  minValueDate: never;
  minValueDateTime?: dateTime;
  minValueInstant: never;
  minValueTime: never;
  minValueDecimal: never;
  minValueInteger: never;
  minValuePositiveInt: never;
  minValueUnsignedInt: never;
  minValueQuantity: never;
}

type minValueInstant = {
  minValueDate: never;
  minValueDateTime: never;
  minValueInstant?: instant;
  minValueTime: never;
  minValueDecimal: never;
  minValueInteger: never;
  minValuePositiveInt: never;
  minValueUnsignedInt: never;
  minValueQuantity: never;
}

type minValueTime = {
  minValueDate: never;
  minValueDateTime: never;
  minValueInstant: never;
  minValueTime?: time;
  minValueDecimal: never;
  minValueInteger: never;
  minValuePositiveInt: never;
  minValueUnsignedInt: never;
  minValueQuantity: never;
}

type minValueDecimal = {
  minValueDate: never;
  minValueDateTime: never;
  minValueInstant: never;
  minValueTime: never;
  minValueDecimal?: decimal;
  minValueInteger: never;
  minValuePositiveInt: never;
  minValueUnsignedInt: never;
  minValueQuantity: never;
}

type minValueInteger = {
  minValueDate: never;
  minValueDateTime: never;
  minValueInstant: never;
  minValueTime: never;
  minValueDecimal: never;
  minValueInteger?: integer;
  minValuePositiveInt: never;
  minValueUnsignedInt: never;
  minValueQuantity: never;
}

type minValuePositiveInt = {
  minValueDate: never;
  minValueDateTime: never;
  minValueInstant: never;
  minValueTime: never;
  minValueDecimal: never;
  minValueInteger: never;
  minValuePositiveInt?: positiveInt;
  minValueUnsignedInt: never;
  minValueQuantity: never;
}

type minValueUnsignedInt = {
  minValueDate: never;
  minValueDateTime: never;
  minValueInstant: never;
  minValueTime: never;
  minValueDecimal: never;
  minValueInteger: never;
  minValuePositiveInt: never;
  minValueUnsignedInt?: unsignedInt;
  minValueQuantity: never;
}

type minValueQuantity = {
  minValueDate: never;
  minValueDateTime: never;
  minValueInstant: never;
  minValueTime: never;
  minValueDecimal: never;
  minValueInteger: never;
  minValuePositiveInt: never;
  minValueUnsignedInt: never;
  minValueQuantity?: Quantity;
}

type maxValueDate = {
  maxValueDate?: date;
  maxValueDateTime: never;
  maxValueInstant: never;
  maxValueTime: never;
  maxValueDecimal: never;
  maxValueInteger: never;
  maxValuePositiveInt: never;
  maxValueUnsignedInt: never;
  maxValueQuantity: never;
}

type maxValueDateTime = {
  maxValueDate: never;
  maxValueDateTime?: dateTime;
  maxValueInstant: never;
  maxValueTime: never;
  maxValueDecimal: never;
  maxValueInteger: never;
  maxValuePositiveInt: never;
  maxValueUnsignedInt: never;
  maxValueQuantity: never;
}

type maxValueInstant = {
  maxValueDate: never;
  maxValueDateTime: never;
  maxValueInstant?: instant;
  maxValueTime: never;
  maxValueDecimal: never;
  maxValueInteger: never;
  maxValuePositiveInt: never;
  maxValueUnsignedInt: never;
  maxValueQuantity: never;
}

type maxValueTime = {
  maxValueDate: never;
  maxValueDateTime: never;
  maxValueInstant: never;
  maxValueTime?: time;
  maxValueDecimal: never;
  maxValueInteger: never;
  maxValuePositiveInt: never;
  maxValueUnsignedInt: never;
  maxValueQuantity: never;
}

type maxValueDecimal = {
  maxValueDate: never;
  maxValueDateTime: never;
  maxValueInstant: never;
  maxValueTime: never;
  maxValueDecimal?: decimal;
  maxValueInteger: never;
  maxValuePositiveInt: never;
  maxValueUnsignedInt: never;
  maxValueQuantity: never;
}

type maxValueInteger = {
  maxValueDate: never;
  maxValueDateTime: never;
  maxValueInstant: never;
  maxValueTime: never;
  maxValueDecimal: never;
  maxValueInteger?: integer;
  maxValuePositiveInt: never;
  maxValueUnsignedInt: never;
  maxValueQuantity: never;
}

type maxValuePositiveInt = {
  maxValueDate: never;
  maxValueDateTime: never;
  maxValueInstant: never;
  maxValueTime: never;
  maxValueDecimal: never;
  maxValueInteger: never;
  maxValuePositiveInt?: positiveInt;
  maxValueUnsignedInt: never;
  maxValueQuantity: never;
}

type maxValueUnsignedInt = {
  maxValueDate: never;
  maxValueDateTime: never;
  maxValueInstant: never;
  maxValueTime: never;
  maxValueDecimal: never;
  maxValueInteger: never;
  maxValuePositiveInt: never;
  maxValueUnsignedInt?: unsignedInt;
  maxValueQuantity: never;
}

type maxValueQuantity = {
  maxValueDate: never;
  maxValueDateTime: never;
  maxValueInstant: never;
  maxValueTime: never;
  maxValueDecimal: never;
  maxValueInteger: never;
  maxValuePositiveInt: never;
  maxValueUnsignedInt: never;
  maxValueQuantity?: Quantity;
}

/*
The definition of an element in a resource or an extension. The definition includes:

- Path (name), Cardinality, and data type
- Definitions, usage notes, and requirements
- Default or fixed values
- Constraints, Length limits, and other usage rules
- Terminology Binding
- Mappings to other specifications
- Structural Usage Information such as Slicing
- The ElementDefinition type is the core of the FHIR metadata layer, and is closely (conceptually) aligned to ISO 11179. All the data elements defined in this specification are published as a collection of data elements (XML or JSON).

ElementDefinition is used in StructureDefinition
 */
export type ElementDefinition = IElementDefinition
  & (minValueDate | minValueDateTime | minValueInstant | minValueTime | minValueDecimal | minValueInteger | minValuePositiveInt | minValueUnsignedInt | minValueQuantity)
  & (maxValueDate | maxValueDateTime | maxValueInstant | maxValueTime | maxValueDecimal | maxValueInteger | maxValuePositiveInt | maxValueUnsignedInt | maxValueQuantity)

/*
  The Dosage structure defines general dosage instruction information typically represented in medication requests, medication dispenses and medication statements.
 */
export interface Dosage extends BackboneElement {
  sequence?: integer;
  text?: string;
  additionalInstruction?: CodeableConcept<SNOMEDCTAdditionalDosageInstructions>[];
  patientInstruction?: string;
  timing?: Timing;
  asNeeded?: unknown; // TODO
  site?: CodeableConcept<SNOMEDCTAnatomicalStructureForAdministrationSiteCodes>;
  route?: CodeableConcept<SNOMEDCTRouteCodes>;
  method?: CodeableConcept<SNOMEDCTAdministrationMethodCodes>;
  doseAndRate?: Element & {
    type?: CodeableConcept<DoseAndRateType>;
    dose: unknown; // TODO
    rate: unknown; // TODO
  }[];
  maxDosePerPeriod?: Ratio;
  maxDosePerAdministration?: SimpleQuantity;
  maxDosePerLifetime?: SimpleQuantity;
}
