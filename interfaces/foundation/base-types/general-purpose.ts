import {
  ArrayOfOneOrMore, base64Binary, code, dateTime, decimal, instant,
  markdown, positiveInt, time, unsignedInt, uri, url
} from '../../primitives'
import {
  AddressType, AddressUse, CommonLanguages, ContactPointSystem, ContactPointUse, QuantityComparator, MimeType, NameUse, CurrencyCode,
  IdentifierUse, IdentifierType, TimingAbbreviation, UnitsOfTime, DaysOfWeek, EventTiming, SignatureTypeCodes
} from '../../value-sets/value-sets-4-0'
import { BackboneElement, Reference } from './special-purpose'
import { Element } from './element'
import { Organization } from '../../administration/organization'
import { Practitioner } from '../../administration/practitioner'
import { RelatedPerson } from '../../administration/related-person'
import { Patient } from '../../administration/patient'
import { Device } from '../../administration/device'
import { PractitionerRole } from '../../administration/practitioner-role'

/*
  This type is for containing or referencing attachments - additional data content defined in other formats. The most common use of this type is to include images or reports in some report format such as PDF. However, it can be used for any data that has a MIME type.
 */
export interface Attachment extends Element {
  contentType?: code<MimeType>;
  language?: code<CommonLanguages>;
  data?: base64Binary;
  url?: url;
  size?: unsignedInt;
  hash?: base64Binary;
  title?: string;
  creation?: dateTime;
}

/*
  A Coding is a representation of a defined concept using a symbol from a defined "code system" - see [Using Codes in resources](https://www.hl7.org/fhir/terminologies.html) for more details.
 */
export interface Coding<T = string> extends Element {
  system: uri;
  version: string;
  code: code<T>;
  display: string;
  userSelected: boolean;
}

/*
  A CodeableConcept represents a value that is usually supplied by providing a reference to one or more terminologies or ontologies but may also be defined by the provision of text. This is a common pattern in healthcare data.
 */
export interface CodeableConcept<T> {
  coding?: Coding<T>[];
  text?: string;
}

/*
  A measured amount (or an amount that can potentially be measured).
 */
export interface Quantity {
  value?: decimal;
  comparator?: code<QuantityComparator>;
  unit?: string;
  system?: uri;
  code?: code;
}

/*
  An amount of currency.
 */
export interface Money {
  value?: decimal;
  currency?: code<CurrencyCode>;
}

/*
A set of ordered Quantity values defined by a low and high limit.

A Range specifies a set of possible values; usually, one value from the range applies (e.g. "give the patient between 2 and 4 tablets"). Ranges are typically used in instructions.
 */
export interface Range {
  low?: SimpleQuantity;
  high?: SimpleQuantity;
}

/*
A relationship between two Quantity values expressed as a numerator and a denominator.

The Ratio datatype should only be used to express a relationship of two numbers if the relationship cannot be suitably expressed using a Quantity and a common unit. Where the denominator value is known to be fixed to "1", Quantity should be used instead of Ratio.
 */
export interface Ratio {
  numerator?: Quantity;
  denominator?: Quantity;
}

/*
A time period defined by a start and end date/time.

A period specifies a range of times. The context of use will specify whether the entire range applies (e.g. "the patient was an inpatient of the hospital for this time range") or one value from the period applies (e.g. "give to the patient between 2 and 4 pm on 24-Jun 2013").
 */
export interface Period {
  start?: dateTime;
  end?: dateTime;
}

/*
Data that comes from a series of measurements taken by a device, which may have upper and lower limits. The data type also supports more than one dimension in the data.
*/
export interface SampledData {
  origin: SimpleQuantity;
  period: decimal;
  factor?: decimal;
  lowerLimit?: decimal;
  upperLimit?: decimal;
  dimensions: positiveInt;
  data?: string;
}

/*
A numeric or alphanumeric string that is associated with a single object or entity within a given system. Typically, identifiers are used to connect content in resources to external content available in other frameworks or protocols. Identifiers are associated with objects and may be changed or retired due to human or system process and errors.
 */
export interface Identifier {
  use?: code<IdentifierUse>;
  type?: CodeableConcept<IdentifierType>;
  system?: uri;
  value?: string;
  period?: Period;
  assigner?: Reference<Organization>;
}

/*
A name of a human with text, parts and usage information.
 */
export interface HumanName {
  use?: code<NameUse>;
  text?: string;
  family?: string;
  given?: string[];
  prefix?: string[];
  suffix?: string[];
  period?: Period;
}

/*
  An address expressed using postal conventions (as opposed to GPS or other location definition formats). This data type may be used to convey addresses for use in delivering mail as well as for visiting locations which might not be valid for mail delivery. There are a variety of postal address formats defined around the world.
 */
export interface Address extends Element {
  use?: code<AddressUse>;
  type?: code<AddressType>;
  text?: string;
  line?: string[];
  city?: string;
  district?: string;
  state?: string;
  postalCode?: string;
  country?: string;
  period?: Period;
}

/*
  Details for all kinds of technology-mediated contact points for a person or organization, including telephone, email, etc.
 */
export interface ContactPoint extends Element {
  system?: code<ContactPointSystem>;
  value?: string;
  use?: code<ContactPointUse>;
  rank?: positiveInt;
  period?: Period;
}

/*
Describes the occurrence of an event that may occur multiple times. Timing schedules are used for specifying when events are expected or requested to occur and may also be used to represent the summary of a past or ongoing event. For simplicity, the definitions of Timing components are expressed as 'future' events, but such components can also be used to describe historic or ongoing events.
 */
export interface Timing extends BackboneElement {
  event?: dateTime[];
  repeat?: {
    bounds?: Duration | Range | Period;
    count?: positiveInt;
    countMax?: positiveInt;
    duration?: decimal;
    durationMax?: decimal;
    durationUnit?: code<UnitsOfTime>;
    frequency?: positiveInt;
    frequencyMax?: positiveInt;
    period?: decimal;
    periodMax?: decimal;
    periodUnit?: code<UnitsOfTime>;
    dayOfWeek?: code<DaysOfWeek>[];
    timeOfDay?: time[];
    when?: code<EventTiming>[];
    offset?: unsignedInt;
  }
  code?: CodeableConcept<TimingAbbreviation>;
}

/*
A Signature holds an electronic representation of a signature and its supporting context in a FHIR accessible form. The signature may either be a cryptographic type (XML DigSig or a JWS), which is able to provide non-repudiation proof, or it may be a graphical image that represents a signature or a signature process.
 */
export interface Signature extends Element {
  type: ArrayOfOneOrMore<Coding<SignatureTypeCodes>>;
  when: instant;
  who: Reference<Practitioner | PractitionerRole | RelatedPerson | Patient | Device | Organization>;
  onBehalfOf?: Reference<Practitioner | PractitionerRole | RelatedPerson | Patient | Device | Organization>;
  targetFormat?: code<MimeType>;
  sigFormat?: code<MimeType>
  data?: base64Binary;
}

/*
  A text note which also contains information about who made the statement and when.
 */
export interface Annotation {
  author?: Reference<Practitioner | Patient | RelatedPerson | Organization> | string;
  time?: dateTime;
  text: markdown;
}

// Types of Quantity

export type Age = Quantity;
export type Distance = Quantity;
export type Count = Quantity;
export type Duration = Quantity;
export type MoneyQuantity = Quantity;
export type SimpleQuantity = Quantity;
