import { Attachment, Coding, ContactPoint } from './general-purpose'
import { canonical, code, id, integer, markdown, positiveInt, url } from '../../primitives'
import { StructureDefinition } from '../../conformance/structure-definition'
import { ValueSet } from '../../terminology/value-set'
import { Element } from './element'
import { FHIRAllTypes } from '../../value-sets/value-sets-4-0'

export interface ContactDetail extends Element {
  name?: string;
  telecom?: ContactPoint[];
}

export interface Contributor extends Element {
  type: code<ContributorType>;
  name: string;
  contact?: ContactDetail[];
}

export interface DataRequirement extends Element {
  type: code<FHIRAllTypes>;
  profile?: canonical<StructureDefinition>;
  subject?: unknown; // TODO
  mustSupport?: string[];
  codeFilter?: Element & {
    path?: string;
    searchParam?: string;
    valueSet?: canonical<ValueSet>;
    code?: Coding[];
  }[];
  dateFilter?: Element & {
    path?: string;
    searchParam?: string;
    value: unknown; // TODO
  }[];
  limit?: positiveInt;
  sort?: Element & {
    path: string;
    direction: code<SortDirection>;
  };
}

export interface ParameterDefinition extends Element {
  name?: code;
  use: code<OperationParameterUse>;
  min?: integer;
  max?: string;
  documentation?: string;
  type: code<FHIRAllTypes>;
  profile?: canonical<StructureDefinition>;
}

export interface RelatedArtifact extends Element {
  type: code<RelatedArtifactType>;
  label?: string;
  display?: string;
  citation?: markdown;
  url?: url;
  document?: Attachment;
  resource?: canonical<any>;
}

export interface TriggerDefinition extends Element {
  type: code<TriggerType>;
  name?: string;
  timing?: unknown; // TODO
  data?: DataRequirement[];
  condition?: Expression;
}

export interface Expression extends Element {
  description?: string;
  name?: id;
  language: code<ExpressionLanguage>;
  expression?: string;
  reference?: url;
}

export interface UsageContext extends Element {
  code: Coding<UsageContextType>;
  value: unknown; // TODO
}
