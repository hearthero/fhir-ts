import { Extension } from './special-purpose'
/*
The base definition for all elements contained inside a resource. All elements, whether defined as a Data Type (including primitives) or as part of a resource structure, have this base content:

- Extensions
- An internal id
 */

export interface Element {
  id?: string;
  extension?: Extension[];
}
