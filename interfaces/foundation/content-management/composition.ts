/*
A set of healthcare-related information that is assembled together into a single logical package that provides a single coherent statement of meaning, establishes its own context and that has clinical attestation with regard to who is making the statement. A Composition defines the structure and narrative content necessary for a document. However, a Composition alone does not constitute a document. Rather, the Composition must be the first entry in a Bundle where Bundle.type=document, and any other resources referenced from Composition must be included as subsequent entries in the Bundle (for example Patient, Practitioner, Encounter, etc.).
 */
import { DomainResource } from '../framework/domain-resource'
import { code, dateTime } from '../../primitives'
import { CodeableConcept, Identifier, Period } from '../base-types/general-purpose'
import { BackboneElement, Narrative, Reference } from '../base-types/special-purpose'
import { Encounter } from '../../administration/encounter'
import { Practitioner } from '../../administration/practitioner'
import { PractitionerRole } from '../../administration/practitioner-role'
import { Device } from '../../administration/device'
import { Patient } from '../../administration/patient'
import { RelatedPerson } from '../../administration/related-person'
import { Organization } from '../../administration/organization'
import { ListMode, ListOrderCodes } from '../../value-sets/value-sets-4-0'

export interface Composition extends DomainResource {
  identifier?: Identifier;
  status: code<CompositionStatus>;
  type: CodeableConcept<FHIRDocumentTypeCodes>;
  category?: CodeableConcept<DocumentClassValueSet>[];
  subject?: Reference<any>;
  encounter?: Reference<Encounter>;
  date: dateTime;
  author?: Reference<Practitioner | PractitionerRole | Device | Patient | RelatedPerson | Organization>;
  title: string;
  confidentiality?: code<V3ValueSetConfidentialityClassification>;
  attester?: BackboneElement & {
    mode: code<CompositionAttestationMode>;
    time?: dateTime;
    party?: Reference<Patient | RelatedPerson | Practitioner | PractitionerRole | Organization>;
  }[];
  custodian?: Reference<Organization>;
  relatesTo?: BackboneElement & {
    code: code<DocumentRelationsshipType>;
    target: unknown; //TODO
  }[];
  event?: BackboneElement & {
    code: CodeableConcept<V3CodeSystemActCode>[];
    period?: Period;
    detail?: Reference<any>[];
  }[];
  section?: CompositionSection[];
}

interface CompositionSection extends BackboneElement {
  title?: string;
  code?: CodeableConcept<DocumentSectionCodes>;
  author?: Reference<Practitioner | PractitionerRole | Device | Patient | RelatedPerson | Organization>[];
  focus?: Reference<any>;
  text?: Narrative;
  mode?: code<ListMode>;
  orderedBy?: CodeableConcept<ListOrderCodes>;
  entry?: Reference<any>[];
  emptyReason?: CodeableConcept<ListEmptyReasons>;
  section?: CompositionSection[];
}
