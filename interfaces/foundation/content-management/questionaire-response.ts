/*
A structured set of questions and their answers. The questions are ordered and grouped into coherent subsets, corresponding to the structure of the grouping of the questionnaire being responded to.
 */
import { DomainResource } from '../framework/domain-resource'
import { CarePlan } from '../../clinical/careplan'
import { BackboneElement, Reference } from '../base-types/special-purpose'
import { Identifier } from '../base-types/general-purpose'
import { ServiceRequest } from '../../workflow/service-request'
import { Observation } from '../../diagnostics/observation'
import { Procedure } from '../../clinical/procedure'
import { canonical, code, dateTime, uri } from '../../primitives'
import { Questionaire } from './questionaire'
import { Encounter } from '../../administration/encounter'
import { Device } from '../../administration/device'
import { Practitioner } from '../../administration/practitioner'
import { PractitionerRole } from '../../administration/practitioner-role'
import { Patient } from '../../administration/patient'
import { RelatedPerson } from '../../administration/related-person'
import { Organization } from '../../administration/organization'

export interface QuestionaireResponse extends DomainResource {
  identifier?: Identifier;
  basedOn?: Reference<CarePlan | ServiceRequest>;
  partOf?: Reference<Observation | Procedure>;
  questionaire?: canonical<Questionaire>;
  status: code<QuestionaireResponseStatus>;
  subject?: Reference<any>;
  encounter?: Reference<Encounter>;
  authored?: dateTime;
  author?: Reference<Device | Practitioner | PractitionerRole | Patient | RelatedPerson | Organization>;
  source?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson>;
  item?: QuestionaireResponseItem[];
}

interface QuestionaireResponseItem extends BackboneElement {
  linkId: string;
  definition?: uri;
  text?: string;
  answer?: BackboneElement & {
    value: unknown; // TODO
    item?: QuestionaireResponseItem[];
  }[];
  item?: QuestionaireResponseItem[];
}
