/*
A collection of documents compiled for a purpose together with metadata that applies to the collection.
 */

import { DomainResource } from '../framework/domain-resource'
import { CodeableConcept, Identifier } from '../base-types/general-purpose'
import { code, dateTime, uri } from '../../primitives'
import { BackboneElement, Reference } from '../base-types/special-purpose'
import { Patient } from '../../administration/patient'
import { Practitioner } from '../../administration/practitioner'
import { Group } from '../../administration/group'
import { Device } from '../../administration/device'
import { PractitionerRole } from '../../administration/practitioner-role'
import { Organization } from '../../administration/organization'
import { RelatedPerson } from '../../administration/related-person'

export interface DocumentManifest extends DomainResource {
  masterIdentifier?: Identifier;
  identifier?: Identifier[];
  status: code<DocumentReferenceStatus>;
  type?: CodeableConcept<V3CodeSystemActCode>;
  subject?: Reference<Patient | Practitioner | Group | Device>;
  created?: dateTime;
  author?: Reference<Practitioner | PractitionerRole | Organization | Device | Patient | RelatedPerson>[];
  recipient?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson | Organization>[];
  source?: uri;
  description?: string;
  content: Reference<any>[];
  related?: BackboneElement & {
    identifier?: Identifier;
    ref?: Reference<any>;
  };
}
