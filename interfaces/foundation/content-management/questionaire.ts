/*
A structured set of questions intended to guide the collection of answers from end-users. Questionnaires provide detailed control over order, presentation, phraseology and grouping to allow coherent, consistent data collection.
 */
import { canonical, code, date, dateTime, integer, markdown, uri } from '../../primitives'
import { DomainResource } from '../framework/domain-resource'
import { CodeableConcept, Coding, Identifier, Period } from '../base-types/general-purpose'
import { Jurisdiction, PublicationStatus, ResourceType } from '../../value-sets/value-sets-4-0'
import { ContactDetail, UsageContext } from '../base-types/metadata'
import { ValueSet } from '../../terminology/value-set'
import { BackboneElement } from '../base-types/special-purpose'

export interface Questionaire extends DomainResource {
  url?: uri;
  identifier?: Identifier[];
  version?: string;
  name?: string;
  title?: string;
  derivedFrom?: canonical<Questionaire>;
  status: code<PublicationStatus>;
  experimental?: boolean;
  subjectType?: code<ResourceType>[];
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description: markdown;
  useContext: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  copyright?: markdown;
  approvalDate?: date;
  lastReviewDate?: date;
  effectivePeriod?: Period;
  code?: Coding<QuestionaireQuestionCodes>[];
  item?: QuestionaireItem[];
}

interface QuestionaireItem extends BackboneElement {
  linkId: string;
  definition?: uri;
  code?: Coding<QuestioniareQuestionCodes>[];
  prefix?: string;
  text?: string;
  type: code<QuestionaireItemType>;
  enableWhen?: BackboneElement & {
    question: string;
    operator: code<QuestionaireItemOperator>;
    answer: unknown; // TODO
  };
  enableBehavior?: code<EnableWhenBehavior>;
  required?: boolean;
  repeats?: boolean;
  readOnly?: boolean;
  maxLength?: integer;
  answerValueSet?: canonical<ValueSet>;
  answerOption?: BackboneElement & {
    value: unknown; // TODO
    initialSelected?: boolean;
  }
  initial: BackboneElement & {
    value: unknown; // TODO
  }
  item?: QuestionaireItem;
}
