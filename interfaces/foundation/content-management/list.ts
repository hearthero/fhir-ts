/*
A list is a curated collection of resources.
 */
import { DomainResource } from '../framework/domain-resource'
import {
  ExampleUseCodesForList, ListEmptyReasons,
  ListMode,
  ListOrderCodes,
  ListStatus,
  PatientMedicineChangeTypes
} from '../../value-sets/value-sets-4-0'
import { Annotation, CodeableConcept, Identifier } from '../base-types/general-purpose'
import { BackboneElement, Reference } from '../base-types/special-purpose'
import { Patient } from '../../administration/patient'
import { Group } from '../../administration/group'
import { Device } from '../../administration/device'
import { Location } from '../../administration/location'
import { Encounter } from '../../administration/encounter'
import { code, dateTime } from '../../primitives'
import { Practitioner } from '../../administration/practitioner'
import { PractitionerRole } from '../../administration/practitioner-role'

export interface List extends DomainResource {
  identifier?: Identifier[];
  status: code<ListStatus>;
  mode: code<ListMode>;
  title?: string;
  code?: CodeableConcept<ExampleUseCodesForList>;
  subject?: Reference<Patient | Group | Device | Location>;
  encounter?: Reference<Encounter>;
  date?: dateTime;
  source?: Reference<Practitioner | PractitionerRole | Patient | Device>;
  orderedBy?: CodeableConcept<ListOrderCodes>;
  note?: Annotation[];
  entry?: BackboneElement & {
    flag?: CodeableConcept<PatientMedicineChangeTypes>;
    deleted?: boolean;
    date?: dateTime;
    item: Reference<any>;
  }[];
  emptyReason?: CodeableConcept<ListEmptyReasons>;
}
