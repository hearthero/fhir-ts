/*

A reference to a document of any kind for any purpose. Provides metadata about the document so that the document can be discovered and managed. The scope of a document is any seralized object with a mime-type, so includes formal patient centric documents (CDA), cliical notes, scanned paper, and non-patient specific documents like policy text.
 */
import { DomainResource } from '../framework/domain-resource'
import { Attachment, CodeableConcept, Coding, Identifier, Period } from '../base-types/general-purpose'
import { code, instant } from '../../primitives'
import { BackboneElement, Reference } from '../base-types/special-purpose'
import { Patient } from '../../administration/patient'
import { Practitioner } from '../../administration/practitioner'
import { Group } from '../../administration/group'
import { Device } from '../../administration/device'
import { PractitionerRole } from '../../administration/practitioner-role'
import { Organization } from '../../administration/organization'
import { RelatedPerson } from '../../administration/related-person'
import { PracticeSettingCodeValueSet, SecurityLabels } from '../../value-sets/value-sets-4-0'
import { Encounter } from '../../administration/encounter'
import { EpisodeOfCare } from '../../administration/episode-of-care'

export interface DocumentReference extends DomainResource {
  masterIdentifier?: Identifier;
  identifier?: Identifier[];
  status: code<DocumentReferenceStatus>;
  docStatus?: code<CompositionStatus>;
  type?: CodeableConcept<DocumentTypeValueSet>;
  category?: CodeableConcept<DocumentClassValueSet>[];
  subject?: Reference<Patient | Practitioner | Group | Device>;
  date?: instant;
  author?: Reference<Practitioner | PractitionerRole | Organization | Device | Patient | RelatedPerson>[];
  authentificator?: Reference<Practitioner | PractitionerRole | Organization>;
  custodian?: Reference<Organization>;
  relatesTo?: BackboneElement & {
    code: code<DocumentRelationshipType>;
    target: Reference<DocumentReference>;
  };
  description?: string;
  securityLabel?: CodeableConcept<SecurityLabels>[];
  content: BackboneElement & {
    attachment: Attachment;
    format?: Coding<DocumentReferenceFormatCodeSet>;
  }[];
  context?: BackboneElement & {
    encounter?: Reference<Encounter | EpisodeOfCare>[];
    event?: CodeableConcept<V3CodeSystemsActCode>[];
    period?: Period;
    facilityType?: CodeableConcept<FacilityTypeCodeValueSet>;
    practiceSetting?: CodeableConcept<PracticeSettingCodeValueSet>;
    sourcePatientInfo?: Reference<Patient>;
    related?: Reference<any>[];
  }
}
