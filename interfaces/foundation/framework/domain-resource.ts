
/*
A domain resource is a resource that:

- has a human-readable XHTML representation of the content of the resource (see [Human Narrative in resources](https://www.hl7.org/fhir/narrative.html))
- can contain additional related resources inside the resource (see [Contained Resources](https://www.hl7.org/fhir/references.html#contained))
- can have additional extensions and modifierExtensions as well as the defined data (See [Extensibility](https://www.hl7.org/fhir/extensibility.html))
- As an abstract resource, this resource is never created directly; instead, one of its descendant resources (see [List of Resources](https://www.hl7.org/fhir/resourcelist.html)) is created.
 */
import { Resource } from './resource'
import { Extension, Narrative } from '../base-types/special-purpose'

export interface DomainResource extends Resource {
  text?: Narrative;
  contained?: Resource[];
  extension?: Extension[];
  modifierExtension?: Extension[];
}
