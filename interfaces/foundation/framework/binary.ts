
/*
A resource that represents the data of a single raw artifact as digital content accessible in its native format. A Binary resource can contain any content, whether text, image, pdf, zip archive, etc.
 */
import { Resource } from './resource'
import { MimeType } from '../../value-sets/value-sets-4-0'
import { Reference } from '../base-types/special-purpose'
import { base64Binary, code } from '../../primitives'

export interface Binary extends Resource {
  contentType: code<MimeType>;
  securityContext?: Reference<any>;
  data?: base64Binary;
}
