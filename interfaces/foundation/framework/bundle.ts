import { Resource } from './resource'
import { Identifier, Signature } from '../base-types/general-purpose'
import { BundleType, HTTPVerb, SearchEntryMode } from '../../value-sets/value-sets-4-0'
import { code, decimal, instant, unsignedInt, uri } from '../../primitives'
import { BackboneElement } from '../base-types/special-purpose'

/*
One common operation performed with resources is to gather a collection of resources into a single instance with containing context. In FHIR this is referred to as "bundling" the resources together. These resource bundles are useful for a variety of different reasons, including:

- Returning a set of resources that meet some criteria as part of a server operation (see [RESTful Search](https://www.hl7.org/fhir/http.html#search))
- Returning a set of versions of resources as part of the history operation on a server (see [History](https://www.hl7.org/fhir/http.html#history))
- Sending a set of resources as part of a message exchange (see [Messaging](https://www.hl7.org/fhir/messaging.html))
- Grouping a self-contained set of resources to act as an exchangeable and persistable collection with clinical integrity - e.g. a clinical document (see [Documents](https://www.hl7.org/fhir/documents.html))
- Creating/updating/deleting a set of resources on a server as a single operation (including doing so as a single atomic transaction) (see [Transactions](https://www.hl7.org/fhir/http.html#transaction))
- Storing a collection of resources
 */
export interface Bundle extends Resource {
  identifier?: Identifier;
  type: code<BundleType>;
  timestamp?: instant;
  total?: unsignedInt;
  link?: BundleLink[];
  entry?: BackboneElement & {
    link?: BundleLink[];
    fullUrl?: uri;
    resource?: Resource;
    search?: BackboneElement & {
      mode?: code<SearchEntryMode>;
      score?: decimal;
    };
    request?: BackboneElement & {
      method: code<HTTPVerb>;
      url: uri;
      ifNoneMatch?: string;
      ifModifiedSince?: instant;
      ifMatch?: string;
      ifNoneExist?: string;
    };
    response?: BackboneElement & {
      status: string;
      location?: uri;
      etag?: string;
      lastModified?: instant;
      outcome?: Resource;
    };
  }[];
  signature?: Signature;
}

interface BundleLink extends BackboneElement {
  relation: string;
  url: uri;
}
