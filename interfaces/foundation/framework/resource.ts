/*
A resource is an entity that:

- has a known identity (a URL) by which it can be addressed
- identifies itself as one of the types of resource defined in this specification
- contains a set of structured data items as described by the definition of the resource type
- has an identified version that changes if the contents of the resource change
- Resources have [multiple representations](https://www.hl7.org/fhir/formats.html).
 */
import { code, id, uri } from '../../primitives'
import { CommonLanguages } from '../../value-sets/value-sets-4-0'
import { Meta } from '../base-types/special-purpose'

export interface Resource {
  id?: id;
  meta?: Meta;
  implicitRules?: uri;
  language?: code<CommonLanguages>;
}
