/*
Basic is used for handling concepts not yet defined in FHIR, narrative-only resources that don't map to an existing resource, and custom resources not appropriate for inclusion in the FHIR specification.
 */
import { DomainResource } from './domain-resource'
import { CodeableConcept, Identifier } from '../base-types/general-purpose'
import { date } from '../../primitives'
import { Reference } from '../base-types/special-purpose'
import { BasicResourceTypes } from '../../value-sets/value-sets-4-0'
import { Practitioner } from '../../administration/practitioner'
import { PractitionerRole } from '../../administration/practitioner-role'
import { Patient } from '../../administration/patient'
import { RelatedPerson } from '../../administration/related-person'
import { Organization } from '../../administration/organization'

export interface Basic extends DomainResource {
  identifier?: Identifier[];
  code: CodeableConcept<BasicResourceTypes>;
  subject?: Reference<any>;
  created?: date;
  author?: Reference<Practitioner | PractitionerRole | Patient | RelatedPerson | Organization>
}
