import { DomainResource } from '../foundation/framework/domain-resource'
import { ArrayOfOneOrMore, canonical, code, dateTime, decimal, id, integer, markdown, uri } from '../primitives'
import { CodeableConcept, Identifier } from '../foundation/base-types/general-purpose'
import {
  Jurisdiction,
  PublicationStatus, StructureMapContextType,
  StructureMapGroupTypeMode, StructureMapInputMode,
  StructureMapModelMode, StructureMapSourceListMode, StructureMapTargetListMode
} from '../value-sets/value-sets-4-0'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { BackboneElement } from '../foundation/base-types/special-purpose'
import { StructureDefinition } from '../conformance/structure-definition'

/*
A Map of relationships between 2 structures that can be used to transform data.
 */
export interface StructureMap extends DomainResource {
  url: uri;
  identifier?: Identifier[];
  version?: string;
  name: string;
  title?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  copyright?: markdown;
  structure?: BackboneElement & {
    url: canonical<StructureDefinition>;
    mode: code<StructureMapModelMode>;
    alias?: string;
    documentation?: string;
  }[];
  import?: canonical<StructureMap>[];
  group: ArrayOfOneOrMore<groupObj>;
}

interface groupObj extends BackboneElement {
  name: id;
  extends?: id;
  typeMode: code<StructureMapGroupTypeMode>;
  documentation?: string;
  input: ArrayOfOneOrMore<inputObj>;
  rule: ArrayOfOneOrMore<ruleObj>;
}

interface inputObj extends BackboneElement {
  name: id;
  type?: string;
  mode: code<StructureMapInputMode>;
  documentation?: string;
}

interface ruleObj extends BackboneElement {
  name: id;
  source: ArrayOfOneOrMore<sourceObj>;
  target?: targetObj[];
  rule?: ruleObj[];
  dependent?: BackboneElement & {
    name: id;
    variable?: ArrayOfOneOrMore<string>;
  }[];
  documentation?: string;
}

interface sourceObj extends BackboneElement {
  context: id;
  min?: integer;
  max?: string;
  type?: string;
  defaultValue?: anyFHIR;
  element?: string;
  listMode?: code<StructureMapSourceListMode>;
  variable?: id;
  condition?: string;
  check?: string;
  logMessage?: string;
}

interface targetObj extends BackboneElement {
  context?: id;
  contextType?: code<StructureMapContextType>;
  element?: string;
  variable?: id;
  listMode?: code<StructureMapTargetListMode>[];
  listRuleId?: id;
  transform?: code;
  parameter?: (BackboneElement & value)[];
}

type value = valueId | valueString | valueBoolean | valueInteger | valueDecimal;

interface valueId {
  valueId: id;
}

interface valueString {
  valueString: string;
}

interface valueBoolean {
  valueBoolean: boolean;
}

interface valueInteger {
  valueInteger: integer;
}

interface valueDecimal {
  valueDecimal: decimal;
}

// OpenType for DefaultValue

interface defaultValueX {
  defaultValueX: X;
}
