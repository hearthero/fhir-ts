import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Ratio } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Organization } from '../administration/organization'
import { code, dateTime } from '../primitives'
import { Substance } from '../administration/substance'
import { MedicationStatusCodes, SNOMEDCTFormCodes, SNOMEDCTMedicationCodes } from '../value-sets/value-sets-4-0'

/*
This resource is primarily used for the identification and definition of a medication for the purposes of prescribing, dispensing, and administering a medication as well as for making statements about medication use.
 */
export interface Medication extends DomainResource {
  identifier?: Identifier[];
  code?: CodeableConcept<SNOMEDCTMedicationCodes>;
  status?: code<MedicationStatusCodes>;
  manufacturer?: Reference<Organization>;
  form?: CodeableConcept<SNOMEDCTFormCodes>;
  amount?: Ratio;
  ingredient?: ingredientObj[];
  batch?: BackboneElement & {
    lotNumber?: string;
    expirationDate?: dateTime;
  }
}

type ingredientObj = ingredientObjBase & (itemCodeableConcept | itemReference)

interface ingredientObjBase extends BackboneElement {
  isActive?: boolean;
  strength?: Ratio;
}

interface itemCodeableConcept {
  itemCodeableConcept?: CodeableConcept<any>;
}

interface itemReference {
  itemReference?: Reference<Substance | Medication>;
}
