import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  Identifier,
  Period, Quantity, Ratio,
  SimpleQuantity
} from '../foundation/base-types/general-purpose'
import { code, dateTime, uri } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Procedure } from '../clinical/procedure'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { EpisodeOfCare } from '../administration/episode-of-care'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Device } from '../administration/device'
import { Condition } from '../clinical/condition'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import { MedicationRequest } from './medication-request'
import { Provenance } from '../security/provenance'
import {
  MedicationAdministrationCategoryCodes, MedicationAdministrationPerformerFunctionCodes,
  MedicationAdministrationStatusCodes, ReasonMedicationGivenCodes,
  SNOMEDCTAdministrationMethodCodes,
  SNOMEDCTAnatomicalStructureForAdministrationSiteCodes, SNOMEDCTMedicationCodes, SNOMEDCTReasonMedicationNotGivenCodes,
  SNOMEDCTRouteCodes
} from '../value-sets/value-sets-4-0'
import { Medication } from './medication'

/*
Describes the event of a patient consuming or otherwise being administered a medication. This may be as simple as swallowing a tablet or it may be a long running infusion. Related resources tie this event to the authorizing prescription, and the specific encounter between patient and health care practitioner.
 */
export type MedicationAdministration = MedicationAdministrationBase & medication & effective;

interface MedicationAdministrationBase extends DomainResource {
  identifier?: Identifier[];
  instantiates?: uri[];
  partOf?: Reference<MedicationAdministration | Procedure>[];
  status: code<MedicationAdministrationStatusCodes>;
  statusReason?: CodeableConcept<SNOMEDCTReasonMedicationNotGivenCodes>[];
  category?: CodeableConcept<MedicationAdministrationCategoryCodes>;
  subject: Reference<Patient | Group>;
  context?: Reference<Encounter | EpisodeOfCare>;
  supportingInformation?: Reference<any>[];
  performer?: BackboneElement & {
    function?: CodeableConcept<MedicationAdministrationPerformerFunctionCodes>;
    actor: Reference<Practitioner | PractitionerRole | Patient | RelatedPerson | Device>;
  }[];
  reasonCode?: CodeableConcept<ReasonMedicationGivenCodes>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport>[];
  request?: Reference<MedicationRequest>;
  device?: Reference<Device>[];
  note?: Annotation[];
  dosage?: dosageObj;
  eventHistory?: Reference<Provenance>[];
}

type medication = medicationCodeableConcept | medicationReference;

interface medicationCodeableConcept {
  medicationCodeableConcept?: CodeableConcept<SNOMEDCTMedicationCodes>;
}

interface medicationReference {
  medicationReference?: Reference<Medication>;
}

type effective = effectiveDateTime | effectivePeriod;

interface effectiveDateTime {
  effectiveDateTime?: dateTime;
}

interface effectivePeriod {
  effectivePeriod?: Period;
}

type dosageObj = dosageObjBase & (rateRatio | rateQuantity);

interface dosageObjBase extends BackboneElement {
  text?: string;
  site?: CodeableConcept<SNOMEDCTAnatomicalStructureForAdministrationSiteCodes>;
  route?: CodeableConcept<SNOMEDCTRouteCodes>;
  method?: CodeableConcept<SNOMEDCTAdministrationMethodCodes>;
  dose?: SimpleQuantity;
}

interface rateRatio {
  rateRatio?: Ratio;
}

interface rateQuantity {
  rateQuantity?: Quantity;
}
