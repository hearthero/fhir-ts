import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { ArrayOfOneOrMore, dateTime, positiveInt } from '../primitives'
import { Organization } from '../administration/organization'
import { Immunization } from './immunization'
import { ImmunizationEvaluation } from './immunization-evaluation'
import {
  ImmunizationRecommendationDateCriterionCodes,
  ImmunizationRecommendationReasonCodes,
  ImmunizationRecommendationStatusCodes,
  ImmunizationRecommendationTargetDiseaseCodes,
  VaccineAdministeredValueSet
} from '../value-sets/value-sets-4-0'

/*
A patient's point-in-time set of recommendations (i.e. forecasting) according to a published schedule with optional supporting justification.
 */
export interface ImmunizationRecommendation extends DomainResource {
  identifier?: Identifier[];
  patient: Reference<Patient>;
  date: dateTime;
  authority?: Reference<Organization>;
  recommendation: ArrayOfOneOrMore<recommendationObj>;
}

type recommendationObj = recommendationObjBase & doseNumber & seriesDoses;

interface recommendationObjBase {
  vaccineCode?: CodeableConcept<VaccineAdministeredValueSet>[];
  targetDisease?: CodeableConcept<ImmunizationRecommendationTargetDiseaseCodes>;
  contraindicatedVaccineCode?: CodeableConcept<VaccineAdministeredValueSet>[];
  forecastStatus: CodeableConcept<ImmunizationRecommendationStatusCodes>;
  forecastReason?: CodeableConcept<ImmunizationRecommendationReasonCodes>[];
  dateCriterion?: BackboneElement & {
    code: CodeableConcept<ImmunizationRecommendationDateCriterionCodes>;
    value: dateTime;
  }[];
  description?: string;
  series?: string;
  supportingImmunization?: Reference<Immunization | ImmunizationEvaluation>[];
  supportingPatientInformation?: Reference<any>[];
}

type doseNumber = doseNumberPositiveInt | doseNumberString;

interface doseNumberPositiveInt {
  doseNumberPositiveInt?: positiveInt;
}

interface doseNumberString {
  doseNumberString?: string;
}

type seriesDoses = seriesDosesPositiveInt | seriesDosesString;

interface seriesDosesPositiveInt {
  seriesDosesPositiveInt?: positiveInt;
}

interface seriesDosesString {
  seriesDosesString?: string;
}
