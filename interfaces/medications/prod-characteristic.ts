import { BackboneElement } from '../foundation/base-types/special-purpose'
import { Attachment, CodeableConcept, Quantity } from '../foundation/base-types/general-purpose'

/*
The ProdCharacteristic structure defines TODO.
 */
export interface ProdCharacteristic extends BackboneElement {
  height?: Quantity;
  width?: Quantity;
  depth?: Quantity;
  weight?: Quantity;
  nominalVolume?: Quantity;
  externalDiameter?: Quantity;
  shape?: string;
  color?: string[];
  imprint?: string[];
  image?: Attachment[];
  scoring?: CodeableConcept<any>;
}
