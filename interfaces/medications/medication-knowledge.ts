import { DomainResource } from '../foundation/framework/domain-resource'
import {
  CodeableConcept,
  Duration,
  Money,
  Quantity,
  Ratio,
  SimpleQuantity
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Dosage, Reference } from '../foundation/base-types/special-purpose'
import { Organization } from '../administration/organization'
import { ArrayOfOneOrMore, base64Binary, code, markdown } from '../primitives'
import { Medication } from './medication'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Media } from '../diagnostics/media'
import {
  MedicationKnowledgeCharacteristicCodes,
  MedicationKnowledgePackageTypeCodes,
  MedicationKnowledgeStatusCodes,
  SNOMEDCTFormCodes,
  SNOMEDCTMedicationCodes,
  SNOMEDCTRouteCodes
} from '../value-sets/value-sets-4-0'
import { DetectedIssue } from '../clinical/detected-issue'
import { Substance } from '../administration/substance'
import { ObservationDefinition } from '../administration/observation-definition'

/*
Information about a medication that is used to support knowledge.
 */
export interface MedicationKnowledge extends DomainResource {
  code?: CodeableConcept<SNOMEDCTMedicationCodes>;
  status?: code<MedicationKnowledgeStatusCodes>;
  manufacturer?: Reference<Organization>;
  doseForm?: CodeableConcept<SNOMEDCTFormCodes>;
  amount?: SimpleQuantity;
  synonym?: string;
  relatedMedicationKnowledge?: BackboneElement & {
    type: CodeableConcept<any>;
    reference: ArrayOfOneOrMore<Reference<MedicationKnowledge>>;
  }[];
  associatedMedication?: Reference<Medication>[];
  productType?: CodeableConcept<any>[];
  monograph?: BackboneElement & {
    type?: CodeableConcept<any>;
    source?: Reference<DocumentReference | Media>;
  }[];
  ingredient?: ingredientObj[];
  preparationInstruction?: markdown;
  intendedRoute?: CodeableConcept<SNOMEDCTRouteCodes>[];
  cost?: BackboneElement & {
    type: CodeableConcept<any>;
    source?: string;
    cost: Money;
  }[];
  monitoringProgram?: BackboneElement & {
    type?: CodeableConcept<any>;
    name?: string;
  }[];
  administrativeGuidelines?: administrativeGuidelinesObj[];
  medicineClassification?: BackboneElement & {
    type: CodeableConcept<any>;
    classification?: CodeableConcept<any>[];
  }[];
  packaging?: BackboneElement & {
    type?: CodeableConcept<MedicationKnowledgePackageTypeCodes>;
    quantity?: SimpleQuantity;
  };
  drugCharacteristic?: drugCharacteristicObj[];
  contraindication?: Reference<DetectedIssue>[];
  regulatory?: BackboneElement & {
    regulatoryAuthority: Reference<Organization>;
    substitution?: BackboneElement & {
      type: CodeableConcept<any>;
      allowed: boolean;
    }[];
    schedule?: BackboneElement & {
      schedule: CodeableConcept<any>;
    }[];
    maxDispense?: BackboneElement & {
      quantity: SimpleQuantity;
      period?: Duration;
    };
  }[];
  kinetics?: BackboneElement & {
    areaUnderCurve?: SimpleQuantity[];
    lethalDose50?: SimpleQuantity[];
    halfLifePeriod?: Duration;
  }[];
}

type ingredientObj = ingredientObjBase & (itemCodeableConcept | itemReference)

interface ingredientObjBase extends BackboneElement {
  isActive?: boolean;
  strength?: Ratio;
}

interface itemCodeableConcept {
  itemCodeableConcept?: CodeableConcept<any>;
}

interface itemReference {
  itemReference?: Reference<Substance | Medication>;
}

type administrativeGuidelinesObj = administrativeGuidelinesObjBase & indication;

interface administrativeGuidelinesObjBase extends BackboneElement {
  dosage?: BackboneElement & {
    type: CodeableConcept<any>;
    dosage: ArrayOfOneOrMore<Dosage>;
  }[];
  patientCharacteristics?: patientCharacteristicsObj[];
}

type indication = indicationCodeableConcept | indicationReference;

interface indicationCodeableConcept {
  indicationCodeableConcept?: CodeableConcept<any>;
}

interface indicationReference {
  indicationReference?: Reference<ObservationDefinition>;
}

type patientCharacteristicsObj = patientCharacteristicsObjBase & characteristics;

interface patientCharacteristicsObjBase extends BackboneElement {
  value?: string[];
}

type characteristics = characteristicsCodeableConcept | characteristicsQuantity;

interface characteristicsCodeableConcept {
  characteristicsCodeableConcept: CodeableConcept<any>;
}

interface characteristicsQuantity {
  characteristicsQuantity: Quantity;
}

type drugCharacteristicObj = drugCharacteristicObjBase & value;

interface drugCharacteristicObjBase extends BackboneElement {
  type?: CodeableConcept<MedicationKnowledgeCharacteristicCodes>;
}

type value = valueCodeableConcept | valueString | valueQuantity | valueBase64Binary;

interface valueCodeableConcept {
  valueCodeableConcept?: CodeableConcept<any>;
}

interface valueString {
  valueString?: string;
}

interface valueQuantity {
  valueQuantity?: SimpleQuantity;
}

interface valueBase64Binary {
  valueBase64Binary?: base64Binary;
}
