import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, Identifier, SimpleQuantity } from '../foundation/base-types/general-purpose'
import { code, date, dateTime, positiveInt, uri } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Encounter } from '../administration/encounter'
import { Location } from '../administration/location'
import { Organization } from '../administration/organization'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Condition } from '../clinical/condition'
import { Observation } from '../diagnostics/observation'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import {
  CodesForImmunizationSiteOfAdministration,
  ImmunizationFunctionCodes,
  ImmunizationFundingSource,
  ImmunizationOriginCodes,
  ImmunizationProgramEligibility,
  ImmunizationReasonCodes,
  ImmunizationRouteCodes,
  ImmunizationStatusCodes,
  ImmunizationStatusReasonCodes,
  ImmunizationSubpotentReason,
  ImmunizationTargetDiseaseCodes,
  VaccineAdministeredValueSet
} from '../value-sets/value-sets-4-0'

/*
Describes the event of a patient being administered a vaccine or a record of an immunization as reported by a patient, a clinician or another party.
 */
export type Immunization = ImmunizationBase & occurrence;

interface ImmunizationBase extends DomainResource {
  identifier?: Identifier[];
  status: code<ImmunizationStatusCodes>;
  statusReason?: CodeableConcept<ImmunizationStatusReasonCodes>;
  vaccineCode: CodeableConcept<VaccineAdministeredValueSet>;
  patient: Reference<Patient>;
  encounter?: Reference<Encounter>;
  recorded?: dateTime;
  primarySource?: boolean;
  reportOrigin?: CodeableConcept<ImmunizationOriginCodes>;
  location?: Reference<Location>;
  manufacturer?: Reference<Organization>;
  lotNumber?: string;
  expirationDate?: date;
  site?: CodeableConcept<CodesForImmunizationSiteOfAdministration>;
  route?: CodeableConcept<ImmunizationRouteCodes>;
  doseQuantity?: SimpleQuantity;
  performer?: BackboneElement & {
    function?: CodeableConcept<ImmunizationFunctionCodes>;
    actor: Reference<Practitioner | PractitionerRole | Organization>;
  }[];
  note?: Annotation[];
  reasonCode?: CodeableConcept<ImmunizationReasonCodes>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport>[];
  isSubpotent?: boolean;
  subpotentReason?: CodeableConcept<ImmunizationSubpotentReason>[];
  education?: BackboneElement & {
    documentType?: string;
    reference?: uri;
    publicationDate?: dateTime;
    presentationDate?: dateTime;
  }[];
  programEligibility?: CodeableConcept<ImmunizationProgramEligibility>[];
  fundingSource?: CodeableConcept<ImmunizationFundingSource>;
  reaction?: BackboneElement & {
    date?: dateTime;
    detail?: Reference<Observation>;
    reported?: boolean;
  }[];
  protocolApplied?: protocolAppliedObj[];
}

type occurrence = occurrenceDateTime | occurrenceString;

interface occurrenceDateTime {
  occurrenceDateTime: dateTime;
}

interface occurrenceString {
  occurrenceString: string;
}

type protocolAppliedObj = protocolAppliedObjBase & doseNumber & seriesDoses;

interface protocolAppliedObjBase extends BackboneElement {
  series?: string;
  authority?: Reference<Organization>;
  targetDisease?: CodeableConcept<ImmunizationTargetDiseaseCodes>[];
}

type doseNumber = doseNumberPositiveInt | doseNumberString;

interface doseNumberPositiveInt {
  doseNumberPositiveInt: positiveInt;
}

interface doseNumberString {
  doseNumberString: string;
}

type seriesDoses = seriesDosesPositiveInt | seriesDosesString;

interface seriesDosesPositiveInt {
  seriesDosesPositiveInt?: positiveInt;
}

interface seriesDosesString {
  seriesDosesString?: string;
}
