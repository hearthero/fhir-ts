import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, Identifier, SimpleQuantity } from '../foundation/base-types/general-purpose'
import { BackboneElement, Dosage, Reference } from '../foundation/base-types/special-purpose'
import { Procedure } from '../clinical/procedure'
import { code, dateTime } from '../primitives'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { EpisodeOfCare } from '../administration/episode-of-care'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { Device } from '../administration/device'
import { RelatedPerson } from '../administration/related-person'
import { Location } from '../administration/location'
import { MedicationRequest } from './medication-request'
import { DetectedIssue } from '../clinical/detected-issue'
import { Provenance } from '../security/provenance'
import { Medication } from './medication'
import {
  MedicationDispenseCategoryCodes, MedicationDispensePerformerFunctionCodes,
  MedicationDispenseStatusCodes,
  MedicationDispenseStatusReasonCodes,
  SNOMEDCTMedicationCodes,
  v3_ActPharmacySupplyType,
  v3_ActSubstanceAdminSubstitutionCode, v3_SubstanceAdminSubstitutionReason
} from '../value-sets/value-sets-4-0'

/*
Indicates that a medication product is to be or has been dispensed for a named person/patient. This includes a description of the medication product (supply) provided and the instructions for administering the medication. The medication dispense is the result of a pharmacy system responding to a medication order.
 */
export type MedicationDispense = MedicationDispenseBase & statusReason & medication;

interface MedicationDispenseBase extends DomainResource {
  identifier?: Identifier[];
  partOf?: Reference<Procedure>[];
  status: code<MedicationDispenseStatusCodes>;
  category?: CodeableConcept<MedicationDispenseCategoryCodes>;
  subject?: Reference<Patient | Group>;
  context?: Reference<Encounter | EpisodeOfCare>;
  supportingInformation?: Reference<any>[];
  performer?: BackboneElement & {
    function?: CodeableConcept<MedicationDispensePerformerFunctionCodes>;
    actor: Reference<Practitioner | PractitionerRole | Organization | Patient | Device | RelatedPerson>;
  }[];
  location?: Reference<Location>;
  authorizingPrescription?: Reference<MedicationRequest>[];
  type?: CodeableConcept<v3_ActPharmacySupplyType>;
  quantity?: SimpleQuantity;
  daysSupply?: SimpleQuantity;
  whenPrepared?: dateTime;
  whenHandedOver?: dateTime;
  destination?: Reference<Location>;
  receiver?: Reference<Patient | Practitioner>[];
  note?: Annotation[];
  dosageInstruction?: Dosage[];
  substitution?: BackboneElement & {
    wasSubstituted: boolean;
    type?: CodeableConcept<v3_ActSubstanceAdminSubstitutionCode>;
    reason?: CodeableConcept<v3_SubstanceAdminSubstitutionReason>[];
    responsibleParty?: Reference<Practitioner | PractitionerRole>[];
  };
  detectedIssue?: Reference<DetectedIssue>[];
  eventHistory?: Reference<Provenance>[];
}

type statusReason = statusReasonCodeableConcept | statusReasonReference;

interface statusReasonCodeableConcept {
  statusReasonCodeableConcept?: CodeableConcept<MedicationDispenseStatusReasonCodes>;
}

interface statusReasonReference {
  statusReasonReference?: Reference<DetectedIssue>;
}

type medication = medicationCodeableConcept | medicationReference;

interface medicationCodeableConcept {
  medicationCodeableConcept?: CodeableConcept<SNOMEDCTMedicationCodes>;
}

interface medicationReference {
  medicationReference?: Reference<Medication>;
}
