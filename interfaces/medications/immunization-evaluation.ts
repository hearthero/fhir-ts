import { DomainResource } from '../foundation/framework/domain-resource'
import { code, dateTime, positiveInt } from '../primitives'
import { CodeableConcept, Identifier } from '../foundation/base-types/general-purpose'
import { Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Organization } from '../administration/organization'
import { Immunization } from './immunization'
import {
  ImmunizationEvaluationDoseStatusCodes, ImmunizationEvaluationDoseStatusReasonCodes,
  ImmunizationEvaluationStatusCodes,
  ImmunizationEvaluationTargetDiseaseCodes
} from '../value-sets/value-sets-4-0'

/*
Describes a comparison of an immunization event against published recommendations to determine if the administration is "valid" in relation to those recommendations.
 */
export type ImmunizationEvaluation = ImmunizationEvaluationBase & doseNumber & seriesDoses;

interface ImmunizationEvaluationBase extends DomainResource {
  identifier?: Identifier[];
  status: code<ImmunizationEvaluationStatusCodes>;
  patient: Reference<Patient>;
  date?: dateTime;
  authority?: Reference<Organization>;
  targetDisease: CodeableConcept<ImmunizationEvaluationTargetDiseaseCodes>;
  immunizationEvent: Reference<Immunization>;
  doseStatus: CodeableConcept<ImmunizationEvaluationDoseStatusCodes>;
  doseStatusReason?: CodeableConcept<ImmunizationEvaluationDoseStatusReasonCodes>[];
  description?: string;
  series?: string;
}

type doseNumber = doseNumberPositiveInt | doseNumberString;

interface doseNumberPositiveInt {
  doseNumberPositiveInt?: positiveInt;
}

interface doseNumberString {
  doseNumberString?: string;
}

type seriesDoses = seriesDosesPositiveInt | seriesDosesString;

interface seriesDosesPositiveInt {
  seriesDosesPositiveInt?: positiveInt;
}

interface seriesDosesString {
  seriesDosesString?: string;
}
