import { BackboneElement } from '../foundation/base-types/special-purpose'
import { CodeableConcept, Identifier, Quantity } from '../foundation/base-types/general-purpose'

/*
The ProductShelfLife structure defines TODO.
 */
export interface ProductShelfLife extends BackboneElement {
  identifier?: Identifier;
  type: CodeableConcept<any>;
  period: Quantity;
  specialPrecautionsForStorage?: CodeableConcept<any>[];
}
