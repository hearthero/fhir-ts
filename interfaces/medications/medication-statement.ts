import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { Dosage, Reference } from '../foundation/base-types/special-purpose'
import { MedicationRequest } from './medication-request'
import { CarePlan } from '../clinical/careplan'
import { ServiceRequest } from '../workflow/service-request'
import { MedicationAdministration } from './medication-administration'
import { MedicationDispense } from './medication-dispense'
import { Procedure } from '../clinical/procedure'
import { Observation } from '../diagnostics/observation'
import { code, dateTime } from '../primitives'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { EpisodeOfCare } from '../administration/episode-of-care'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Organization } from '../administration/organization'
import { Practitioner } from '../administration/practitioner'
import { Condition } from '../clinical/condition'
import { DiagnosticReport } from '../diagnostics/diagnostic-report'
import {
  Condition_Problem_DiagnosisCodes,
  MedicationStatusCodes, MedicationUsageCategoryCodes, SNOMEDCTDrugTherapyStatusCodes,
  SNOMEDCTMedicationCodes
} from '../value-sets/value-sets-4-0'
import { Medication } from './medication'

/*

A record of a medication that is being consumed by a patient. A MedicationStatement may indicate that the patient may be taking the medication now or has taken the medication in the past or will be taking the medication in the future. The source of this information can be the patient, significant other (such as a family member or spouse), or a clinician. A common scenario where this information is captured is during the history taking process during a patient visit or stay. The medication information may come from sources such as the patient's memory, from a prescription bottle, or from a list of medications the patient, clinician or other party maintains.

The primary difference between a medication statement and a medication administration is that the medication administration has complete administration information and is based on actual administration information from the person who administered the medication. A medication statement is often, if not always, less specific. There is no required date/time when the medication was administered, in fact we only know that a source has reported the patient is taking this medication, where details such as time, quantity, or rate or even medication product may be incomplete or missing or less precise. As stated earlier, the medication statement information may come from the patient's memory, from a prescription bottle or from a list of medications the patient, clinician or other party maintains. Medication administration is more formal and is not missing detailed information.
 */
export type MedicationStatement = MedicationStatementBase & medication & effective;

interface MedicationStatementBase extends DomainResource {
  identifier?: Identifier[];
  basedOn?: Reference<MedicationRequest | CarePlan | ServiceRequest>[];
  partOf?: Reference<MedicationAdministration | MedicationDispense | MedicationStatement | Procedure | Observation>[];
  status: code<MedicationStatusCodes>;
  statusReason?: CodeableConcept<SNOMEDCTDrugTherapyStatusCodes>[];
  category?: CodeableConcept<MedicationUsageCategoryCodes>;
  subject: Reference<Patient | Group>;
  context?: Reference<Encounter | EpisodeOfCare>;
  dateAsserted?: dateTime;
  informationSource?: Reference<Patient | Practitioner | PractitionerRole | RelatedPerson | Organization>;
  derivedFrom?: Reference<any>[];
  reasonCode?: CodeableConcept<Condition_Problem_DiagnosisCodes>[];
  reasonReference?: Reference<Condition | Observation | DiagnosticReport>[];
  note?: Annotation[];
  dosage?: Dosage[];
}

type medication = medicationCodeableConcept | medicationReference;

interface medicationCodeableConcept {
  medicationCodeableConcept?: CodeableConcept<SNOMEDCTMedicationCodes>;
}

interface medicationReference {
  medicationReference?: Reference<Medication>;
}

type effective = effectiveDateTime | effectivePeriod;

interface effectiveDateTime {
  effectiveDateTime?: dateTime;
}

interface effectivePeriod {
  effectivePeriod?: Period;
}
