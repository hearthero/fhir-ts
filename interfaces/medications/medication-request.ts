import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  Duration,
  Identifier, Period,
  SimpleQuantity
} from '../foundation/base-types/general-purpose'
import { canonical, code, dateTime, unsignedInt, uri } from '../primitives'
import { BackboneElement, Dosage, Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Group } from '../administration/group'
import { Encounter } from '../administration/encounter'
import { Organization } from '../administration/organization'
import { RelatedPerson } from '../administration/related-person'
import { Device } from '../administration/device'
import { CareTeam } from '../clinical/careteam'
import { Condition } from '../clinical/condition'
import { Observation } from '../diagnostics/observation'
import { CarePlan } from '../clinical/careplan'
import { ServiceRequest } from '../workflow/service-request'
import { DetectedIssue } from '../clinical/detected-issue'
import { Provenance } from '../security/provenance'
import { Coverage } from '../financial/coverage'
import { ClaimResponse } from '../financial/claim-response'
import { ImmunizationRecommendation } from './immunization-recommendation'
import {
  Condition_Problem_DiagnosisCodes,
  MedicationRequestCategoryCodes,
  MedicationRequestCourseofTherapyCodes,
  MedicationRequestIntent, MedicationRequestStatus, MedicationRequestStatusReasonCodes,
  ProcedurePerformerRoleCodes,
  RequestPriority,
  SNOMEDCTMedicationCodes,
  v3_ActSubstanceAdminSubstitutionCode,
  v3_SubstanceAdminSubstitutionReason
} from '../value-sets/value-sets-4-0'
import { Medication } from './medication'

/*
An order or request for both supply of the medication and the instructions for administration of the medication to a patient. The resource is called "MedicationRequest" rather than "MedicationPrescription" or "MedicationOrder" to generalize the use across inpatient and outpatient settings, including care plans, etc., and to harmonize with workflow patterns.
 */
export type MedicationRequest = MedicationRequestBase & reported & medication;

interface MedicationRequestBase extends DomainResource {
  identifier?: Identifier[];
  status: code<MedicationRequestStatus>;
  statusReason?: CodeableConcept<MedicationRequestStatusReasonCodes>;
  intent: code<MedicationRequestIntent>;
  category?: CodeableConcept<MedicationRequestCategoryCodes>[];
  priority?: code<RequestPriority>;
  doNotPerform?: boolean;
  subject: Reference<Patient | Group>;
  encounter?: Reference<Encounter>;
  supportingInformation?: Reference<any>[];
  authoredOn?: dateTime;
  requester?: Reference<Practitioner | PractitionerRole | Organization | Patient | RelatedPerson | Device>;
  performer?: Reference<Practitioner | PractitionerRole | Organization | Patient | Device | RelatedPerson | CareTeam>;
  performerType?: CodeableConcept<ProcedurePerformerRoleCodes>;
  recorder?: Reference<Practitioner | PractitionerRole>;
  reasonCode?: CodeableConcept<Condition_Problem_DiagnosisCodes>[];
  reasonReference?: Reference<Condition | Observation>[];
  instantiatesCanonical?: canonical<any>[];
  instantiatesUri?: uri[];
  basedOn?: Reference<CarePlan | MedicationRequest | ServiceRequest | ImmunizationRecommendation>[];
  groupIdentifier?: Identifier;
  courseOfTherapyType?: CodeableConcept<MedicationRequestCourseofTherapyCodes>;
  insurance?: Reference<Coverage | ClaimResponse>[];
  note?: Annotation[];
  dosageInstruction?: Dosage[];
  dispenseRequest?: BackboneElement & {
    initialFill?: BackboneElement & {
      quantity?: SimpleQuantity;
      duration?: Duration;
    };
    dispenseInterval?: Duration;
    validityPeriod?: Period;
    numberOfRepeatsAllowed?: unsignedInt;
    quantity?: SimpleQuantity;
    expectedSupplyDuration?: Duration;
    performer?: Reference<Organization>;
  };
  substitution?: substitutionObj;
  priorPrescription?: Reference<MedicationRequest>;
  detectedIssue?: Reference<DetectedIssue>[];
  eventHistory?: Reference<Provenance>[];
}

type reported = reportedBoolean | reportedReference;

interface reportedBoolean {
  reportedBoolean?: boolean;
}

interface reportedReference {
  reportedReference?: Reference<Patient | Practitioner | PractitionerRole>;
}

type medication = medicationCodeableConcept | medicationReference;

interface medicationCodeableConcept {
  medicationCodeableConcept?: CodeableConcept<SNOMEDCTMedicationCodes>;
}

interface medicationReference {
  medicationReference: Reference<Medication>;
}

type substitutionObj = substitutionObjBase & (allowedBoolean | allowedCodeableConcept);

interface substitutionObjBase {
  reason?: CodeableConcept<v3_ActSubstanceAdminSubstitutionCode>;
}

interface allowedBoolean {
  allowedBoolean?: boolean;
}

interface allowedCodeableConcept {
  allowedCodeableConcept?: CodeableConcept<v3_SubstanceAdminSubstitutionReason>;
}
