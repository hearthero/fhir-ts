import { DomainResource } from '../foundation/framework/domain-resource'
import { ArrayOfOneOrMore, canonical, code, dateTime, markdown, uri } from '../primitives'
import {
  ConceptMapEquivalence,
  ConceptMapGroupUnmappedMode,
  Jurisdiction,
  PublicationStatus
} from '../value-sets/value-sets-4-0'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { CodeableConcept, Identifier } from '../foundation/base-types/general-purpose'
import { ValueSet } from './value-set'
import { BackboneElement } from '../foundation/base-types/special-purpose'
import { CodeSystem } from './code-system'

/*
A statement of relationships from one set of concepts to one or more other concepts - either concepts in code systems, or data element/data element concepts, or classes in class models.
 */
export type ConceptMap = ConceptMapBase & (sourceUri | sourceCanonical) & (targetUri | targetCanonical);

interface ConceptMapBase extends DomainResource { // TRIAL
  url?: uri;
  identifier?: Identifier;
  version?: string;
  name?: string;
  title?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  copyright?: markdown;
  group: BackboneElement & {
    source?: uri;
    sourceVersion?: string;
    target?: uri;
    targetVersion?: string;
    element?: ArrayOfOneOrMore<elementObj>;
    unmapped?: BackboneElement & {
      mode: code<ConceptMapGroupUnmappedMode>;
      code?: code;
      display?: string;
      url?: canonical<ConceptMap>;
    }
  }[];
}

interface elementObj extends BackboneElement {
  code?: code;
  display?: string;
  equivalence: code<ConceptMapEquivalence>;
  comment?: string;
  dependsOn?: dependsOnObj[];
  product?: dependsOnObj[];
}

interface dependsOnObj extends BackboneElement {
  property: uri;
  system?: canonical<CodeSystem>;
  value: string;
  display?: string;
}

interface sourceUri {
  sourceUri?: uri;
}

interface sourceCanonical {
  sourceCanonical?: canonical<ValueSet>;
}

interface targetUri {
  targetUri?: uri;
}

interface targetCanonical {
  targetCanonical?: canonical<ValueSet>;
}
