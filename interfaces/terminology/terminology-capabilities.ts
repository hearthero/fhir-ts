import { DomainResource } from '../foundation/framework/domain-resource'
import { ArrayOfOneOrMore, canonical, code, dateTime, markdown, uri, url } from '../primitives'
import {
  CapabilityStatementKind, CodeSearchSupport,
  Jurisdiction,
  PublicationStatus
} from '../value-sets/value-sets-4-0'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { CodeableConcept } from '../foundation/base-types/general-purpose'
import { BackboneElement } from '../foundation/base-types/special-purpose'
import { CodeSystem } from './code-system'

/*
A TerminologyCapabilities resource documents a set of capabilities (behaviors) of a FHIR Terminology Server that may be used as a statement of actual server functionality or a statement of required or desired server implementation.
 */
export interface TerminologyCapabilities extends DomainResource { // TRIAL
  url?: uri;
  version?: string;
  name?: string;
  title?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  copyright?: markdown;
  kind: code<CapabilityStatementKind>;
  software?: BackboneElement & {
    name: string;
    version?: string;
  };
  implementation?: BackboneElement & {
    description: string;
    url?: url;
  };
  lockedDate?: boolean;
  codeSystem?: BackboneElement & {
    uri?: canonical<CodeSystem>;
    version?: BackboneElement & {
      code?: string;
      isDefault?: boolean;
      compositional?: boolean;
      language?: code[];
      filter?: BackboneElement & {
        code: code;
        op: ArrayOfOneOrMore<code>;
      }[];
      property?: code[];
    }[];
    subsumption?: boolean;
  }[];
  expansion?: BackboneElement & {
    hierarchical?: boolean;
    paging?: boolean;
    incomplete?: boolean;
    parameter?: BackboneElement & {
      name: code;
      documentation?: string;
    }[];
    textFilter?: markdown;
  };
  codeSearch: code<CodeSearchSupport>;
  validateCode?: BackboneElement & {
    translations: boolean;
  };
  translation?: BackboneElement & {
    needsMap?: boolean;
  };
  closure?: BackboneElement & {
    translation?: boolean;
  };
}
