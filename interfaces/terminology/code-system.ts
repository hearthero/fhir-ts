import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Coding, Identifier } from '../foundation/base-types/general-purpose'
import {
  CodeSystemContentMode, CodeSystemHierarchyMeaning,
  CommonLanguages,
  DesignationUse,
  FilterOperator,
  Jurisdiction, PropertyType,
  PublicationStatus
} from '../value-sets/value-sets-4-0'
import {
  ArrayOfOneOrMore,
  canonical,
  code,
  dateTime,
  decimal,
  integer,
  markdown,
  unsignedInt,
  uri
} from '../primitives'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { ValueSet } from './value-set'
import { BackboneElement } from '../foundation/base-types/special-purpose'

/*
The CodeSystem resource is used to declare the existence of and describe a code system or code system supplement and its key properties, and optionally define a part or all of its content.
 */
export interface CodeSystem extends DomainResource {
  url?: uri;
  identifier?: Identifier[];
  version?: string;
  name?: string;
  title?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[] // TRIAL
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  purpose?: markdown;
  copyright?: markdown;
  caseSensitive?: boolean;
  valueSet?: canonical<ValueSet>;
  hierarchyMeaning?: code<CodeSystemHierarchyMeaning>;
  compositional?: boolean;
  versionNeeded?: boolean;
  content: code<CodeSystemContentMode>;
  supplements?: canonical<CodeSystem>;
  count?: unsignedInt;
  filter?: BackboneElement & {
    code: code;
    description?: string;
    operator: ArrayOfOneOrMore<code<FilterOperator>>;
    value: string;
  }[];
  property?: BackboneElement & {
    code: code;
    uri?: uri;
    description?: string;
    type: code<PropertyType>;
  }[];
  concept?: conceptObj[];
}

interface conceptObj extends BackboneElement {
  code: code;
  display?: string;
  definition?: string;
  designation?: BackboneElement & {
    language: code<CommonLanguages>;
    use?: Coding<DesignationUse>;
    value: string;
  }[];
  property?: propertyObj[];
  concept?: conceptObj[];
}

type propertyObj = {code: code} & (valueCode | valueCoding | valueString | valueInteger | valueBoolean | valueDateTime | valueDecimal);

interface valueCode {
  valueCode: code;
}

interface valueCoding {
  valueCoding: Coding;
}

interface valueString {
  valueString: string;
}

interface valueInteger {
  valueInteger: integer;
}

interface valueBoolean {
  valueBoolean: boolean;
}

interface valueDateTime {
  valueDateTime: dateTime;
}

interface valueDecimal {
  valueDecimal: decimal;
}
