import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Coding, Identifier } from '../foundation/base-types/general-purpose'
import { ArrayOfOneOrMore, canonical, code, date, dateTime, integer, markdown, uri } from '../primitives'
import {
  CommonLanguages,
  DesignationUse,
  FilterOperator,
  Jurisdiction,
  PublicationStatus
} from '../value-sets/value-sets-4-0'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { BackboneElement } from '../foundation/base-types/special-purpose'

interface designationObj extends BackboneElement {
  language?: code<CommonLanguages>;
  use?: Coding<DesignationUse>;
  value: string;
}

interface includeObj extends BackboneElement {
  system?: uri;
  version?: string;
  concept?: BackboneElement & {
    code: code;
    display?: string;
    designation?: designationObj[];
    filter?: BackboneElement & {
      property: code;
      op: code<FilterOperator>;
      value: string;
    }[];
    valueSet?: canonical<ValueSet>;
  }[];
}

interface containsObj extends BackboneElement {
  system?: uri;
  abstract?: boolean;
  inactive?: boolean;
  version?: string;
  code?: code;
  display?: string;
  designation?: designationObj[];
  contains?: containsObj[];
}

/*
A ValueSet resource instance specifies a set of codes drawn from one or more code systems, intended for use in a particular context. Value sets link between [CodeSystem](https://www.hl7.org/fhir/codesystem.html) definitions and their use in [coded elements](https://www.hl7.org/fhir/terminologies.html).
 */
export interface ValueSet extends DomainResource {
  url: uri;
  identifier?: Identifier[];
  version?: string;
  name?: string;
  title?: string;
  status: code<PublicationStatus>;
  experimental?: boolean;
  date?: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>;
  immutable?: boolean;
  purpose?: markdown;
  copyright?: markdown;
  compose?: BackboneElement & {
    lockedDate?: date;
    inactive?: boolean;
    include: ArrayOfOneOrMore<includeObj>;
    exclude?: includeObj[];
  };
  expansion?: BackboneElement & {
    identifier?: uri;
    timestamp: dateTime;
    total?: integer;
    offset?: integer;
    parameter?: BackboneElement & {
      name: string;
      value: unknown; // TODO
    }[];
    contains?: containsObj[];
  }
}
