import { DomainResource } from '../foundation/framework/domain-resource'
import {
  IdentifierType,
  Jurisdiction,
  NamingSystemIdentifierType,
  NamingSystemType,
  PublicationStatus
} from '../value-sets/value-sets-4-0'
import { ContactDetail, UsageContext } from '../foundation/base-types/metadata'
import { ArrayOfOneOrMore, code, dateTime, markdown } from '../primitives'
import { CodeableConcept, Period } from '../foundation/base-types/general-purpose'
import { BackboneElement } from '../foundation/base-types/special-purpose'

/*
A curated namespace that issues unique symbols within that namespace for the identification of concepts, people, devices, etc. Represents a "System" used within the Identifier and Coding data types.
 */
export interface NamingSystem extends DomainResource { // TRIAL
  name: string;
  status: code<PublicationStatus>;
  kind: code<NamingSystemType>;
  date: dateTime;
  publisher?: string;
  contact?: ContactDetail[];
  responsible?: string;
  type?: CodeableConcept<IdentifierType>;
  description?: markdown;
  useContext?: UsageContext[];
  jurisdiction?: CodeableConcept<Jurisdiction>[];
  usage?: string;
  uniqueId: ArrayOfOneOrMore<BackboneElement & {
    type: code<NamingSystemIdentifierType>
    value: string;
    preferred?: boolean;
    comment?: string;
    period?: Period;
  }>;
}
