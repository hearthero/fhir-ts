import { DomainResource } from '../foundation/framework/domain-resource'
import { ArrayOfOneOrMore, code, dateTime, instant, uri } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { CodeableConcept, Period, Signature } from '../foundation/base-types/general-purpose'
import {
  ProvenanceActivityType,
  ProvenanceEntityRole,
  ProvenanceParticipantType,
  SecurityRoleType, v3_PurposeOfUse
} from '../value-sets/value-sets-4-0'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Patient } from '../administration/patient'
import { Organization } from '../administration/organization'
import { Location } from '../administration/location'
import { Device } from '../administration/device'

/*
Provenance of a resource is a record that describes entities and processes involved in producing and delivering or otherwise influencing that resource. Provenance provides a critical foundation for assessing authenticity, enabling trust, and allowing reproducibility. Provenance assertions are a form of contextual metadata and can themselves become important records with their own provenance. Provenance statement indicates clinical significance in terms of confidence in authenticity, reliability, and trustworthiness, integrity, and stage in lifecycle (e.g. Document Completion - has the artifact been legally authenticated), all of which may impact security, privacy, and trust policies.
 */
export type Provenance = ProvenanceBase & (occurredPeriod | occurredDateTime);

interface ProvenanceBase extends DomainResource {
  target: ArrayOfOneOrMore<Reference<any>>;
  recorded: instant;
  policy?: uri[];
  location?: Reference<Location>;
  reason?: CodeableConcept<v3_PurposeOfUse>;
  activity?: CodeableConcept<ProvenanceActivityType>;
  agent: ArrayOfOneOrMore<agentObj>;
  entity?: BackboneElement & {
    role: code<ProvenanceEntityRole>;
    what: Reference<any>;
    agent?: agentObj[];
  }[];
  signature?: Signature[];
}

interface agentObj {
  type?: CodeableConcept<ProvenanceParticipantType>;
  role?: CodeableConcept<SecurityRoleType>[];
  who: Reference<Practitioner | PractitionerRole | RelatedPerson | Patient | Device | Organization>;
  onBehalfOf?: Reference<Practitioner | PractitionerRole | RelatedPerson | Patient | Device | Organization>;
}

interface occurredPeriod {
  occurredPeriod?: Period;
}

interface occurredDateTime {
  occurredDateTime?: dateTime;
}
