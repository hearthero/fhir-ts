import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Coding, Period } from '../foundation/base-types/general-purpose'
import {
  AuditEventAction, AuditEventAgentNetworkType, AuditEventEntityRole, AuditEventEntityType,
  AuditEventID, AuditEventOutcome,
  AuditEventSubType, MediaTypeCodes, ObjectLifecycleEvents, ParticipantRoleType,
  SecurityLabels,
  SecurityRoleType,
  v3_PurposeOfUse
} from '../value-sets/value-sets-4-0'
import { ArrayOfOneOrMore, base64Binary, code, instant, uri } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Patient } from '../administration/patient'
import { Device } from '../administration/device'
import { Organization } from '../administration/organization'
import { Practitioner } from '../administration/practitioner'
import { Location } from '../administration/location'

/*
A record of an event made for purposes of maintaining a security log. Typical uses include detection of intrusion attempts and monitoring for inappropriate usage.
 */
export type AuditEvent = AuditEventBase;

interface AuditEventBase extends DomainResource {
  type: Coding<AuditEventID>;
  subtype?: Coding<AuditEventSubType>[];
  action?: code<AuditEventAction>;
  period?: Period;
  recorded: instant;
  outcome?: code<AuditEventOutcome>;
  outcomeDesc?: string;
  purposeOfEvent?: CodeableConcept<v3_PurposeOfUse>[];
  agent?: ArrayOfOneOrMore<agentObj>;
  entity?: BackboneElement & {
    what?: Reference<any>;
    type?: Coding<AuditEventEntityType>;
    role?: Coding<AuditEventEntityRole>;
    lifecycle?: Coding<ObjectLifecycleEvents>;
    securityLabel?: Coding<SecurityLabels>[];
    name?: string;
    description?: string;
    query?: base64Binary;
    detail?: detailObj[];
  }[];
}

interface agentObj extends BackboneElement {
  type?: CodeableConcept<ParticipantRoleType>;
  role?: CodeableConcept<SecurityRoleType>[];
  who?: Reference<PractitionerRole | Practitioner | Organization | Device | Patient | RelatedPerson>;
  altId?: string;
  name?: string;
  requestor: boolean;
  location?: Reference<Location>;
  policy?: uri[];
  media?: Coding<MediaTypeCodes>[];
  network?: BackboneElement & {
    address?: string;
    type?: code<AuditEventAgentNetworkType>;
  };
  purposeOfUse?: CodeableConcept<v3_PurposeOfUse>[];
}

type detailObj = {type: string} & (valueString | valueBase64Binary);

interface valueString {
  valueString?: string;
}

interface valueBase64Binary {
  valueBase64Binary?: base64Binary;
}
