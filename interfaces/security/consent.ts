import { DomainResource } from '../foundation/framework/domain-resource'
import { Attachment, CodeableConcept, Coding, Identifier, Period } from '../foundation/base-types/general-purpose'
import { ArrayOfOneOrMore, code, dateTime, uri } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { QuestionaireResponse } from '../foundation/content-management/questionaire-response'
import {
  ConsentActionCodes,
  ConsentCategoryCodes,
  ConsentContentClass,
  ConsentContentCodes,
  ConsentDataMeaning,
  ConsentPolicyRuleCodes,
  ConsentProvisionType,
  ConsentScopeCodes,
  ConsentState, SecurityLabels,
  SecurityRoleType,
  v3_PurposeOfUse
} from '../value-sets/value-sets-4-0'
import { Patient } from '../administration/patient'
import { Organization } from '../administration/organization'
import { PractitionerRole } from '../administration/practitioner-role'
import { RelatedPerson } from '../administration/related-person'
import { Practitioner } from '../administration/practitioner'
import { Contract } from '../financial/contract'
import { Device } from '../administration/device'
import { Group } from '../administration/group'
import { CareTeam } from '../clinical/careteam'

/*
A record of a healthcare consumer’s choices, which permits or denies identified recipient(s) or recipient role(s) to perform one or more actions within a given policy context, for specific purposes and periods of time.
 */
export type Consent = ConsentBase & (sourceAttachment | sourceReference);

interface ConsentBase extends DomainResource {
  identifier?: Identifier;
  status: code<ConsentState>;
  scope: CodeableConcept<ConsentScopeCodes>;
  category: ArrayOfOneOrMore<CodeableConcept<ConsentCategoryCodes>>;
  patient?: Reference<Patient>;
  dateTime?: dateTime;
  performer?: Reference<Organization | Patient | Practitioner | RelatedPerson | PractitionerRole>[];
  organization?: Reference<Organization>[];
  policy?: BackboneElement & {
    authority?: uri;
    uri: uri;
  }[];
  policyRule?: CodeableConcept<ConsentPolicyRuleCodes>;
  verification?: BackboneElement & {
    verified: boolean;
    verifiedWith?: Reference<Patient | RelatedPerson>;
    verificationDate?: dateTime;
  }[];
  provision?: provisionObj;
}

interface sourceAttachment {
  sourceAttachment?: Attachment;
}

interface sourceReference {
  sourceReference?: Reference<Consent | DocumentReference | Contract | QuestionaireResponse>;
}

interface provisionObj {
  type?: code<ConsentProvisionType>;
  period?: Period;
  actor?: BackboneElement & {
    role: CodeableConcept<SecurityRoleType>;
    reference: Reference<Device | Group | CareTeam | Organization | Patient | Practitioner | RelatedPerson | PractitionerRole>;
  }[];
  action?: CodeableConcept<ConsentActionCodes>[];
  securityLabel?: Coding<SecurityLabels>[];
  purpose?: Coding<v3_PurposeOfUse>[];
  class?: Coding<ConsentContentClass>[];
  code?: CodeableConcept<ConsentContentCodes>[];
  dataPeriod?: Period;
  data?: BackboneElement & {
    meaning: code<ConsentDataMeaning>;
    reference: Reference<any>;
  };
  provision?: provisionObj[];
}
