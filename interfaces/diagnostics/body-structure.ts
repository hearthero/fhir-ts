import { DomainResource } from '../foundation/framework/domain-resource'
import { Attachment, CodeableConcept, Identifier } from '../foundation/base-types/general-purpose'
import { Reference } from '../foundation/base-types/special-purpose'
import { Patient } from '../administration/patient'
import {
  BodystructureLocationQualifier,
  SNOMEDCTBodyStructures,
  SNOMEDCTMorphologicAbnormalities
} from '../value-sets/value-sets-4-0'

/*
Record details about an anatomical structure. This resource may be used when a coded concept does not provide the necessary detail needed for the use case.
 */
export interface BodyStructure extends DomainResource {
  identifier?: Identifier[];
  active?: boolean;
  morphology?: CodeableConcept<SNOMEDCTMorphologicAbnormalities>;
  location?: CodeableConcept<SNOMEDCTBodyStructures>;
  locationQualifier?: CodeableConcept<BodystructureLocationQualifier>[];
  description?: string;
  image?: Attachment[];
  patient: Reference<Patient>;
}
