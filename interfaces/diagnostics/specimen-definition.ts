import { DomainResource } from '../foundation/framework/domain-resource'
import {CodeableConcept, Duration, Identifier, Range, SimpleQuantity} from '../foundation/base-types/general-purpose'
import {BackboneElement, Reference} from '../foundation/base-types/special-purpose'
import { code } from '../primitives'
import {Substance} from "../administration/substance";
import {
  ContainerCap, ContainerMaterials, HandlingConditionSet,
  PreparePatient, RejectionCriterion, SpecimenCollection,
  SpecimenContainedPreference,
  SpecimenContainerType,
  v2_0371,
  v2_0487
} from "../value-sets/value-sets-4-0";

/*
A kind of specimen with associated set of requirements.
 */
export interface SpecimenDefinition extends DomainResource {
  identifier?: Identifier;
  typeCollected?: CodeableConcept<v2_0487>;
  patientPreparation?: CodeableConcept<PreparePatient>[];
  timeAspect?: string;
  collection?: CodeableConcept<SpecimenCollection>;
  typeTested?: BackboneElement & {
    isDerived?: boolean;
    type?: CodeableConcept<v2_0487>;
    preference: code<SpecimenContainedPreference>;
    container?: containerObj;
    requirement?: string;
    retentionTime?: Duration;
    rejectionCriterion?: CodeableConcept<RejectionCriterion>[];
    handling?: BackboneElement & {
      temperatureQualifier?: CodeableConcept<HandlingConditionSet>;
      temperatureRange?: Range;
      maxDuration?: Duration;
      instruction?: string;
    }
  }[];
}

type containerObj = containerObjBase & minimumVolume;

interface containerObjBase extends BackboneElement {
  material?: CodeableConcept<ContainerMaterials>;
  type?: CodeableConcept<SpecimenContainerType>;
  cap?: CodeableConcept<ContainerCap>;
  description?: string;
  capacity?: SimpleQuantity;
  additive?: (BackboneElement & additive)[];
}

type minimumVolume = minimumVolumeQuantity | minimumVolumeString;

type additive = additiveCodeableConcept | additiveReference;

interface minimumVolumeQuantity {
  minimumVolumeQuantity?: SimpleQuantity;
}

interface minimumVolumeString {
  minimumVolumeString?: string;
}

interface additiveCodeableConcept {
  additiveCodeableConcept: CodeableConcept<v2_0371>;
}

interface additiveReference {
  additiveReference: Reference<Substance>;
}
