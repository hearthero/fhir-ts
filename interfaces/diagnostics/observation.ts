import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  Identifier, Period, Quantity,
  Range, Ratio, SampledData,
  SimpleQuantity, Timing
} from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { CarePlan } from '../clinical/careplan'
import {
  DataAbsentReason,
  LOINCCodes,
  ObservationCategoryCodes,
  ObservationInterpretationCodes,
  ObservationMethods,
  ObservationReferenceRangeAppliesToCodes,
  ObservationReferenceRangeMeaningCodes,
  ObservationStatus,
  SNOMEDCTBodyStructures
} from '../value-sets/value-sets-4-0'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Device } from '../administration/device'
import { Location } from '../administration/location'
import { Encounter } from '../administration/encounter'
import { code, dateTime, instant, integer, time } from '../primitives'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { CareTeam } from '../clinical/careteam'
import { RelatedPerson } from '../administration/related-person'
import { Procedure } from '../clinical/procedure'
import { DeviceMetric } from '../administration/device-metric'
import { QuestionaireResponse } from '../foundation/content-management/questionaire-response'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { ImagingStudy } from './imaging-study'
import { Media } from './media'
import { MolecularSequence } from './molecular-sequence'
import { Specimen } from './specimen'
import { ServiceRequest } from '../workflow/service-request'
import { DeviceRequest } from '../workflow/device-request'
import { ImmunizationRecommendation } from '../medications/immunization-recommendation'
import { NutritionOrder } from '../workflow/nutrition-order'
import { MedicationRequest } from '../medications/medication-request'
import { MedicationAdministration } from '../medications/medication-administration'
import { MedicationDispense } from '../medications/medication-dispense'
import { MedicationStatement } from '../medications/medication-statement'
import { Immunization } from '../medications/immunization'

/*
Measurements and simple assertions made about a patient, device or other subject.
 */
export type Observation = ObservationBase & effective & value;

interface ObservationBase extends DomainResource {
  identifier?: Identifier[];
  basedOn?: Reference<CarePlan | DeviceRequest | ImmunizationRecommendation | MedicationRequest | NutritionOrder | ServiceRequest>[];
  partOf?: Reference<MedicationAdministration | MedicationDispense | MedicationStatement | Procedure | Immunization | ImagingStudy>[];
  status: code<ObservationStatus>;
  category?: CodeableConcept<ObservationCategoryCodes>[];
  code: CodeableConcept<LOINCCodes>;
  subject?: Reference<Patient | Group | Device | Location>;
  focus?: Reference<any>[];
  encounter?: Reference<Encounter>;
  issued?: instant;
  performer?: Reference<Practitioner | PractitionerRole | Organization | CareTeam | Patient | RelatedPerson>[];
  dataAbsentReason?: CodeableConcept<DataAbsentReason>;
  interpretation?: CodeableConcept<ObservationInterpretationCodes>[];
  note?: Annotation[];
  bodySite?: CodeableConcept<SNOMEDCTBodyStructures>;
  method?: CodeableConcept<ObservationMethods>;
  specimen?: Reference<Specimen>;
  device?: Reference<Device | DeviceMetric>;
  referenceRange?: referenceRangeObj[];
  hasMember?: Reference<Observation | QuestionaireResponse | MolecularSequence>[];
  derivedFrom?: Reference<DocumentReference | ImagingStudy | Media | QuestionaireResponse | Observation | MolecularSequence>[];
  component?: componentObj[];
}

interface referenceRangeObj extends BackboneElement {
  low?: SimpleQuantity;
  high?: SimpleQuantity;
  type?: CodeableConcept<ObservationReferenceRangeMeaningCodes>;
  appliesTo?: CodeableConcept<ObservationReferenceRangeAppliesToCodes>[];
  age?: Range;
  text?: string;
}

type componentObj = componentObjBase & value;

interface componentObjBase {
  code: CodeableConcept<LOINCCodes>;
  dataAbsentReason?: CodeableConcept<DataAbsentReason>;
  interpretation?: CodeableConcept<ObservationInterpretationCodes>[];
  referenceRange?: referenceRangeObj[];
}

type effective = effectiveDateTime | effectivePeriod | effectiveTiming | effectiveInstant;

interface effectiveDateTime {
  effectiveDateTime?: dateTime;
}

interface effectivePeriod {
  effectivePeriod?: Period;
}

interface effectiveTiming {
  effectiveTiming?: Timing;
}

interface effectiveInstant {
  effectiveInstant?: instant;
}

type value = valueQuantity | valueCodeableConcept | valueString | valueBoolean | valueInteger | valueRange | valueRatio | valueSampledData | valueTime | valueDateTime | valuePeriod;

interface valueQuantity {
  valueQuantity?: Quantity;
}

interface valueCodeableConcept {
  valueCodeableConcept?: CodeableConcept<any>;
}

interface valueString {
  valueString?: string;
}

interface valueBoolean {
  valueBoolean?: boolean;
}

interface valueInteger {
  valueInteger?: integer;
}

interface valueRange {
  valueRange?: Range;
}

interface valueRatio {
  valueRatio?: Ratio;
}

interface valueSampledData {
  valueSampledData?: SampledData;
}

interface valueTime {
  valueTime?: time;
}

interface valueDateTime {
  valueDateTime?: dateTime;
}

interface valuePeriod {
  valuePeriod?: Period;
}
