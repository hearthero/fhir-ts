import { DomainResource } from '../foundation/framework/domain-resource'
import { CodeableConcept, Identifier, Quantity } from '../foundation/base-types/general-purpose'
import { code, decimal, integer, uri } from '../primitives'
import { Patient } from '../administration/patient'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Device } from '../administration/device'
import { Organization } from '../administration/organization'
import { Observation } from './observation'
import { Specimen } from './specimen'
import {
  chromosome_human,
  ENSEMBL, FDA_Method, FDA_StandardSequence, LOINCLL379_9_answerlist,
  orientationType,
  qualityType,
  repositoryType,
  sequenceType, strandType
} from '../value-sets/value-sets-4-0'

/*
The MolecularSequence resource is designed to describe an atomic sequence which contains the alignment sequencing test result and multiple variations. Atomic sequences can be connected by link element and they will lead to sequence graph. By this method, a sequence can be reported. Complete genetic sequence information, of which specific genetic variations are a part, is reported by reference to the GA4GH repository. Thus, the FHIR MolecularSequence resource avoids large genomic payloads in a manner analogous to how the FHIR ImagingStudy resource references large images maintained in other systems. For use cases, details on how this resource interact with other Clinical Genomics resources or profiles, please refer to implementation guidance document [here](https://www.hl7.org/fhir/genomics.html).
 */
export interface MolecularSequence extends DomainResource {
  identifier?: Identifier[];
  type?: code<sequenceType>;
  coordinateSystem: integer;
  patient?: Reference<Patient>;
  specimen?: Reference<Specimen>;
  device?: Reference<Device>;
  performer?: Reference<Organization>;
  quantity?: Quantity;
  referenceSeq?: BackboneElement & {
    chromosome?: CodeableConcept<chromosome_human>;
    genomeBuild?: string;
    orientation?: code<orientationType>;
    referenceSeqId?: CodeableConcept<ENSEMBL>;
    referenceSeqPointer?: Reference<MolecularSequence>;
    referenceSeqString?: string;
    strand?: code<strandType>;
    windowStart?: integer;
    windowEnd?: integer;
  };
  variant?: BackboneElement & {
    start?: integer;
    end?: integer;
    observedAllele?: string;
    referenceAllele?: string;
    cigar?: string;
    variantPointer?: Reference<Observation>;
  }[];
  observedSeq?: string;
  quality?: BackboneElement & {
    type?: code<qualityType>;
    standardSequence?: CodeableConcept<FDA_StandardSequence>;
    start?: integer;
    end?: integer;
    score?: Quantity;
    method?: CodeableConcept<FDA_Method>;
    truthTP?: decimal;
    queryTP?: decimal;
    truthFN?: decimal;
    queryFP?: decimal;
    gtFP?: decimal;
    precision?: decimal;
    recall?: decimal;
    fScore?: decimal;
    roc?: BackboneElement & {
      score?: integer[];
      numTP: integer[];
      numFP?: integer[];
      numFN?: integer[];
      precision?: decimal[];
      sensitivity?: decimal[];
      fMeasure?: decimal[];
    };
    readCoverage?: integer;
    repository?: BackboneElement & {
      type: code<repositoryType>;
      url?: uri;
      name?: string;
      datasetId?: string;
      variantsetId?: string;
      readsetId?: string;
    }[];
    pointer?: Reference<MolecularSequence>[];
    structureVariant?: BackboneElement & {
      variantType?: CodeableConcept<LOINCLL379_9_answerlist>;
      exact: boolean;
      length?: integer;
      outer?: BackboneElement & {
        start?: integer;
        end?: integer;
      };
      inner?: BackboneElement & {
        start?: integer;
        end?: integer;
      };
    }[];
  }[];
}
