import { DomainResource } from '../foundation/framework/domain-resource'
import { Attachment, CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { CarePlan } from '../clinical/careplan'
import { code, dateTime, instant } from '../primitives'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Device } from '../administration/device'
import { Location } from '../administration/location'
import { Encounter } from '../administration/encounter'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Organization } from '../administration/organization'
import { CareTeam } from '../clinical/careteam'
import { Observation } from './observation'
import { ServiceRequest } from '../workflow/service-request'
import { Specimen } from './specimen'
import { ImagingStudy } from './imaging-study'
import { Media } from './media'
import {
  DiagnosticReportStatus,
  DiagnosticServiceSectionCodes, LOINCDiagnosticReportCodes,
  SNOMEDCTClinicalFindings
} from '../value-sets/value-sets-4-0'
import { ImmunizationRecommendation } from '../medications/immunization-recommendation'
import { NutritionOrder } from '../workflow/nutrition-order'
import { MedicationRequest } from '../medications/medication-request'

/*
The findings and interpretation of diagnostic tests performed on patients, groups of patients, devices, and locations, and/or specimens derived from these. The report includes clinical context such as requesting and provider information, and some mix of atomic results, images, textual and coded interpretations, and formatted representation of diagnostic reports.
 */
export type DiagnosticReport = DiagnosticReportBase & (effectiveDateTime | effectivePeriod);

interface DiagnosticReportBase extends DomainResource {
  identifier?: Identifier[];
  basedOn?: Reference<CarePlan | ImmunizationRecommendation | MedicationRequest | NutritionOrder | ServiceRequest>[];
  status: code<DiagnosticReportStatus>;
  category?: CodeableConcept<DiagnosticServiceSectionCodes>[];
  code: CodeableConcept<LOINCDiagnosticReportCodes>;
  subject?: Reference<Patient | Group | Device | Location>;
  encounter?: Reference<Encounter>;
  issued?: instant;
  performer?: Reference<Practitioner | PractitionerRole | Organization | CareTeam>[];
  resultsInterpreter?: Reference<Practitioner | PractitionerRole | Organization | CareTeam>[];
  specimen?: Reference<Specimen>[];
  result?: Reference<Observation>[];
  imagingStudy?: Reference<ImagingStudy>[];
  media?: BackboneElement & {
    comment?: string;
    link: Reference<Media>;
  }[];
  conclusion?: string;
  conclusionCode?: CodeableConcept<SNOMEDCTClinicalFindings>[];
  presentedForm?: Attachment[];
}

interface effectiveDateTime {
  effectiveDateTime?: dateTime;
}

interface effectivePeriod {
  effectivePeriod?: Period;
}
