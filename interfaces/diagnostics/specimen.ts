import { DomainResource } from '../foundation/framework/domain-resource'
import {
  Annotation,
  CodeableConcept,
  Duration,
  Identifier, Period,
  SimpleQuantity
} from '../foundation/base-types/general-purpose'
import { Patient } from '../administration/patient'
import { Group } from '../administration/group'
import { Device } from '../administration/device'
import { Substance } from '../administration/substance'
import { Location } from '../administration/location'
import { code, dateTime } from '../primitives'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { ServiceRequest } from '../workflow/service-request'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import {
  FHIRSpecimenCollectionMethod,
  SNOMEDCTBodyStructures,
  SpecimenContainerType, SpecimenProcessingProcedure,
  SpecimenStatus,
  v2_0371, v2_0487, v2_0493,
  v2_0916
} from '../value-sets/value-sets-4-0'

/*
A sample to be used for analysis.
 */
export interface Specimen extends DomainResource {
  identifier?: Identifier[];
  accessionIdentifier?: Identifier;
  status?: code<SpecimenStatus>;
  type?: CodeableConcept<v2_0487>;
  subject?: Reference<Patient | Group | Device | Substance | Location>;
  receivedTime?: dateTime;
  parent?: Reference<Specimen>[];
  request?: Reference<ServiceRequest>[];
  collection?: collectionObj;
  processing?: processingObj[];
  container?: containerObj[];
  condition?: CodeableConcept<v2_0493>[];
  note?: Annotation[];
}

type collectionObj = collectionObjBase & (collectedDateTime | collectedPeriod) & (fastingStatusCodeableConcept | fastingStatusDuration);

interface collectionObjBase extends BackboneElement {
  collector?: Reference<Practitioner | PractitionerRole>;
  duration?: Duration;
  quantity?: SimpleQuantity;
  method?: CodeableConcept<FHIRSpecimenCollectionMethod>;
  bodySite?: CodeableConcept<SNOMEDCTBodyStructures>;
}

type processingObj = processingObjBase & (timeDateTime | timePeriod);

interface processingObjBase extends BackboneElement {
  description?: string;
  procedure?: CodeableConcept<SpecimenProcessingProcedure>;
  additive?: Reference<Substance>[];
}

type containerObj = containerObjBase & (additiveCodeableConcept | additiveReference);

interface containerObjBase extends BackboneElement {
  identifier?: Identifier[];
  description?: string;
  type?: CodeableConcept<SpecimenContainerType>;
  capacity?: SimpleQuantity;
  specimenQuantity?: SimpleQuantity;
}

interface collectedDateTime {
  collectedDateTime?: dateTime;
}

interface collectedPeriod {
  collectedPeriod?: Period;
}

interface fastingStatusCodeableConcept {
  fastingStatusCodeableConcept?: CodeableConcept<v2_0916>;
}

interface fastingStatusDuration {
  fastingStatusDuration?: Duration;
}

interface timeDateTime {
  timeDateTime?: dateTime;
}

interface timePeriod {
  timePeriod?: Period;
}

interface additiveCodeableConcept {
  additiveCodeableConcept?: CodeableConcept<v2_0371>;
}

interface additiveReference {
  additiveReference?: Reference<Substance>;
}
