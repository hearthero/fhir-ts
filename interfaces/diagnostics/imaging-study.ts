import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, CodeableConcept, Coding, Identifier } from '../foundation/base-types/general-purpose'
import { code, dateTime, id, unsignedInt } from '../primitives'
import { Patient } from '../administration/patient'
import { Device } from '../administration/device'
import { Group } from '../administration/group'
import { BackboneElement, Reference } from '../foundation/base-types/special-purpose'
import { Encounter } from '../administration/encounter'
import { CarePlan } from '../clinical/careplan'
import { ServiceRequest } from '../workflow/service-request'
import { AppointmentResponse } from '../workflow/appointment-response'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Endpoint } from '../administration/endpoint'
import { Procedure } from '../clinical/procedure'
import { Location } from '../administration/location'
import { Condition } from '../clinical/condition'
import { Observation } from './observation'
import { Media } from './media'
import { DiagnosticReport } from './diagnostic-report'
import { DocumentReference } from '../foundation/content-management/document-reference'
import { Organization } from '../administration/organization'
import { CareTeam } from '../clinical/careteam'
import { RelatedPerson } from '../administration/related-person'
import { Specimen } from './specimen'
import {
  AcquisitionModality,
  ImagingProcedureCode, ImagingStudySeriesPerformerFunction,
  ImagingStudyStatus, Laterality,
  ProcedureReasonCodes,
  SNOMEDCTBodyStructures,
  sopClass
} from '../value-sets/value-sets-4-0'
import { Appointment } from '../workflow/appointment'
import { Task } from '../workflow/task'

/*
Representation of the content produced in a DICOM imaging study. A study comprises a set of series, each of which includes a set of Service-Object Pair Instances (SOP Instances - images or other data) acquired or produced in a common context. A series is of only one modality (e.g. X-ray, CT, MR, ultrasound), but a study may have multiple series of different modalities.
 */
export interface ImagingStudy extends DomainResource {
  identifier?: Identifier[];
  status: code<ImagingStudyStatus>;
  modality?: Coding<AcquisitionModality>[];
  subject: Reference<Patient | Device | Group>;
  encounter?: Reference<Encounter>;
  started?: dateTime;
  basedOn?: Reference<CarePlan | ServiceRequest | Appointment | AppointmentResponse | Task>[];
  referrer?: Reference<Practitioner | PractitionerRole>;
  interpreter?: Reference<Practitioner | PractitionerRole>[];
  endpoint?: Reference<Endpoint>[];
  numberOfSeries?: unsignedInt;
  numberOfInstances?: unsignedInt;
  procedureReference?: Reference<Procedure>;
  procedureCode?: CodeableConcept<ImagingProcedureCode>[];
  location?: Reference<Location>;
  reasonCode?: CodeableConcept<ProcedureReasonCodes>[];
  reasonReference?: Reference<Condition | Observation | Media | DiagnosticReport | DocumentReference>[];
  note?: Annotation[];
  description?: string;
  series?: BackboneElement & {
    uid: id;
    number?: unsignedInt;
    modality: Coding<AcquisitionModality>;
    description?: string;
    numberOfInstances?: unsignedInt;
    endpoint?: Reference<Endpoint>[];
    bodySite?: Coding<SNOMEDCTBodyStructures>;
    laterality?: Coding<Laterality>;
    specimen?: Reference<Specimen>[];
    started?: dateTime;
    performer?: BackboneElement & {
      function?: CodeableConcept<ImagingStudySeriesPerformerFunction>;
      actor: Reference<Practitioner | PractitionerRole | Organization | CareTeam | Patient | Device | RelatedPerson>;
    }[];
    instance?: BackboneElement & {
      uid: id;
      sopClass: Coding<sopClass>;
      number?: unsignedInt;
      title?: string;
    }[];
  }[];
}
