import { DomainResource } from '../foundation/framework/domain-resource'
import { Annotation, Attachment, CodeableConcept, Identifier, Period } from '../foundation/base-types/general-purpose'
import { Reference } from '../foundation/base-types/special-purpose'
import { ServiceRequest } from '../workflow/service-request'
import { CarePlan } from '../clinical/careplan'
import { Patient } from '../administration/patient'
import { Practitioner } from '../administration/practitioner'
import { PractitionerRole } from '../administration/practitioner-role'
import { Group } from '../administration/group'
import { Device } from '../administration/device'
import { Location } from '../administration/location'
import { Encounter } from '../administration/encounter'
import { code, dateTime, decimal, instant, positiveInt } from '../primitives'
import { Organization } from '../administration/organization'
import { CareTeam } from '../clinical/careteam'
import { RelatedPerson } from '../administration/related-person'
import { DeviceMetric } from '../administration/device-metric'
import { Specimen } from './specimen'
import {
  EventStatus,
  MediaCollectionView_Projection,
  MediaModality,
  MediaType,
  ProcedureReasonCodes, SNOMEDCTBodyStructures
} from '../value-sets/value-sets-4-0'

/*
A photo, video, or audio recording acquired or used in healthcare. The actual content may be inline or provided by direct reference.
 */
export type Media = MediaBase & created;

interface MediaBase extends DomainResource {
  identifier?: Identifier[];
  basedOn?: Reference<ServiceRequest | CarePlan>[];
  partOf?: Reference<any>[];
  status: code<EventStatus>;
  type?: CodeableConcept<MediaType>;
  modality?: CodeableConcept<MediaModality>;
  view?: CodeableConcept<MediaCollectionView_Projection>;
  subject?: Reference<Patient | Practitioner | PractitionerRole | Group | Device | Specimen | Location>;
  encounter?: Reference<Encounter>;
  issued?: instant;
  operator?: Reference<Practitioner | PractitionerRole | Organization | CareTeam | Patient | Device | RelatedPerson>;
  reasonCode?: CodeableConcept<ProcedureReasonCodes>[];
  bodySite?: CodeableConcept<SNOMEDCTBodyStructures>;
  deviceName?: string;
  device?: Reference<Device | DeviceMetric>;
  height?: positiveInt;
  width?: positiveInt;
  frames?: positiveInt;
  duration?: decimal;
  content: Attachment;
  note?: Annotation[];
}

type created = createdDateTime | createdPeriod;

interface createdDateTime {
  createdDateTime?: dateTime;
}

interface createdPeriod {
  createdPeriod?: Period;
}
